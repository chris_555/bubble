function sendComment() {
    var text =  $(".input-block").html();
    var url = window.location.pathname+'/sendComment';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '<?= csrf_token() ?>'
        }
    });
    $.ajax({
        url: url,
        data: {text:text},
        type: 'POST',
        success: function(response){
            console.log(response);
            $('.comments').append('<div class ="comment"> <div class="comment-header"><div class="comment-user">'+response.user+'</div> <div class="comment-date">'+response.created_at+'</div><div class="comment-content">'+text+' </div> </div>')
            $(".input-block").html('');

        },
        error: function(){
            console.log('Ошибка');
        }
    });
};
