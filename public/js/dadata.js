

$("#address").suggestions({
    token: "58cafa94228f6f4db6f297a1ae5d144dac4436ae",
    type: "ADDRESS",
    constraints: {
        locations: {
            kladr_id: "7200000000000"
        },
        deletable: false
    },
    onSelect: function(suggestion) {
        console.log(suggestion);
    }
});
$( "#address" ).change(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/dadata',
        method: 'PUT',
        data: {_token: CSRF_TOKEN, message:$("#address").val()},
        dataType: 'JSON',
        success: function(res)
        {
         console.log(res);
        },
        error: function(msg){
            console.log(msg);
        }
    });
});
