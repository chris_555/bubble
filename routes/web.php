<?php

Auth::routes();

Route::get('/', 'UserController@myStatistics')->name('myStatistics');;
Route::get('/home', 'UserController@lk')->name('home');
Route::get('/auth', 'HomeController@auth');

// objects
Route::get('/objects/{tab?}', 'ObjectController@objectsList')->name('objectsList');
Route::get('/objects/{tab?}/{drawer?}', 'ObjectController@objectsList')->name('objectsList');
Route::get('/object/{objectId}/ticket/{ticketId}/{tab?}/{edit?}', 'ObjectController@objectCard')->name('objectCard');
Route::post('/object/{objectId}/ticket/{ticketId}/getTab','ObjectController@getTab');
Route::post('/objects/get', 'ObjectController@objectsGet');
Route::post('/objects/search', 'ObjectController@objectsSearch');
Route::post('/objects/filter_search','ObjectController@filterSearch');
Route::post('/objects/filter_reset','ObjectController@filterReset');
Route::post('/objects/getCoords','ObjectController@getCoords');

//tickets
Route::get('/tickets/{tab?}/{drawer?}', 'TicketController@ticketList')->name('ticketsList');
Route::get('/ticket/{ticketId}/{tab?}/{edit?}', 'TicketController@ticketCard')->name('ticketCard');
Route::post('/tickets/get', 'TicketController@ticketsGet');
Route::post('/tickets/search', 'TicketController@ticketsSearch');
Route::post('/tickets/findMyTicket', 'TicketController@findMyTicket');

//developers
Route::get('/developers', 'DeveloperController@developersList')->name('developersList');
Route::get('/developer/{developerId}/{tab?}/{edit?}', 'DeveloperController@developerCard')->name('developerCard');
Route::post('/developers/get', 'DeveloperController@developersGet');
Route::post('/developers/search', 'DeveloperController@developersSearch');
Route::post('/developer/edit', 'DeveloperController@developerEdit');

//clients
Route::get('/clients', 'ClientController@index')->name('clientsList');
Route::get('/client/{clientId}/{tab?}/{edit?}', 'ClientController@clientCard')->name('clientCard');
Route::post('/clients/get', 'ClientController@clientsGet');
Route::post('/clients/search', 'ClientController@clientsSearch');
Route::post('/client/edit', 'ClientController@clientEdit');

//users
Route::get('/lk', 'UserController@lk')->name('lk');
Route::get('/user', 'UserController@lk')->name('userCard');
Route::post('/users/search', 'UserController@searchUser');
Route::post('/users/findUser', 'UserController@findUser');
Route::get('/statistics/{id?}','UserController@myStatistics')->name('myStatistics');;
Route::get('/statistics/user/{id?}','UserController@userStatistics')->name('userStatistics');;
Route::post('/users/getList', 'UserController@usersList');
Route::post('/users/get', 'UserController@users');
Route::post('/users/createUser', 'UserController@createUser');
Route::get('/user/{id}', 'UserController@userCard')->name('userCard');
Route::get('/users/{drawer?}', 'UserController@users_admin')->name('users_admin');
Route::get('/tickets', 'UserController@tickets_admin')->name('tickets_admin');

//test pages
Route::get('/test', function () {return view('test');});


//comments
Route::post('/{ticketId}/sendComment','SmartController@sendComment');

//SMART CONTROLLER -AJAX
Route::post('tickets/addToFavorite','SmartController@addToFavorite');
Route::post('tickets/changeStatus','SmartController@changeStatus');
Route::post('tickets/editTicket','SmartController@editTicket');
Route::post('tickets/createTicket','SmartController@createTicket');
Route::post('objects/editObject','SmartController@editObject');
Route::post('media/makeMainImage','SmartController@makeMainImage');
Route::post('media/deleteImage','SmartController@deleteImage');
Route::post('scan_deals/deleteScan','SmartController@deleteScan');
Route::post('/developers/findDeveloper', 'SmartController@findDeveloper');
Route::post('clients/findClient','SmartController@findClient');
Route::put('/dadata', 'SmartController@dadata');
Route::get('/reports','SmartController@report')->name('reports');
Route::post('/report/get','SmartController@reportGet');
Route::get('/сontract/{id}','SmartController@сontractGet')->name('сontractGet');
Route::get('/report/download/{date_min}/{date_max}/{entity?}/{entity_id?}','SmartController@downloadReport')->name('downloadReport');
//подборки

Route::post('/collections/create','SmartController@createCollection');
Route::get('/collection/download/{id}','SmartController@downloadCollection')->name('downloadCollection');
//показы
Route::get('/shows/{tab?}', 'SmartController@showsList')->name('showsList');
Route::post('/shows/create', 'SmartController@createShows');
Route::post('/shows/set_datetime', 'SmartController@setDateTimeShow');
//загрузка
Route::post('/{object_id}/upload_media/','SmartController@upload_media');
Route::post('/{deal_id}/scan_deal_upload/','SmartController@scan_deal_upload');
