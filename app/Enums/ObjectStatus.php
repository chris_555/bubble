<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;
/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ObjectStatus extends Enum implements LocalizedEnum
{
    const Active = 1;
    const InDeal = 2;
    const Closed = 3;
}