<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Client extends Model
{   use SoftDeletes;
    public $timestamps = true;
    protected $table = 'clients';
    protected $fillable = [
        'name', 'login', 'password','first_name','second_name','last_name','date_of_birth','gender','phone'
    ];


    public static function GetShortInfo($id)
    {
        $client_array=array();
        $client = Client::find($id);
        $client_array['fullname'] = $client->second_name." ".$client->first_name." ".$client->last_name;
        $client_array['phone'] =User::formatPhone($client->phone);
        $client_array['id'] = $client->id;
        return $client_array;
    }

    public static function FindByPhone($phone)
    {

        $clients_id = Client::where('phone',$phone)->pluck('id')->toArray();
        $client_array=array();
        foreach ($clients_id as $client_id) {
            array_push($client_array,  Client::GetShortInfo($client_id));
        }
        return $client_array;
    }
    public static function GetClientsList($offset,$text)
    {
        $clients = Client::offset($offset);
        $words = explode(" ", $text);
        foreach($words as $word){
            $clients->where(function ($query) use ($word) {
                $query->where('first_name', 'ilike','%'.$word.'%')
                    ->orWhere('last_name', 'ilike', '%'.$word.'%')
                    ->orWhere('second_name', 'ilike', '%'.$word.'%')
                    ->orWhere('phone', 'ilike', '%'.$word.'%');;
            });
        }
        if((Auth::user()->position_id == 1))
        {
            $clients->leftJoin('tickets','tickets.client_id','clients.id')
                ->where('tickets.user_id',Auth::user()->id);
        }
        $clients=  $clients->limit(15)
            ->orderBy('second_name','asc')->get();
        $array = array();
        foreach($clients as $client)
        {
            array_push($array,array(
                'id'    => $client->id,
                'fio' => $client->GetFio(),
                'phone' => User::formatPhone($client->phone),
                'email' => $client->email,
                'created_at' => date_format(date_create($client->created_at),'d.m.Y' ),
            ));
        }

        return $array;
    }
    public static function GetClientsListCount($text)
    {
        $clients = Client:: where(function ($query) use ($text) {
                $query->where('first_name', 'ilike','%'.$text.'%')
                    ->orWhere('last_name', 'ilike', '%'.$text.'%')
                    ->orWhere('second_name', 'ilike', '%'.$text.'%')
                    ->orWhere('phone', 'ilike', '%'.$text.'%');;
            });
                 if((Auth::user()->position_id == 1))
                 {
                     $clients->leftJoin('tickets','tickets.client_id','clients.id')
                         ->where('tickets.user_id',Auth::user()->id);
                 }
        $clients= $clients ->count();
        return $clients;
    }

    public function GetFio()
    {
        return implode(" ",[$this->second_name,$this->first_name,$this->last_name]);
    }
}
