<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;

class Settlemet extends Model
{
    protected $table = 'settlements';
    protected $fillable = [
        'settlement_kladr','settlement_name','settlement_with_type',
    ];
}
