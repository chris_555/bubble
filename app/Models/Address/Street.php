<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $table = 'streets';
    protected $fillable = [
        'city_id','settelemt_id','street_name','street_kladr', 'street_with_type',
    ];
}
