<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;
use App\Models\Address\{City, Settlemet , Street};
class Address extends Model
{
    protected $table = 'addresses';
    protected $fillable = [
        'block','house','house_kladr','geo_lon', 'geo_lat', 'street_id','value',
    ];
    public static function getAddressesStreet($id)
    {
        $street_id = Address::where('id',$id)->pluck('street_id')->first();
        $streets_array = Address::where('street_id', $street_id) ->pluck('id')->toArray();
        return $streets_array;
    }

    public static function get_id_or_create($address)
    {
        $id_return ="";
        if ($address['data']) {

            $street_id = null;
            $city_id = City::firstOrCreate([
                'city_name' => $address['data']['city'],
                'city_kladr' => $address['data']['city_kladr_id'],
                'city_with_type' => $address['data']['city_with_type'],
            ])->id;

            if ($address['data']['settlement'] != null) {
                $settlement = Settlemet::firstOrCreate([
                    'settlement_name' => $address['data']['settlement'],
                    'settlement_kladr' => $address['data']['settlement_kladr_id'],
                    'settlement_with_type' => $address['data']['settlement_with_type'],
                ]);
            }

            if ($address['data']['street'] != null) {
                $street_id = Street::firstOrCreate([
                    'street_name' => $address['data']['street'],
                    'street_kladr' => $address['data']['street_kladr_id'],
                    'city_id' => $city_id,
                    'street_with_type' => $address['data']['street_with_type'],
                ])->id;
            }
                $id_return =   Address::firstOrCreate([
                    'block' => $address['data']['block'],
                    'house' => $address['data']['house'],
                    'value' => $address['value'],
                    'geo_lon' => $address['data']['geo_lon'],
                    'geo_lat' => $address['data']['geo_lat'],
                    'street_id' => $street_id ?? null,
                    'house_kladr' => $address['data']['house_kladr_id']])->id;


        }
        return $id_return;
    }

    public static function put_addresses($array)
    {
        if($array){
            foreach ($array as $address) {
                $street_id=null;
                if($address['data']){
                    $city_id= City::firstOrCreate([
                        'city_name' => $address['data']['city'],
                        'city_kladr' => $address['data']['city_kladr_id'],
                        'city_with_type' => $address['data']['city_with_type'],
                    ])->id;
                    if($address['data']['settlement']!=null) {
                        $settlement = Settlemet::firstOrCreate([
                            'settlement_name' => $address['data']['settlement'],
                            'settlement_kladr' => $address['data']['settlement_kladr_id'],
                            'settlement_with_type' => $address['data']['settlement_with_type'],
                        ]);
                    }
                    if($address['data']['street']!=null) {
                        $street_id = Street::firstOrCreate([
                            'street_name' => $address['data']['street'],
                            'street_kladr' => $address['data']['street_kladr_id'],
                            'city_id' => $city_id,
                            'street_with_type' => $address['data']['street_with_type'],
                        ])->id;
                    }

                            Address::firstOrCreate([
                                'block' => $address['data']['block'],
                                'house' => $address['data']['house'],
                                'value'=>$address['value'],
                                'geo_lon' => $address['data']['geo_lon'],
                                'geo_lat' => $address['data']['geo_lat'],
                                'street_id'=>$street_id,
                                'house_kladr'=>$address['data']['house_kladr_id']]);


                }
            }
        }
    }
}
