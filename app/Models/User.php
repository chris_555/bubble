<?php

namespace App\Models;
use App\Models\Ticket\Ticket;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'password','first_name','second_name','last_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function GetShortInfo($id)
    {
        $user_array=array();
        $user = User::find($id);
        $user_array['id'] = $user->id;
        $user_array['fullname'] = $user->name;
        $user_array['initials'] = $user->second_name.' '.mb_substr($user->first_name,0,1).'.'.mb_substr($user->last_name,0,1);
        $user_array['phone'] = User::formatPhone($user->phone);
        $user_array['email'] = $user->email;
        return $user_array;
    }
    public static function searchUsersInTab($text,$offset){

        $words = explode(" ", $text);
            $tickets = DB::table('users')
                ->select('users.id')->orderByDesc('id');
        foreach($words as $word){
            $tickets->where(function ($query) use ($word) {
                $query->where('first_name', 'ilike','%'.$word.'%')
                    ->orWhere('last_name', 'ilike', '%'.$word.'%')
                    ->orWhere('second_name', 'ilike', '%'.$word.'%')
                    ->orWhere('phone', 'ilike', '%'.$word.'%');;
            });
        }
        $tickets = $tickets ->offset($offset)->limit(15)->get();

        $arrayTickets = array();
        foreach($tickets as $ticket)
        {
            array_push($arrayTickets,User::GetShortInfo($ticket->id));
        }
        return $arrayTickets;
    }

    public static function searchUsersCount($text){
        $words = explode(" ", $text);
        $tickets = DB::table('users')->orderByDesc('id');
        foreach($words as $word){
            $tickets->where(function ($query) use ($word) {
                $query->where('first_name', 'ilike','%'.$word.'%')
                    ->orWhere('last_name', 'ilike', '%'.$word.'%')
                    ->orWhere('second_name', 'ilike', '%'.$word.'%')
                    ->orWhere('phone', 'ilike', '%'.$word.'%');;
            });
        }
        $tickets =$tickets ->count();
        return $tickets;
    }
    public static function num2str($sourceNumber){

            $smallNumbers=array(
                array('ноль'),
                array('','один','два','три','четыре','пять','шесть','семь','восемь','девять'),
                array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать',
                    'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать'),
                array('','','двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят','восемьдесят','девяносто'),
                array('','сто','двести','триста','четыреста','пятьсот','шестьсот','семьсот','восемьсот','девятьсот'),
                array('','одна','две')
            );
            $degrees=array(
                array('дофигальон','','а','ов'), //обозначение для степеней больше, чем в списке
                array('тысяч','а','и',''), //10^3
                array('миллион','','а','ов'), //10^6
                array('миллиард','','а','ов'), //10^9
                array('триллион','','а','ов'), //10^12
                array('квадриллион','','а','ов'), //10^15
                array('квинтиллион','','а','ов'), //10^18
                array('секстиллион','','а','ов'), //10^21
                array('септиллион','','а','ов'), //10^24
                array('октиллион','','а','ов'), //10^27
                array('нониллион','','а','ов'), //10^30
                array('дециллион','','а','ов') //10^33
                //досюда написано в Вики по нашей короткой шкале: https://ru.wikipedia.org/wiki/Именные_названия_степеней_тысячи
            );

            if ($sourceNumber==0) return $smallNumbers[0][0]; //Вернуть ноль
            $sign = '';
            if ($sourceNumber<0) {
                $sign = 'минус ';
                $sourceNumber = substr ($sourceNumber,1);
            }
            $result=array();

            $digitGroups = array_reverse(str_split(str_pad($sourceNumber,ceil(strlen($sourceNumber)/3)*3,'0',STR_PAD_LEFT),3));
            foreach($digitGroups as $key=>$value){
                $result[$key]=array();

                foreach ($digit=str_split($value) as $key3=>$value3) {
                    if (!$value3) continue;
                    else {
                        switch ($key3) {
                            case 0:
                                $result[$key][] = $smallNumbers[4][$value3];
                                break;
                            case 1:
                                if ($value3==1) {
                                    $result[$key][]=$smallNumbers[2][$digit[2]];
                                    break 2;
                                }
                                else $result[$key][]=$smallNumbers[3][$value3];
                                break;
                            case 2:
                                if (($key==1)&&($value3<=2)) $result[$key][]=$smallNumbers[5][$value3];
                                else $result[$key][]=$smallNumbers[1][$value3];
                                break;
                        }
                    }
                }
                $value*=1;
                if (!$degrees[$key]) $degrees[$key]=reset($degrees);

                if ($value && $key) {
                    $index = 3;
                    if (preg_match("/^[1]$|^\\d*[0,2-9][1]$/",$value)) $index = 1; //*1, но не *11
                    else if (preg_match("/^[2-4]$|\\d*[0,2-9][2-4]$/",$value)) $index = 2; //*2-*4, но не *12-*14
                    $result[$key][]=$degrees[$key][0].$degrees[$key][$index];
                }
                $result[$key]=implode(' ',$result[$key]);
            }
            return $sign.implode(' ',array_reverse($result));
        }

    public static function morph($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
    }
    public static function formatPhone($phone) {
        if (empty($phone)) return "";//7 922 796 0195
        sscanf($phone, "7%3d%3d%4d", $area, $prefix, $exchange);
        $out = @$area ? "+7 ($area) " : "";
        $out .= $prefix . '-' . $exchange;
        return $out;
    }
}
