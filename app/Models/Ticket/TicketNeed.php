<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketNeed extends Model
{
    use SoftDeletes;
    protected $table = 'ticketNeeds';
    protected $fillable = [
        'field_id','ticket_id','value',
    ];

}
