<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScanDeal extends Model
{
    use SoftDeletes;
    protected $table = 'scan_deals';
    protected $fillable = [
        'deal_id','name', 'path'
    ];

}
