<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Collection extends Model
{
    protected $casts = [
        'collection' => 'array',
    ];
    protected $table = 'collections';
    protected $fillable = [
        'ticket_id','collection'
    ];
    public static function GetCollection($_id){
        $collection =Collection::select('id','collection','created_at')
            ->where('id',$_id)
            ->get();
        $arrayTickets = array();
        foreach($collection as $tickets)
        {
            $arrayTickets[$tickets->id]['created_at'] =  Date::parse($tickets->created_at)->format('j F Y г.')  ;
            $arrayTickets[$tickets->id]['id'] = $tickets->id ;

            foreach($tickets['collection'] as $ticket) {
                $ticket = Ticket::GetTicketShort($ticket);
                $arrayTickets[$tickets->id]['tickets'][] = $ticket;
            }
        }
        return $arrayTickets;
    }
}
