<?php

namespace App\Models\Ticket;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
class Comment extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'comments';
    protected $fillable = ['text', 'ticket_id', 'user_id','created_at','deleted_at'];

    public static function GetComments($ticketId)
    {
        $comments = Comment::where('ticket_id',  $ticketId)->get();
        $array =array();
        foreach($comments as $comment)
        {
            array_push($array,array(
                'text' => $comment->text,
                'user' => User::GetShortInfo($comment->user_id),
                'created_at' => Date::parse($comment->created_at)->format('j F Y в H:i'),
            ));
        }

           $params['comments'] =array(
               'ticket_id' => $ticketId,
               'comments' => $array,

        );
        return   $params['comments'] ;
    }


    public static function GetLastComment($ticketId)
    {

        $comment = Comment::where('ticket_id', $ticketId)->orderBy('created_at', 'DESC')->first();
        if ($comment) {
            $params['comment'] = array(
                    'text' => $comment->text,
                    'user' => User::GetShortInfo($comment->user_id),
                    'created_at' => Date::parse($comment->created_at)->format('j F Y в H:i'),
            );
            return $params['comment'];
        }
        return null;
    }


}
