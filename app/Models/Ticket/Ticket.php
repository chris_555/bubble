<?php

namespace App\Models\Ticket;
use App\Models\Catalog\CatalogItems;
use App\Models\Object\{Objects,Media};
use App\Models\{Address\Address, Client, Favorites, User};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\SoftDeletes;
class Ticket extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    public static function  CreateCoolectionFromNeed($ticket_id){
        $tickets = Ticket::whereNotNull('object_id')->select('tickets.id')
            ->leftjoin('objects','objects.id','=','object_id');

        $filter = TicketNeed::where('ticket_id',$ticket_id)
            ->leftjoin('needFields','needFields.id','=','field_id')->get()->toArray();

        foreach ($filter as $param) {
            if (strpos($param['title'], '-')) {
                $key = explode('-', $param['title']);
                if ($key[1] == "min") $tickets->where($key[0], '>', $param['value']);
                if ($key[1] == "max") $tickets->where($key[0], '<', $param['value']);
            }
            else if($param['source']=="catalog") {

             //   $tickets->leftjoin('entityCatalogItems', 'entityCatalogItems.entity_id', '=', 'object_id');
              //  $tickets->where('item_id', $param['source']);
            }
            else if ($param['title'] =="objectClass") {
                if($param['value'] ==2) $tickets->where('objects.type', '!=',6);
                if($param['value'] ==1) $tickets->where('objects.type', '=',6);
            }
            else if($param['title'] =='address' ) {
                $street_array = Address::getAddressesStreet($param['value']);
                $tickets->whereIn('address_id', $street_array);
            }
            else if (is_array(explode(',',$param['value'])))$tickets->whereIn($param['title'], explode(',',$param['value']));
            else $tickets->where($param['title'], $param['value']);
        }
        $tickets = $tickets->get();
 if(count($tickets)>0)
 {
     $array =array();
     foreach ($tickets as $ticket)
     {
     array_push($array,$ticket->id);
     }
       $coll = new Collection();
       $coll->ticket_id = $ticket_id;
       $coll->collection = $array;
       $coll->save();
 }
       return $tickets;
    }
    public static function GetCollection($ticket_id){
        $collection =Collection::select('id','collection','created_at')
            ->where('ticket_id',$ticket_id)
            ->orderBy('created_at', 'desc')
            ->get();
        $arrayTickets = array();
        foreach($collection as $tickets)
        {
            $arrayTickets[$tickets->id]['created_at'] =  Date::parse($tickets->created_at)->format('j F Y г. H:i')  ;
            $arrayTickets[$tickets->id]['id'] = $tickets->id ;

            foreach($tickets['collection'] as $ticket) {
                $ticket = Ticket::GetTicketShort($ticket);
                $arrayTickets[$tickets->id]['tickets'][] = $ticket;
            }
        }
        return $arrayTickets;
    }
    public static function GetTicketShort($id){
        $ticket= Ticket::find($id);
        $items= array();
        if($ticket->price !=null)
            $items['price'] =   number_format($ticket->price, 0, ' ', ' ').' руб.';
        else
            $items['price']='';
        $items['type'] =   CatalogItems::getItemByValue('ticketTypes', $ticket->type)['title'];
        $items['title'] =  $ticket->title;
        $items['id']  =  $ticket->id;
        $items['object_id'] =  $ticket->object_id;
        $items['object'] = Objects::GetShortParamsForTcket( $ticket->object_id);
        return $items;
}
    public static function findMyTicket($ticket_id, $user_id)
    {
        $tickets =Ticket::select('id', DB::raw("concat('#',id,' ',title) as name"))
            ->where('id', 'ilike', '%' . $ticket_id . '%')
            ->where('user_id',  $user_id)
            ->where('type',  2)
            ->limit(5)->get()->toArray();
        return $tickets;
    }
    public static function ClientDocs($id){
        $documents_client=  CatalogItems::getItemByCatalogName('documents_client');
        $client =Client::whereId($id)->first()->toArray();
        $array= array();
        foreach($documents_client as $item)
        {
            $array[$item['item']['block']][] = array(
                'value'=>$client[$item['item']['name']],
                'title' => $item['item']['title'],
                'name' => $item['item']['name'],
                'type' => $item['item']['type'] ?? "text",
            );
        }
        return $array;
    }
    public static function clientTickets($client_id)
    {
        $tickets =Ticket::select('tickets.id')
            ->where('tickets.client_id',$client_id)
            ->get();
        $arrayTickets = array();
        foreach($tickets as $ticket)
        {
            array_push($arrayTickets,Ticket::getTicket($ticket->id));
        }
        return $arrayTickets;
    }
    public static function searchTicketsInTab($text,$status,$offset)
    {
        $userId = Auth::user()->id;
        if($status==5){
            $array_fav= Favorites::GetFavorites($userId,3);
            $tickets =  DB::table('tickets')
                ->select('tickets.id')
                ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
                ->whereIn('tickets.id',$array_fav);
            if((Auth::user()->position_id == 1)) {
                $tickets ->
                where('tickets.user_id', $userId);
            }
            $tickets =  $tickets->where('tickets.deleted_at', NULL)
                ->where(function ($query) use ($text) {
                    $query->where('tickets.title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->offset($offset)->limit(15)->get();
        }
        else if($status==4){
            $tickets =  DB::table('tickets')
                ->select('tickets.id')
                ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
                ->where('tickets.status',$status);
            if((Auth::user()->position_id == 1)) {
                $tickets ->
                where('tickets.user_id', $userId);
            }

            $tickets =  $tickets ->where(function ($query) use ($text) {
                    $query->where('tickets.title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->offset($offset)->limit(15)->get();
        }
        else {
            $tickets = DB::table('tickets')
                ->select('tickets.id')
                ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id');
            if((Auth::user()->position_id == 1)) {
                $tickets -> where('tickets.user_id', $userId);
            }

            $tickets = $tickets ->where('tickets.status',$status)
                ->where('tickets.deleted_at', NULL)
                ->where(function ($query) use ($text) {
                    $query->where('tickets.title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->offset($offset)->limit(15)->get();
        }
        $arrayTickets = array();
        foreach($tickets as $ticket)
        {
            array_push($arrayTickets,Ticket::getTicket($ticket->id));
        }
        return $arrayTickets;
    }
    public static function searchTicketsCount($text)
    {
        $user_id = Auth::user()->id;
        $array_count = array();
        $array_fav= Favorites::GetFavorites($user_id,3);
        $tickets = DB::table('tickets')
            ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
            ->whereIn('tickets.id',$array_fav);

        if((Auth::user()->position_id == 1)) {
            $tickets ->
            where('tickets.user_id', $user_id);
        }

        $tickets = $tickets ->where('tickets.deleted_at', NULL)
                ->where(function ($query) use ($text) {
                    $query->where('tickets.title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->count();
        $array_count['5']=$tickets;
        $tickets = DB::table('tickets')
            ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
            ->where('status',4);
                 if((Auth::user()->position_id == 1)) {
                     $tickets ->
                     where('tickets.user_id', $user_id);
                 }

        $tickets = $tickets ->where(function ($query) use ($text) {
                    $query->where('title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->count();
        $array_count['4']=$tickets;
        $tickets = DB::table('tickets')
            ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
            ->where('status',3);
                  if((Auth::user()->position_id == 1)) {
                      $tickets ->
                      where('tickets.user_id', $user_id);
                  }

        $tickets = $tickets ->where('tickets.deleted_at', NULL)
                ->where(function ($query) use ($text) {
                    $query->where('title', 'ilike','%'.$text.'%')
                        ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                        ->orwhere('first_name', 'ilike','%'.$text.'%')
                        ->orWhere('last_name', 'ilike', '%'.$text.'%')
                        ->orWhere('second_name', 'ilike', '%'.$text.'%');
                })
                ->count();
        $array_count['3']=$tickets;
        $tickets = DB::table('tickets')
            ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
            ->where('status',2);
              if((Auth::user()->position_id == 1)) {
                  $tickets ->
                  where('tickets.user_id', $user_id);
              }

        $tickets = $tickets  ->where('tickets.deleted_at', NULL)
            ->where(function ($query) use ($text) {
                $query->where('title', 'ilike','%'.$text.'%')
                    ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                    ->orwhere('first_name', 'ilike','%'.$text.'%')
                    ->orWhere('last_name', 'ilike', '%'.$text.'%')
                    ->orWhere('second_name', 'ilike', '%'.$text.'%');
            })
            ->count();
        $array_count['2']=$tickets;
        $tickets = DB::table('tickets')
            ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
            ->where('status',1);
             if((Auth::user()->position_id == 1)) {
                 $tickets ->
                 where('tickets.user_id', $user_id);
             }

        $tickets = $tickets
            ->where('tickets.deleted_at', NULL)
            ->where(function ($query) use ($text) {
                $query->where('title', 'ilike','%'.$text.'%')
                    ->orWhere('tickets.id', 'ilike', '%'.$text.'%')
                    ->orwhere('first_name', 'ilike','%'.$text.'%')
                    ->orWhere('last_name', 'ilike', '%'.$text.'%')
                    ->orWhere('second_name', 'ilike', '%'.$text.'%');
            })
            ->count();
        $array_count['1']=$tickets;
        return $array_count;
    }
    public static function GetMyTicketList($offset,$status)
    {
        $userId = Auth::user()->id;
        if($status==5){
            $array_fav= Favorites::GetFavorites($userId,3);
            $tickets = Ticket::whereIn('id',$array_fav)->where('user_id',$userId)->where('deleted_at', NULL)->offset($offset)->limit(15)->get();
        }
        else if($status==4){
            $tickets = Ticket::where('status',$status)->where('user_id',$userId)->offset($offset)->limit(15)->get();
        }
        else {
            $tickets = Ticket::where('status',$status)->where('user_id',$userId)->where('deleted_at', NULL)->offset($offset)->limit(15)->get();
        }
        $arrayTickets = array();
        foreach($tickets as $ticket)
        {
            array_push($arrayTickets,$ticket->getTicket($ticket->id));
        }
        return $arrayTickets;
    }
    public static function GetMyTicketListCount($offset,$status)
    {
        $userId = Auth::user()->id;
        if($status==5){
            $array_fav= Favorites::GetFavorites($userId,3);
            $allcount=  Ticket::whereIn('id',$array_fav)->where('user_id',$userId)->where('deleted_at', NULL)->count();

        }
        else if($status==4){
            $allcount= Ticket::where('status',$status)->where('user_id',$userId)->count();
        }
        else {
            $allcount= Ticket::where('status',$status)->where('user_id',$userId)->where('deleted_at', NULL)->count();
        }
        return $allcount;
    }
    public static function GetTicketType($id)
    {
        $ticket_type= Ticket::withTrashed()
            ->where('id', $id)
            ->pluck('type')
            ->first();

        return $ticket_type;
    }
    public static function GetTicketStatusStage($id)
    {
        $ticket= Ticket::withTrashed()
            ->where('id', $id)
            ->first();
        $params =array();
        $params['status'] =CatalogItems::getItemByValue('ticketStatus', $ticket->status);
        $params['stage'] =CatalogItems::getItemByValueAndType('ticketStages', $ticket->stage,$ticket->type);
        return $params;
    }
    public function GetTicketWithObject($id)
    {
        $ticket= Ticket::find($id);;
        $params =array();
        $items= array();

       // foreach( $this->CatalogsItem as $CatalogsItem)
       // {
        //    $items[ strval(CatalogItems::find($CatalogsItem->item_id)->catalogName->name)] =  7;
             //   CatalogItems::find($CatalogsItem->item_id)->item['title'];
      //  }
        $params['stage'] =CatalogItems::getItemByValueAndType('ticketStages', $ticket->stage,$ticket->type);
        $items['description'] =  $ticket->description;
        $items['price'] =  number_format( $ticket->price, 0, ' ', ' ').' руб.';
        $items['type'] =  CatalogItems::find($ticket->type)->item['title'];
        $params['params']  =$items;
        $params['client'] = Client::GetShortInfo($ticket->client_id);
        $params['user'] = User::GetShortInfo($ticket->user_id);
        $params['id'] = $ticket->id;
        $params['lastComment'] = Comment::GetLastComment($ticket->id);
        return $params;
    }
    public static function CountTicketByStatus()
    {
        $userId = Auth::user()->id;
        $array =array();
        $array['inWork'] = Ticket::where('deleted_at', NULL)->where('status',1)->where('user_id',$userId)->count();
        $array['canceled'] = Ticket::where('deleted_at', NULL)->where('status',2)->where('user_id',$userId)->count();
        $array['deleted'] = Ticket::where('status',4)->where('user_id',$userId)->count();
        $array['completed'] = Ticket::where('deleted_at', NULL)->where('status',3)->where('user_id',$userId)->count();
        $array['favorites'] = Ticket::where('deleted_at', NULL)->whereIn('id',Favorites::GetFavorites($userId,3))->where('user_id',$userId)->count();
        return $array;
    }
    public static function GetTicket($id)
    {
        $ticket= Ticket::withTrashed()
            ->where('id', $id)
            ->first();
        $params =array();
        $items= array();
        //foreach( $ticket->CatalogsItem as $CatalogsItem)
       // {
      //      $items[ strval(CatalogItems::find($CatalogsItem->item_id)->catalogName->name)] =  CatalogItems::find($CatalogsItem->item_id)->item['title'];
      //  }
        $items['description'] =  $ticket->description;
        if($ticket->price !=null)
            $items['price'] =   number_format($ticket->price, 0, ' ', ' ').' руб.';
        else
            $items['price']='';
        $items['type'] =   CatalogItems::getItemByValue('ticketTypes', $ticket->type);
        $items['title'] =  $ticket->title;
        $revenue =null;
        if($ticket->type==1)
        {
            if(Deal::where('ticket_sell_id',$ticket->id)->get()->count()!=0)
                $revenue = Deal::where('ticket_sell_id',$ticket->id)->pluck('price')[0];
            else $revenue = $ticket->price;
        }
        else {
            if(Deal::where('ticket_buy_id',$ticket->id)->get()->count()!=0)
                $revenue =Deal::where('ticket_buy_id',$ticket->id)->pluck('price')[0];
            else {
                $id_max = NeedFields::where('title','price-max')->pluck('id')[0];
                $id_min = NeedFields::where('title','price-min')->pluck('id')[0];
                $max =TicketNeed::where('ticket_id',$ticket->id)->where('field_id', $id_max)->pluck('value');
                if(count($max) !=0)
                    $max =$max[0];else $max=0;
                $min =TicketNeed::where('ticket_id',$ticket->id)->where('field_id', $id_min)->pluck('value');
                if(count($min) !=0)
                    $min =$min[0]; else $min=0;
                if($max>0 && $min>0)
                    $revenue = (floatval($max)+floatval($min))/2;
                else if($max=0 || $min =0)
                    $revenue = ($max+$min);
                else $revenue = 0;
            }
        }
        $params['revenue'] = number_format($revenue * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'], 0, ' ', ' ').' руб.';
        $params['id']  =  $ticket->id;
        $params['params']  =$items;
        $params['client'] = Client::GetShortInfo($ticket->client_id);
        $params['user'] = User::GetShortInfo($ticket->user_id);
        $params['status'] =CatalogItems::getItemByValue('ticketStatus', $ticket->status);
        $params['stage'] =CatalogItems::getItemByValueAndType('ticketStages', $ticket->stage,$ticket->type);
        $params['created_at'] = Date::parse( $ticket->created_at)->format('j F Y г, H:i');
        $params['lastComment'] = Comment::GetLastComment($ticket->id);
        if($ticket->object_id >=1) {
            $params['object_id'] =  $ticket->object_id;
            $params['object'] = Objects::GetShortParamsForTcket( $ticket->object_id);
        }
        return $params;
    }
    public function CatalogsItem()
    {
        return $this->hasMany('App\Models\Catalog\EntityCatalogItems', 'entity_id', 'id');
    }
    public function GetClient()
    {
        return  $this->belongsTo('App\Models\Client', 'client_id', 'id');
    }
    public function GetComments()
    {
        return $this->hasMany('App\Models\Ticket\Comment', 'ticket_id', 'id');
    }
    public function getMyEnum()
    {
        return CatalogItems::getEnumValue('ticket');
    }
    public static function setStatus($id,$status)
    {
        if($status ==4){
        Ticket::where('id', $id)->update(['status'=>$status]);
        Ticket::where('id', $id)->delete();
        return 'success deleted';
        }
        else
        {
            Ticket::where('id', $id)->update(['status'=>$status]); return 'success changed status';
        }
        return 'bad, very bad!';
    }
    public static function setStage($id,$stage)
    {

        Ticket::where('id', $id)->update(['stage'=>$stage]); return 'success changed status';

    }
}
