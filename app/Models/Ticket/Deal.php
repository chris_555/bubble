<?php

namespace App\Models\Ticket;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deal extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'deals';
    protected $fillable = ['ticket_buy_id','deposit','day_payment', 'ticket_sell_id', 'price','date'];

    public static function getDeal($param,$id){
        $deal =Deal::where($param,$id)->first();
        if($deal!="")
        {
            $deal->toArray();
            $deal['price'] = number_format( $deal['price'], 2, ',', ' ');
            $deal['object_id'] = Ticket::whereId($deal['ticket_sell_id'])->pluck('object_id')[0];
            $deal['date']  =  DateTime::createFromFormat('Y-m-d', $deal['date'])->format('d-m-Y');
            return $deal;
        }
        return null;
    }
}
