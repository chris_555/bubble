<?php

namespace App\Models\Ticket;
use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Show extends Model
{
    protected $table = 'shows';
    protected $fillable = [
        'ticket_buy_id','ticket_sell_id','date_time'
    ];
    public static function GetMyShows($tab,$user_id){
        $array =array();
        $mytickets = Ticket::where('user_id',$user_id)->pluck('id')->toArray();
        if($tab==1) {
            $shows = Show::whereIn('ticket_buy_id', $mytickets)->orderBy('date_time', 'ASC')->get()->toArray();
            foreach ($shows as $show) {
                $user_id_2side = Ticket::find($show['ticket_sell_id'])->user_id;
                $array[$show['ticket_buy_id']]['client'] =
                    Client::GetShortInfo(Ticket::find($show['ticket_buy_id'])->client_id);
                $show['date_time'] ==null? $datetime=null : $datetime =Date::parse( $show['date_time'])->format('j F Y г, H:i');

                $array[$show['ticket_buy_id']]['shows'][] = array(
                    'id' => $show['id'],
                    'date_time' => $datetime,
                    'ticket_buy_id' => $show['ticket_buy_id'],
                    'ticket_sell_id' => $show['ticket_sell_id'],
                    'ticket_sell' => Ticket::GetTicketShort($show['ticket_sell_id']),
                    'user_sell' => User::GetShortInfo($user_id_2side),
                );
            }

        }
            if ($tab == 2) {
                $shows = Show::whereIn('ticket_sell_id', $mytickets)->get()->toArray();
            foreach ($shows as $show) {
                $show['date_time'] ==null? $datetime=null : $datetime =Date::parse( $show['date_time'])->format('j F Y г, H:i');
                $user_id_2side = Ticket::find($show['ticket_buy_id'])->user_id;
                $array[] = array(
                    'id' => $show['id'],
                    'date_time' => $datetime,
                    'ticket_buy_id' => $show['ticket_buy_id'],
                    'ticket_sell_id' => $show['ticket_sell_id'],
                    'ticket_sell' => Ticket::GetTicketShort($show['ticket_sell_id']),
                    'user_buy' => User::GetShortInfo($user_id_2side),

                );
            }
        }
        return $array;
    }
}
