<?php

namespace App\Models\Ticket;

use App\Models\Catalog\CatalogItems;
use Illuminate\Database\Eloquent\Model;

class NeedFields extends Model
{
    protected $table = 'needFields';
    protected $fillable = [
        'description','title'
    ];

    public static function get_need_fields($with_values=false,$ticket_id){

    $fields_array =array();
        $fields = NeedFields::all();
        foreach ($fields as $field) {
            $value = TicketNeed::where('field_id', $field->id)->where('ticket_id', $ticket_id)->pluck('value')->first();

            if(($field->title == 'objectClass' || $field->title == 'objectKeep') && $value !="")
            {
                if(!$with_values) $value = CatalogItems::getItemByValue($field->title,  (integer) $value)['title'];
                else $value = CatalogItems::getItemByValue($field->title,  (integer) $value)['value'];
            }
            if($field->title == 'num_rooms'  && $value !="")
            {
                $value = explode(",", $value) ;
            }
            $fields_array[$field->title] = array(
                'title' => $field->title,
                'description' => $field->description,
                'value' => $value,
            );

        }

        if($with_values) {
            $values = CatalogItems::getItemByCatalogName('objectKeep');
            array_push($values, array('item' => array('title' => "Любой", "value" => null)));
            $fields_array ['objectKeep']['values']= $values;

            $values = CatalogItems::getItemByCatalogName('objectClass');
            array_push($values, array('item' => array('title' => "Любой", "value" => null)));
            $fields_array ['objectClass']['values'] = $values;

    }
        return $fields_array;
    }
}
