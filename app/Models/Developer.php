<?php

namespace App\Models;

use App\Models\Address\Address;
use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $casts = [
        'phones' => 'array',
    ];
    protected $table = 'developers';
    protected $fillable = [
        'fisrt_name','inn','kpp','ogrn','address_id','date_registration','phones', 'last_name', 'name_of_org','second_name',
    ];

    public static function GetDevelopersList($offset,$text)
    {
        $developers = Developer::offset($offset)
            ->where(function ($query) use ($text) {
                $query->where('first_name', 'ilike','%'.$text.'%')
                    ->orWhere('last_name', 'ilike', '%'.$text.'%')
                    ->orWhere('second_name', 'ilike', '%'.$text.'%')
                    ->orWhere('name_of_org', 'ilike', '%'.$text.'%');;
            })
            ->limit(15)
            ->orderBy('name_of_org','ASC')->get();
        $array = array();
        foreach($developers as $developer)
        {
            array_push($array,array(
                'id'    => $developer->id,
                'fio' => $developer->GetFio(),
                'name_of_org' => $developer->name_of_org,
                'full_name_of_org' => $developer->full_name_of_org,
                'created_at' => date_format(date_create($developer->created_at),'d.m.Y' ),
            ));
        }
        return $array;
    }
    public static function GetDeveloper($id)
    {
        if($id>0) {
            $array = array();
            $developer = Developer::whereId($id)->first();
            $array['id'] = $developer->id;
            $array['first_name'] = $developer->first_name;
            $array['second_name'] = $developer->second_name;
            $array['last_name'] = $developer->last_name;
            $array['name_of_org'] = $developer->name_of_org;
            $array['inn'] = $developer->inn;
            $array['ogrn'] = $developer->ogrn;
            $array['kpp'] = $developer->kpp;
            $array['address_id'] = Address::find($developer->address_id)->value;
            $array['date_registration'] = $developer->date_registration;
            $array['full_name_of_org'] = $developer->full_name_of_org;
            $array['phones'] = $developer->phones;
            $array['created_at'] = date_format(date_create($developer->created_at), 'd.m.Y');
            return $array;
        }
    }

    public static function GetDevelopersListCount($text)
    {
        $developers = Developer:: where(function ($query) use ($text) {
            $query->where('first_name', 'ilike','%'.$text.'%')
                ->orWhere('last_name', 'ilike', '%'.$text.'%')
                ->orWhere('second_name', 'ilike', '%'.$text.'%')
                ->orWhere('name_of_org', 'ilike', '%'.$text.'%');;
        })
            ->count();
        return $developers;
    }
    public function GetFio()
    {
        return implode(" ",[$this->first_name,$this->second_name,$this->last_name]);
    }
}
