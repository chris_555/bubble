<?php

namespace App\Models\Object;
use App\Models\{Address\Street, Developer, User};
use App\Models\Ticket\Ticket;
use Illuminate\Database\Eloquent\Model;
use App\Models\Address\Address;
use App\Models\Catalog\{CatalogItems,Catalog};

class Objects extends Model
{

    protected $table = 'objects';
    protected $fillable = [
        'year_construct','area', 'flat', 'floor','num_rooms','type',
    ];
    public static function GetCoords($offset,$tab,$user_id,$text,$filter =null)
    {
        $array = array();
        if($tab==1) {
            $tickets = Ticket::whereNotNull('object_id')->select('tickets.id','price', 'object_id')
                ->whereIn('tickets.status', [1, 2, 3]);
                if($text!='' && $text !=null )
                    $tickets->where('tickets.object_id',$text );
            if($filter!=null){
                $tickets->leftjoin('objects','objects.id','=','object_id');
                foreach ($filter as $key=>$param) {
                    if (strpos($key, '-')) {
                        $key = explode('-', $key);
                        if ($key[1] == "min") $tickets->where($key[0], '>', $param);
                        if ($key[1] == "max") $tickets->where($key[0], '<', $param);
                    }
                    else if($key=="catalog") {
                        $tickets->leftjoin('entityCatalogItems', 'entityCatalogItems.entity_id', '=', 'object_id');
                        foreach ($param as $par) {
                            $tickets->where('item_id', $par);
                        }
                    }
                    else if (is_array($param))$tickets->whereIn($key, $param);
                    else $tickets->where($key, $param);
                }
            }
            $tickets =$tickets->distinct('object_id')->offset($offset)
                ->limit(10)->get();
            foreach ($tickets as $ticket) {
                $object = Objects::find($ticket->object_id);
                array_push($array, array(
                    'id' => $object->id,
                    'ticket_id' => $ticket->id,
                    'price' =>  number_format( $ticket->price, 0, ' ', ' ').' руб.',
                    'coords' => $object->GetObjectCoords(),
                    'params' => Objects::GetShortParamsForTcket($object->id),
                ));
            }
        }
        if($tab==2){
            $tickets = Ticket::whereNotNull('object_id')->select('tickets.id', 'object_id')
                ->where('tickets.type',1)
                ->where('user_id',$user_id);
                  if($text!='' && $text !=null )
                      $tickets->where('tickets.object_id',$text );
            $tickets ->whereIn('status', [1, 2, 3]);
            if($filter!=null){
                $tickets->leftjoin('objects','objects.id','=','object_id');
                foreach ($filter as $key=>$param) {
                    if (strpos($key, '-')) {
                        $key = explode('-', $key);
                        if ($key[1] == "min") $tickets->where($key[0], '>', $param);
                        if ($key[1] == "max") $tickets->where($key[0], '<', $param);
                    }
                    else if($key=="catalog") {
                        $tickets->leftjoin('entityCatalogItems', 'entityCatalogItems.entity_id', '=', 'object_id');
                        foreach ($param as $par) {
                            $tickets->where('item_id', $par);
                        }
                    }
                    else $tickets->where($key, $param);
                }
            }
            $tickets ->distinct('object_id')->offset($offset)
                ->limit(10)->get();
            foreach ($tickets as $ticket) {
                $object = Objects::find($ticket->object_id);
                array_push($array, array(
                    'id' => $object->id,
                    'ticket_id' => $ticket->id,
                    'price' =>  number_format( $ticket->price, 0, ' ', ' ').' руб.',
                    'coords' => $object->GetObjectCoords(),
                    'params' => Objects::GetShortParamsForTcket($object->id),
                ));
            }
        }

        return $array;
    }
    public function GetObjectCoords()
    {
        $items= array();
        $adr_id = Address::find($this->address_id);
        $items['geo_lon'] = $adr_id->geo_lon;
        $items['geo_lat'] =  $adr_id->geo_lat;
        return $items;
    }
    public function GetShortParams()
    {
        $items= array();
        foreach( Objects::get_catalogsItems($this->id) as $catalogsItem)
        {
            //$value= $catalogsItem['catalog_title'];
            $items[$catalogsItem['catalog_title']] = array( 'title' =>$catalogsItem['item']['title'] ?? null,'value' => $catalogsItem['item']['value']?? null );
        }
        $items['address'] =  Address::find($this->address_id)->value;
        $items['area'] =  $this->area;
        $items['flat'] =  $this->flat;
        $items['year_construct'] =  $this->year_construct;
        $items['num_rooms'] =  $this->num_rooms;
        $items['floor'] =  $this->floor;
        $items['media_count'] =  Media::GetCount($this->id);
        $items['main_photo'] = Media::GetMainPhoto($this->id);
        $items['type'] =  CatalogItems::find($this->type)->item['title'];
        return $items;
    }
    public static function GetShortParamsForTcket($id)
    {
        $object =Objects::find($id);
        $items= array();
        $add = Address::find($object->address_id);
        $items['address'] = $add->value;
        $items['address_short'] =  Street::find($add->street_id)->street_with_type.($add->house != null ? ', '.$add->house :'');
        $items['main_photo'] = Media::GetMainPhoto($object->id);
        $items['type'] = CatalogItems::find($object->type)->item['title'];
        $array_paro=array();
        if($object->num_rooms!='' || $object->num_rooms!=null)
            array_push($array_paro,$object->num_rooms.' ком');
        if($object->area !='' ||  $object->area !=null)
            array_push($array_paro,$object->area." кв.м");
        if( $object->floor !=''||   $object->floor!=null)
            array_push($array_paro,'эт.'. $object->floor);
        $items['short_par']=implode(', ',$array_paro);
        if($object-> is_new_building) $items['class'] = "Новостройка" ; else  $items['class'] = "Вторичная";
        return $items;
    }
    public static function GetObject_edit($id)
    {
        $object = Objects::find($id);
        $items= array();
        $block = array();
        $block['1'] =array(
            $items[] = array(
                'type_field'=>'textarea',
                'title_table' =>'address',
                'table' =>'objects',
                'title' =>'Адрес',
                'value' =>Address::find($object->address_id)->value),
            $items[] =  array(
                'title' => 'Тип объекта',
                'title_table' =>'type',
                'type_field'=>'select',
                'table' =>'objects',
                'id'=>'type',
                'value_id'=>$object->type,
                'values' =>   CatalogItems::getItemByCatalogName('objectTypes')),
            $items[] =  array(
                'type_field'=>'number',
                'table' =>'objects',
                'title_table' =>'floor',
                'title' =>'Этаж',
                'value' =>$object->floor),
            $items[] = array(
                'title_table' =>'flat',
                'table' =>'objects',
                'title' =>'Номер квартиры',
                'value' =>$object->flat),
            $items[''] =  array(
                'title_table' =>'developer_id',
                'table' =>'objects',
                'type_field'=>'select',
                'title' =>'Застройщик',
                'search_class'=>'search',
                'id' =>  $object->developer_id,
                'value_title' => Developer::whereId($object->developer_id)->pluck('name_of_org')->first(),

            ),
            $items[''] =   array(
                'type_field'=>'number',
                'table' =>'objects',
                'title_table' =>'year_construct',
                'title' =>'Год постройки',
                'value' =>$object->year_construct),
        );
        $block['2'] =array(
            $items[''] =  array(
                'maska'=>'float',
                'table' =>'objects',
                'title_table' =>'celling_height',
                'title' =>'Высота потолков',
                'value' =>$object->celling_height),
            $items[] =  array(
                'maska'=>'float',
                'title_table' =>'area',
                'table' =>'objects',
                'title' =>'Общая площадь',
                'value' =>$object->area),
            $items[] =  array(
                'maska'=>'float',
                'title_table' =>'area_kitchen',
                'table' =>'objects',
                'title' =>'Площадь кухни',
                'value' => $object->area_kitchen),
            $items[] =  array(
                'type_field'=>'number',
                'title_table' =>'num_rooms',
                'table' =>'objects',
                'title' =>'Количество комнат',
                'value' =>$object->num_rooms),
            $items[] =   array(
                'type_field'=>'number',
                'title_table' =>'num_bathrooms',
                'table' =>'objects',
                'title' =>'Количество ванных комнат',
                'value' =>$object->num_bathrooms),
            $items[] =  array(
                'type_field'=>'number',
                'title_table' =>'num_bedrooms',
                'table' =>'objects',
                'title' =>'Количество спальных комнат',
                'value' => $object->num_bedrooms),

        );
        $block['3'] =array(
            $items[] =  array(
            'type_field'=>'textarea',
            'title_table' =>'description',
                'table' =>'tickets',
            'title' =>'Описание объекта',
            'value' => Ticket::where('object_id',$object->id)->first() ->description)
            );


        foreach( Objects::get_catalogsItems($id) as $catalogsItem)
        {
            array_push($block['2'],
                $items[ $catalogsItem['catalog_name']] = array(
                    'title' => $catalogsItem['catalog_title'],
                    'type_field'=>'select',
                    'table' =>'catalogs',
                    'catalog_name'=>$catalogsItem['catalog_name'],
                    'is_catalog'=>"true",
                    'id'=>$catalogsItem['item_id'],
                    'value_id' =>   $catalogsItem['item']['value'] ?? null,
                    'value_title' =>   $catalogsItem['item']['title']?? null,
                    'values' =>   CatalogItems::getItemByCatalogName($catalogsItem['catalog_name']),
                )
            );
        }
        return $block;

    }
    public static function GetObject($id)
    {
        $object = Objects::find($id);
        $items= array();
        $block = array();
        $adr = Address::find($object->address_id);
        $block['1'] =array(
            $items['address'] = array(
                'title' =>'Адрес',
                'value' =>$adr->value,
                'geo_lon'=> $adr->geo_lon,
                'geo_lat'=>$adr->geo_lat,
            ),
            $items['floor'] =  array( 'title' =>'Этаж', 'value' =>$object->floor),
            $items['flat'] = array( 'title' =>'Номер квартиры', 'value' =>$object->flat),
            $items['developer_id'] =  array( 'title' =>'Застройщик', 'id' =>$object->developer_id ?? '', 'value' => Developer::GetDeveloper($object->developer_id)['name_of_org'] ?? ''),
            $items['year_construct'] =   array( 'title' =>'Год постройки', 'value' =>$object->year_construct ? ($object->year_construct." г"):''),

        );
        $block['2'] =array(
            $items['celling_height'] =  array( 'title' =>'Высота потолков', 'value' =>$object->celling_height ? ($object->celling_height."м") :''),
            $items['area'] =  array( 'title' =>'Общая площадь', 'value' =>$object->area?($object->area." кв.м"):''),
            $items['area_kitchen'] =  array( 'title' =>'Площадь кухни', 'value' => $object->area_kitchen?($object->area_kitchen." кв.м"):''),
            $items['num_rooms'] =  array( 'title' =>'Количество комнат', 'value' =>$object->num_rooms),
            $items['num_bathrooms'] =   array( 'title' =>'Количество ванных комнат', 'value' =>$object->num_bathrooms),
            $items['num_bedrooms'] =  array( 'title' =>'Количество спальных комнат', 'value' => $object->num_bedrooms),

        );
        $block['3'] =array(
            $items[] =  array(
                'title' =>'Описание объекта',
                'value' => Ticket::where('object_id',$object->id)->first() ->description)
        );

        foreach( Objects::get_catalogsItems($id) as $catalogsItem)
        {
            array_push(   $block['2'],
                $items[$catalogsItem['catalog_title']] = array(
                    'title' =>$catalogsItem['catalog_title'],
                    'value' => $catalogsItem['item']['title']??""  )
        );
        }
        return $block;

    }
    public static function GetObjectsOfDeveloper($developerId)
    {
        $array =array();
        $objects = Objects::where('developer_id', $developerId)->get();
        foreach($objects as $object)
        {
            $ticket =Ticket::where('object_id',$object->id)->first();
            array_push($array,array(
                'id'    => $object->id,
                'object_params' => $object->GetShortParams(),
                'ticket' =>$ticket->GetTicketWithObject($ticket->id),

            ));
        }
        return $array;
    }
    public static function GetSimilarObjects($id,$offset, $limit)
    {
        $object= Objects::find($id);
        $street_array = Address::getAddressesStreet($object->address_id);
        $array=array();

        $array['inStreet']=array();
        $objects = Objects::whereIn('address_id', $street_array)->where('id','!=',$id );
        if($object->area) $objects->where('area','>',$object->area-10)->where('area','<',($object->area)+10);
            $objects = $objects->get();
        foreach($objects as $object)
        {
            $ticket =Ticket::where('object_id',$object->id)->first();
            array_push($array['inStreet'],array(
                'id'    => $object->id,
                'object_params' => $object->GetShortParams(),
                'ticket' =>$ticket->GetTicketWithObject($ticket->id),

            ));
        }

        return $array;
    }
    public static function GetObjectsListCount($tab,$user_id,$text,$filter =null)
    {
        $count = 0;
        $count = Ticket::whereNotNull('object_id');
        if($tab==2) {
           $count  ->where('tickets.type', 1);
           $count ->where('user_id', $user_id);
       }
        if($text !=null && $text!='') $count->where('object_id',$text);
        $count->select('tickets.id', 'object_id')->whereIn('status', [1, 2]);
        if($filter!=null){
            $count->leftjoin('objects','objects.id','=','object_id');
            foreach ($filter as $key=>$param) {
                if (strpos($key, '-')) {
                    $key = explode('-', $key);
                    if ($key[1] == "min") $count->where($key[0], '>', $param);
                    if ($key[1] == "max") $count->where($key[0], '<', $param);
                    }
                else if($key=="catalog") {
                    $count->leftjoin('entityCatalogItems', 'entityCatalogItems.entity_id', '=', 'object_id');
                    foreach ($param as $par) {
                        $count->where('item_id', $par);
                        }
                    }
                    else if (is_array($param))$count->whereIn($key, $param);
                    else $count->where($key, $param);
                }
            }
            $count =$count->distinct('object_id')->count();
        return $count;
    }
    public static function GetObjectsList($offset,$tab,$user_id,$text,$filter = null)
    {
        $array = array();
            $tickets = Ticket::whereNotNull('object_id')->select('tickets.id', 'object_id');
                   if($tab==2) {
                       $tickets ->where('tickets.type',1)->where('user_id',$user_id);

                   }
                   else $tickets ->whereIn('tickets.status', [1, 2]);

                         if($text!='' && $text !=null )
                             $tickets->where('tickets.object_id',$text );
                if($filter!=null){
                    $tickets->leftjoin('objects','objects.id','=','object_id');
                    foreach ($filter as $key=>$param) {
                        if (strpos($key, '-')) {
                            $key = explode('-', $key);
                            if ($key[1] == "min") $tickets->where($key[0], '>', $param);
                            if ($key[1] == "max") $tickets->where($key[0], '<', $param);
                        }
                        else if($key=="catalog") {
                            $tickets->leftjoin('entityCatalogItems', 'entityCatalogItems.entity_id', '=', 'object_id');
                            $tickets ->where(function ($query) use ($tickets,$param ) {
                                foreach ($param as $par) {
                                    $tickets->where('item_id', $par);
                                }
                            }) ;
                        }
                        else if (is_array($param))$tickets->whereIn($key, $param);
                        else $tickets->where($key, $param);
                    }
                }
            $tickets = $tickets->distinct('object_id')->offset($offset)
                ->limit(10)->get();
            foreach ($tickets as $ticket) {
                $object = Objects::find($ticket->object_id);
                array_push($array, array(
                    'id' => $object->id,
                    'object_params' => $object->GetShortParams(),
                    'ticket' => $ticket->GetTicketWithObject($ticket->id),
                ));
            }

        return $array;
    }
    public static function get_catalogsItems($object_id)
    {
        $array =array();

        $catalogs =  CatalogItems::getItemByCatalogName('fieldsObject');
        foreach( $catalogs as $catalog)
        {
            $main_catalog = Catalog::whereName($catalog['item']['title'])->first()->toArray();
            $item_id=CatalogItems::getItemObjectByCatalogName($catalog['item']['title'], $object_id);

            array_push( $array, array(
               'item_id'=>$item_id,
               'catalog_name' => $main_catalog['name'],
               'catalog_title' => $main_catalog['title'],
               'item' => CatalogItems::whereId($item_id)->first()->item ?? 'null',
           ));

        }

        return $array;
    }
    public function CatalogsItems()
    {
        return $this->hasMany('App\Catalog\EntityCatalogItems', 'entity_id', 'id');
    }
    public function GetTicket()
    {
        return $this->hasMany('App\Ticket', 'object_id', 'id');
    }
}
