<?php

namespace App\Models\Object;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $timestamps = false;
    protected $table = 'media';
    protected $fillable = [
        'object_id','path','name', 'is_main'
    ];
    public static function GetMediaObject($id)
    {

        return Media::where('object_id',$id)->get()->toArray();

    }
    public static function GetMainPhoto($id)
    {

        return Media::where('object_id',$id)->where('is_main',true)->first();

    }
    public static function GetCount($id)
    {

        return Media::where('object_id',$id)->count();

    }
}
