<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class EntityCatalogItems extends Model
{
    protected $table = 'entityCatalogItems';
    protected $fillable = [
        'entity_id','entity_type','item_id'
    ];
    public function catalogItem()
    {
        return $this->belongsTo('App\Models\Catalog\CatalogItems', 'item_id', 'id');
    }
}
