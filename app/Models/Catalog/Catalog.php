<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $table = 'catalogs';
    protected $fillable = [
        'name'
    ];

    public function catalogItems()
    {
        return $this->hasMany('App\Models\Catalog\CatalogItems', 'catalog_id', 'id');
    }

}
