<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogItems extends Model
{
    protected $casts = [
        'item' => 'array',
    ];
    protected $table = 'catalogItems';
    protected $fillable = [
        'name','item'
    ];
    public function catalogName()
    {
        return $this->belongsTo('App\Models\Catalog\Catalog', 'catalog_id', 'id');
    }

    public static function getEnumValue($title)
    {
        return Catalog::whereName('entities')->first()->catalogItems()->whereJsonContains('item', ['title' => $title])->first()->item;

    }

    public static function getItemByCatalogName($catalogName)
    {
        $id = Catalog::where('name',$catalogName)->pluck('id')->first();
        return CatalogItems::where('catalog_id',$id)->select('item','id')->get()->toArray();

    }
    public static function getItemObjectByCatalogName($catalogName,$objectId)
    {
        $id = Catalog::where('name',$catalogName)->pluck('id')->first();
        $id_items = CatalogItems::where('catalog_id',$id)->pluck('id')->toArray();
        $return = EntityCatalogItems::whereIn('item_id',$id_items)->where('entity_type',4)->where('entity_id',$objectId)->pluck('item_id')->first();
        return $return;
    }
    public static function getIdByValue($catalog_name, $value){
        return Catalog::whereName($catalog_name)->first()->catalogItems()->whereJsonContains('item', ['value' => (int)$value])->first()->id;

    }
    public static function getItemByValue($catalogName, $value)
    {
       return Catalog::whereName($catalogName)->first()->catalogItems()->whereJsonContains('item', ['value' => (int)$value])->first()->item;

    }
    public static function getItemValueByTitle($catalogName, $title)
    {
        return Catalog::whereName($catalogName)->first()->catalogItems()->whereJsonContains('item', ['title' => $title])->first()->item;

    }

    public static function getFilter($catalogName)
    {
       $arr_return= array();
        $id = Catalog::where('name',$catalogName)->pluck('id')->first();
        $arr =  CatalogItems::where('catalog_id',$id)->select('item')->get()->toArray();
        foreach ($arr as  $item)
        {

            $title = null;
            $values = array();
            if($item['item']['source'] =="catalog" )
            {
                $title = Catalog::whereName($item['item']['name'])->pluck('title')->first();
                $values = CatalogItems::getItemByCatalogName($item['item']['name']);
            }
            elseif ($item['item']['source'] !="search" ) {
                foreach ($item['item']['source'] as  $value)
                {
                    $values[] = $value;
                }
            }

            $help_array = array(
                'name' => $item['item']['name'] ?? '',
                'source' => $item['item']['source'] ?? '',
                'type_field' => $item['item']['typeField'] ?? '',
                'type' => $item['item']['type'] ?? '',
                'maska' => $item['item']['maska'] ?? '',
                'title' => $title ?? $item['item']['title'],
                'values' => $values,
                'where_to_look' => $item['item']['where_to_look'] ?? '',
            );
            if($item['item']['block'] =="main") {
                $arr_return["main"][] = $help_array;
            }
            else   $arr_return["additional"][] =$help_array;
        }
         return $arr_return;
    }


    public static function getItemByValueAndType($catalogName, $value, $type)
    {
        return Catalog::whereName($catalogName)->first()->catalogItems()->whereJsonContains('item', ['value' => $value])->whereJsonContains('item',['type' => $type])->first()->item;
        //пока так, не знаю как по другому, по хорошему бы сделать один метод и массив передавать в него параметров

    }
}
