<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Favorites extends Model
{
    protected $table = 'favorites';
    protected $fillable = ['entity', 'entity_id', 'user_id','created_at','deleted_at'];
    public $timestamps = false;
    public static function GetFavorites($userId,$entity)
    {
        return Favorites::where('user_id',$userId)->where('entity',$entity)->pluck('entity_id')->toArray();
    }
    public static function AddFav($entityId,$userId,$entity)
    {
     $item = new Favorites;
        $item->user_id = $userId;
        $item->entity_id = $entityId;

        if($entity == "ticket")  $item->entity=3;
            else if($entity == "object") $item->entity=4;

        if($item->save()) return 'success created';
       else  return 'bad, very bad!';

    }
    public static function DelFav($entityId,$userId,$entity)
    {
        if($entity == "ticket")  $entity=3;
        else if($entity == "object") $entity=4;

        if(Favorites::where('entity_id', $entityId)->where('entity',$entity)->where('user_id',$userId)->delete()) return 'success deleted';
        else  return 'bad, very bad!';

    }
    public static function GetStatus($entityId,$userId,$entity)
    {

        if($entity == "ticket")  $entity=3;
        else if($entity == "object") $entity=4;
        $fav=Favorites::where('entity_id',$entityId )-> where('user_id', $userId)->where('entity',$entity)->count();
        if($fav>=1) return true;
        else return false;
    }
}
