<?php

namespace App\Http\Controllers;

use App\Charts\SampleChart;
use App\Models\Catalog\CatalogItems;
use App\Models\Ticket\Ticket;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

class UserController extends Controller
{
    public function findUser(Request $request){
        $user_array=array();
        $user_name =  $request->input('developer_name');
        $user_name = explode(" ", $user_name);
        $results = User::select("name", "id");
        foreach($user_name as $word){
            $results->orWhere('name', 'ilike', '%'.$word.'%');
        }

        $results= $results ->limit(5)->get();
        foreach ($results as $result){
            $user_array[]=array(
                'name_of_org'=>$result->name,
                'id'=> $result->id,
            );
    }
    return response()->json(['results'=>$user_array]);
    }
    public function  usersList(request $request){
        $offset =Cache::get('users_page',0);
        $page = $request->input('page');
        $text = Cache::get('users_search', '');
        if($page == 'next') $new_offset = $offset+15;
        if($page =="back") $new_offset = $offset-15;
        if($new_offset <0)$new_offset=0;
        $users = User::searchUsersInTab($text,$offset);
        $allcount = User::searchUsersCount($text);
        Cache::forever('users_page', $new_offset);
        return response()->json(['data'=>$users,'new_offset'=> $new_offset,'allcount'=>$allcount]);
    }
    public function  searchUser(request $request){
        $text= $request->input('text');
        $offset =0;
        Cache::forever('users_page', 0);
        Cache::forever('users_search', $text);
        $users = User::searchUsersInTab($text,$offset);
        $allcount = User::searchUsersCount($text);
        return response()->json(['data'=>$users,'new_offset'=> $offset,'allcount'=>$allcount]);
    }

    public function users_admin($drawer=null){
        $search = Cache::get('users_search',"");
        $offset =  Cache::get('users_page',0);
        $users = User::searchUsersInTab($search,$offset);
        $countTickets = User::searchUsersCount($search);
        $allcount =$countTickets;

        return view('admin.users')->with(['users'=> $users,'drawer'=>$drawer,'allcount'=>$allcount]);
    }

    public function userCard($id){
        $user_array=array();
        $user = User::find($id);
        $user_array['id'] =$id;
        $user_array['fullname'] = $user->name;
        $user_array['phone'] = $user->phone;
        $user_array['gender'] = $user->gender;
        $user_array['email'] = $user->email;
        $user_array['date_of_birth'] = date_format(date_create( $user->date_of_birth),'d.m.Y' );
        $user_array['position'] =  DB::table('positions')
            ->select(DB::raw('name'))
            ->whereId($user->position_id)
            ->get()->toArray()[0]->name;
        return view('user.about')->with(['user'=> $user_array]);
    }

    public function lk(){
    $userId = Auth::user()->id;
    $user_array=array();
    $user = User::find($userId);
    $user_array['fullname'] = $user->name;
    $user_array['id'] =$userId;
    $user_array['phone'] = $user->phone;
    $user_array['gender'] = $user->gender;
    $user_array['email'] = $user->email;
    $user_array['date_of_birth'] = date_format(date_create( $user->date_of_birth),'d.m.Y' );
    $user_array['position'] =  DB::table('positions')
        ->select(DB::raw('name'))
        ->whereId($user->position_id)
        ->get()->toArray()[0]->name;
    return view('user.about')->with(['user'=> $user_array]);

}
    public function userStatistics($id )
    {

         $userId =$id;
        $user_array = array();
        $user = User::find($userId);
        $user_array['fullname'] = $user->name;
        $user_array['id'] = $id;
        $static = DB::table('deals')
            ->orderBy('new_date')
            ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
            ->join('tickets', 'tickets.id', '=', 'deals.ticket_buy_id')
            ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
            $static->where('tickets.user_id', $userId);
        $static= $static ->groupby('new_date')
            ->get()->toArray();

        $static_sell = DB::table('deals')
            ->orderBy('new_date')
            ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
            ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
            ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
    $static_sell->where('tickets.user_id', $userId);
        $static_sell = $static_sell->where('tickets.type', 1)
            ->groupby('new_date')
            ->get()->toArray();


        $static_buy = DB::table('deals')
            ->orderBy('new_date')
            ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
            ->join('tickets', 'tickets.id', '=', 'deals.ticket_buy_id')
            ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
 $static_buy->where('tickets.user_id', $userId);
        $static_buy = $static_buy ->where('tickets.type', 2)
            ->groupby('new_date')
            ->get()->toArray();

        $labels = collect([]);

        $data_all = collect([]);
        $data_sell = collect([]);
        $data_buy = collect([]);

        $data2_all = collect([]);
        $data2_sell = collect([]);
        $data2_buy = collect([]);

        $allcount = 0;
        $allsum = 0;
        $buy_count = 0;
        $sell_count = 0;
        foreach ($static_buy as $st) {
            $buy_count += $st->count;
            $data_buy->push($st->count);
            $allsum+=$st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'];
            $data2_buy->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);
        }

        foreach ($static_sell as $st) {
            $sell_count += $st->count;
            $data_sell->push($st->count);
            $allsum+=$st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'];
            $data2_buy->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);

        }
        $allcount = $sell_count + $buy_count;
        foreach ($static as $st) {
            $data_all->push($st->count);
            $labels->push(Date::parse($st->new_date)->format(' F Y г.'));
            $data2_all->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);

        }


            $chart = new SampleChart;
            $chart->labels($labels);
            $chart->dataset('Общее количество - ' . $allcount, 'line', $data_all)
                ->backgroundColor('rgba(0, 170, 238, 0.2)');
            $chart->dataset('Количество сделок в качестве продавца - ' . $sell_count, 'line', $data_sell)
                ->backgroundColor('rgba(60, 179, 113, 0.2)');
            $chart->dataset('Количество сделок в качестве покупателя - ' . $buy_count, 'line', $data_buy)
                ->backgroundColor('rgba(255, 255, 0, 0.2)');


            $chart2 = new SampleChart;
            $chart2->labels($labels);
            $chart2->dataset('Общая выручка- ' . $allcount, 'line', $data2_all)
                ->backgroundColor('rgba(0, 170, 238, 0.2)');
            $chart2->dataset('Выручка от продаж  - ' . $sell_count, 'line', $data2_sell)
                ->backgroundColor('rgba(60, 179, 113, 0.2)');
            $chart2->dataset('Выручка от покупок - ' . $buy_count, 'line', $data2_buy)
                ->backgroundColor('rgba(255, 255, 0, 0.2)');

        if (empty($labels[0])) $labels[0] = 0;

            return view('user.myStatistics')->with(['user' => $user_array,
                'chart2' => $chart2,
                'chart' => $chart,
                'date_min' => $labels[0],
                'date_max' => $labels[count($labels) - 1]]);

    }
    public function myStatistics($id =null)
    {
       if($id==null) $userId = Auth::user()->id;
       else $userId =$id;

            $user_array = array();
            $user = User::find($userId);
            $user_array['fullname'] = $user->name;
            $user_array['id'] =$userId;
        if (Auth::user()->position_id == 2) {
            $static_users_count = DB::table('deals')
                ->orderBy('count','desc')
                ->select(DB::raw('user_id'), DB::raw('users.name'), DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->join('users', 'users.id', '=', 'user_id')
                ->where('date', '>', today()->subMonths(1))->where('date', '<', today())
                ->groupby('user_id')
                ->groupby('name')
                ->limit(5)->get()->toArray();

            $static_users_sum = DB::table('deals')
                ->orderBy('sum','desc')
                ->select(DB::raw('user_id'), DB::raw('users.name'), DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price)*'.CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'].' as sum'))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->join('users', 'users.id', '=', 'user_id')
                ->where('date', '>', today()->subMonths(1))->where('date', '<', today())
                ->groupby('user_id')
                ->groupby('name')
                ->limit(5)->get()->toArray();

            $static_users_newb = DB::table('deals')
                ->orderBy('count','desc')
                ->select(DB::raw('user_id'), DB::raw('users.name'), DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->join('objects', 'objects.id', '=', 'tickets.object_id')
                ->join('users', 'users.id', '=', 'user_id')
                ->where('objects.type',6)
                ->where('date', '>', today()->subMonths(1))->where('date', '<', today())
                ->groupby('user_id')
                ->groupby('name')
                ->limit(5)->get()->toArray();

            $static_users_oldb = DB::table('deals')
                ->orderBy('count','desc')
                ->select(DB::raw('user_id'), DB::raw('users.name'), DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->join('objects', 'objects.id', '=', 'tickets.object_id')
                ->join('users', 'users.id', '=', 'user_id')
                ->where('objects.type','<',6)
                ->where('date', '>', today()->subMonths(1))->where('date', '<', today())
                ->groupby('user_id')
                ->groupby('name')
                ->limit(5)->get()->toArray();

            $static_users_all= DB::table('deals')
                ->select(DB::raw('user_id'), DB::raw('users.name'), DB::raw('count(tickets.id) as count'))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->join('users', 'users.id', '=', 'user_id')
                ->where('date', '>', today()->subMonths(1))->where('date', '<', today())
                ->groupby('user_id')
                ->groupby('name')
                ->limit(5)->get()->toArray();

            $in_work= DB::table('users')
                ->orderBy('name','asc')
                ->select(DB::raw('count(tickets.id) as count'),DB::raw('users.name'), DB::raw('user_id'))
                ->join('tickets', 'user_id', '=', 'users.id')
                ->where('status', '1')
                ->where('tickets.created_at', '>', today()->subMonths(1))
                ->groupby('user_id')
                ->groupby('name')
                ->pluck('count')->toArray();


            $done= DB::table('users')
                ->orderBy('name','desc')
                ->select(DB::raw('count(tickets.id) as count'),DB::raw('users.name'), DB::raw('user_id'))
                ->join('tickets', 'user_id', '=', 'users.id')
                ->where('status', '2')
                ->where('tickets.created_at', '>', today()->subMonths(1))
                ->groupby('user_id')
                ->groupby('name')
                ->pluck('count')->toArray();

            $paused= DB::table('users')
                ->orderBy('name','desc')
                ->select(DB::raw('count(tickets.id) as count'),DB::raw('users.name'), DB::raw('user_id'))
                ->join('tickets', 'user_id', '=', 'users.id')
                ->where('status', '3')
                ->where('tickets.created_at', '>', today()->subMonths(1))
                ->groupby('user_id')
                ->groupby('name')
                 ->pluck('count')->toArray();

        }

                // кол-во
            $static = DB::table('deals')
                ->orderBy('new_date')
                ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                    DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_buy_id')
                ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
                if (Auth::user()->position_id == 1)
                    $static->where('tickets.user_id', $userId);
               $static= $static ->groupby('new_date')
                ->get()->toArray();


            $static_sell = DB::table('deals')
                ->orderBy('new_date')
                ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                    DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
                ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
              if (Auth::user()->position_id == 1) $static_sell->where('tickets.user_id', $userId);
        $static_sell = $static_sell->where('tickets.type', 1)
                ->groupby('new_date')
                ->get()->toArray();


            $static_buy = DB::table('deals')
                ->orderBy('new_date')
                ->select(DB::raw('count(deals.id) as count'), DB::raw('sum(deals.price) as sum'),
                    DB::raw("to_date(to_char( date, 'MONTH YYYY' ),'MONTH YYYY' ) as new_date"))
                ->join('tickets', 'tickets.id', '=', 'deals.ticket_buy_id')
                ->where('date', '>', today()->subMonths(12))->where('date', '<', today());
              if (Auth::user()->position_id == 1) $static_buy->where('tickets.user_id', $userId);
        $static_buy = $static_buy ->where('tickets.type', 2)
                ->groupby('new_date')
            ->get()->toArray();

            $labels = collect([]);

            $data_all = collect([]);
            $data_sell = collect([]);
            $data_buy = collect([]);

            $data2_all = collect([]);
            $data2_sell = collect([]);
            $data2_buy = collect([]);

            $allcount = 0;
        $allsum = 0;
            $buy_count = 0;
            $sell_count = 0;
            foreach ($static_buy as $st) {
                $buy_count += $st->count;
                $data_buy->push($st->count);
                $allsum+=$st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'];
                $data2_buy->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);
            }

            foreach ($static_sell as $st) {
                $sell_count += $st->count;
                $data_sell->push($st->count);
                $allsum+=$st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value'];
                $data2_buy->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);

            }
            $allcount = $sell_count + $buy_count;
            foreach ($static as $st) {
                $data_all->push($st->count);
                $labels->push(Date::parse($st->new_date)->format(' F Y г.'));
                $data2_all->push($st->sum * CatalogItems::getItemByCatalogName('priceRealtor')[0]['item']['value']);

            }

        if (Auth::user()->position_id == 1)
        {
            $chart = new SampleChart;
            $chart->labels($labels);
            $chart->dataset('Общее количество - ' . $allcount, 'line', $data_all)
                ->backgroundColor('rgba(0, 170, 238, 0.2)');
            $chart->dataset('Количество сделок в качестве продавца - ' . $sell_count, 'line', $data_sell)
                ->backgroundColor('rgba(60, 179, 113, 0.2)');
            $chart->dataset('Количество сделок в качестве покупателя - ' . $buy_count, 'line', $data_buy)
                ->backgroundColor('rgba(255, 255, 0, 0.2)');


            $chart2 = new SampleChart;
            $chart2->labels($labels);
            $chart2->dataset('Общая выручка- ' . $allcount, 'line', $data2_all)
                ->backgroundColor('rgba(0, 170, 238, 0.2)');
            $chart2->dataset('Выручка от продаж  - ' . $sell_count, 'line', $data2_sell)
                ->backgroundColor('rgba(60, 179, 113, 0.2)');
            $chart2->dataset('Выручка от покупок - ' . $buy_count, 'line', $data2_buy)
                ->backgroundColor('rgba(255, 255, 0, 0.2)');
    }
        else {
            $chart = new SampleChart;
            $chart->labels($labels);
            $chart->dataset('Общее количество - ' . $allcount, 'line', $data_all)
                ->backgroundColor('rgba(0, 170, 238, 0.2)');

            $chart2 = new SampleChart;
            $chart2->labels($labels);
            $chart2->dataset('Общая выручка- ' . number_format( $allsum , 0, ' ', ' ').' руб.' , 'line', $data2_all)
                ->backgroundColor('rgba(14, 10, 255, 0.2)');

        }
            if (empty($labels[0])) $labels[0] = 0;
        if (Auth::user()->position_id == 1)
        {
            return view('user.myStatistics')->with(['user' => $user_array,
                'chart2' => $chart2,
                'chart' => $chart,
                'date_min' => $labels[0],
                'date_max' => $labels[count($labels) - 1]]);
        }

        else {
            return view('admin.main')->with([
                'static_users_sum'=>$static_users_sum,
                'static_users_count'=>$static_users_count,
                'static_users_newb'=> $static_users_newb,
                'static_users_oldb'=> $static_users_oldb,
                'in_work'=>'['.implode(',',$in_work).']',
                'done'=>'['.implode(',',$done).']',
                'paused'=>'['.implode(',',$paused).']',
                'chart2' => $chart2,
                'chart' => $chart,
                'date_min' => $labels[0],
                'date_max' => $labels[count($labels) - 1]]);
        }
    }
    public function createUser(Request $request)
    {

        if ($request->ajax()) {
            $param = $request->input('ticket');
            $param = json_decode($param, true);

            $user = new User();
            foreach ($param as $key => $item) {
                $user->$key = $item;
            }
            $user->name = $param['second_name']." ". $param['first_name']." ".$param['last_name'];
            if ($param['gender'] == 'male')
                $user->gender = 1;
            else
                $user->gender = 2;
            $user->position_id= 1;
            $user->save(); $data = $request->input('ticket');

        }
        return response()->json(['data' => 'success','id'=>$user->id]);


    }
    public function users(Request $request)
    {
        $users= DB::table('users')
            ->orderBy('second_name','asc')
            ->select('second_name','first_name','last_name')->get();

        $us_ar =array();
        foreach ($users as $user)
        {
            array_push($us_ar,
                $user->second_name.' '.mb_substr($user->first_name,0,1).'.'.mb_substr($user->last_name,0,1));
        }
        return response()->json(array('data' => $us_ar));
    }
    public function getFullName()
    {
        return $this->first_name." ".$this->second_name." ".$this->last_name;
    }

}
