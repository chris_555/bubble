<?php

namespace App\Http\Controllers;
use App\Models\Catalog\CatalogItems;
use App\Models\Client;
use App\Models\Ticket\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;

class ClientController extends Controller
{
    public function index()
    {
        $search = Cache::get('clients_search',"");
        $offset =  Cache::get('clients_page');
        $clients =  Client::GetClientsList($offset,$search);
        $count = count($clients);
        $allcount =Client::GetClientsListCount($search);
        return view('client.clients', ['clients' => $clients,'offset'=>$offset,'count'=>$count,'allcount'=>$allcount]);
    }

    public function ClientEdit(request $request)
    {
        if($request->ajax()){
        $params = $request->input('client');
        $client_id = $request->input('clientId');
        $client = Client::whereId($client_id)->first();
            foreach ($params as $key => $param) {
                if ($param != '' && $key) {
                    $client->$key = $param;
                } else   $client->$key = null;
            }
            $client->save();
    }
        return response()->json(['params'=>$params]);
    }

    public function clientCard($clientId,$tab=1, $edit=false){
        $client = Client::find($clientId);
        $view = View::make('client.clientCard', [
            'tab' => $tab,
            'edit'=>$edit,
            'client' => $client,
            'clientId'=>$clientId
        ]);
        if($edit=='edit' && $tab==1)  $view->nest('child', 'client.childClientCard.edit.client', ['client' => $client, 'clientId'=>$clientId]);
        else if($edit=='edit' && $tab==3)
        {
            $fields = Ticket::clientDocs($clientId);
            $view->nest('child', 'client.childClientCard.edit.documents', ['fields' => $fields, 'clientId'=>$clientId]);
        }

        else {
            if($tab==1)
                $view->nest('child', 'client.childClientCard.client', ['client' => $client, 'clientId'=>$clientId]);
            if($tab==2)
            {

                $tickets = Ticket::clientTickets($clientId);
                $view->nest('child', 'client.childClientCard.tickets', ['tickets' => $tickets]);
            }
            if($tab==3)
            {
                $fields = Ticket::clientDocs($clientId);
                $view->nest('child', 'client.childClientCard.documents', ['fields' => $fields]);
            }
        }
        return $view;
    }
    public function clientsSearch(request $request)
    {
        $search= $request->input('text');
        $offset =0;
        Cache::forever('clients_page', 0);
        Cache::forever('clients_search', $search);
        $clients =  Client::GetClientsList($offset,$search);
        $count = Client::GetClientsListCount($search);
        return response()->json(['data'=>$clients,'new_offset'=> $offset,'count'=>$count]);
    }
    public function clientsGet(request $request){
        $search = Cache::get('clients_search',"");
        $offset =Cache::get('clients_page',0);
        $page = $request->input('page');
        if($page == 'next') $new_offset = $offset+15;
        if($page =="back") $new_offset = $offset-15;
        if($new_offset <0)$new_offset=0;
        $clients =  Client::GetClientsList($new_offset,$search);
        $count = Client::GetClientsListCount($search);
        Cache::forever('clients_page',$new_offset);
        return Response()->json(['data'=>$clients,'count'=>$count,'new_offset'=> $new_offset]);
    }
}
