<?php

namespace App\Http\Controllers;
use App\Models\Catalog\{CatalogItems,Catalog,EntityCatalogItems};
use Illuminate\Support\Facades\DB;
use App\Models\Ticket\{Collection, Show, Ticket, Comment, ScanDeal, Deal, NeedFields, TicketNeed};
use App\Models\{Address\Address, Developer, Favorites, Client, User};
use Illuminate\Support\Facades\View;
use App\Models\Object\{Objects,Media};
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use Safe\Exceptions\MailparseException;
use Shows;

class SmartController extends Controller
{

    public function report()
    {
        $user_id = Auth::user()->id;
        $user_array=array();
        $user = User::find($user_id);
        $user_array['fullname'] = $user->first_name." ".$user->second_name." ".$user->last_name;
        $user_array['phone'] = User::formatPhone($user->phone);
        return view('report')->with(['user'=> $user_array]);
    }
    public function showsList($tab=1){
        $user_id = Auth::user()->id;
        $tickets  = Show::GetMyShows($tab,$user_id);
        $view = View::make('show.list', [
            'tab' => $tab,'count'=>count($tickets)
        ]);
        if($tab==1)
            $view->nest('child', 'show.child.buy', [
                'tickets' => $tickets,
                ]);

        if($tab==2)
            $view->nest('child', 'show.child.sell', [
                'tickets' => $tickets,
            ]);

        return $view;
    }
    public function setDateTimeShow(Request $request)
{
    $datetime = $request->datetime;
    $id = $request->id;
    $show = Show::find($id);
    $show->date_time = $datetime;
    $show->save();
    $datetime = Date::parse( $show['date_time'])->format('j F Y г, H:i');
    return response()->json(array('datetime'=>$datetime,'id'=>$id), 200);
}
    public function dadata(Request $request)
    {
        $data = $request->message;
        $result = DadataSuggest::suggest("address", ["query" => $data]);
        Address::put_addresses($result);
        return response()->json(array('data' => $result), 200);
    }
    public function editTicket(request $request)
    {
        if ($request->ajax()) {
            $param = $request->input('ticket');
            $ticket_id = $request->input('ticket_id');
            $tab_id = $request->input('tab_id');
            $params = json_decode($param, true);
            $ticket_type = Ticket::GetTicketType($ticket_id);
            if ($tab_id == 1) {
                if ($ticket_type == 2) {
                    foreach ($params as $key => $param) {
                        $field_id = NeedFields::whereTitle($key)->pluck('id')->first();
                        if ($param != '' && $key) {
                            if($field_id ==3)
                            {
                            $result = DadataSuggest::suggest("address", ["query" => $param, "count" => 1]);
                            $param = Address::get_id_or_create($result);
                            }

                            $ticket_need = TicketNeed::firstOrNew(array('ticket_id' => $ticket_id, 'field_id' => $field_id));
                            $ticket_need->value = $param;
                            $ticket_need->save();
                        } else TicketNeed::where('field_id', $field_id)->delete();
                    }
                    $collection = Ticket::CreateCoolectionFromNeed($ticket_id);
                }
                if ($ticket_type == 1) {
                    $ticket = Ticket::whereId($ticket_id)->first();
                    $ticket->price = $params['price'];
                    $ticket->save();
                }
            }

            if ($tab_id == 4) {

                $deal = new Deal;
                $deal->price = str_replace(",", ".", str_replace(" ", '', $params['price']));
                $deal->date = $params['date'];
                $deal->deposit = $params['deposit'];
                $deal->day_payment = $params['day_payment'];
                if ($ticket_type == 1) {
                    $deal->ticket_sell_id = $ticket_id;
                    $deal->ticket_buy_id = $ticket_id;
                }
                if ($ticket_type == 2) {
                    $deal->ticket_sell_id = Ticket::where('object_id', $params['object_id'])->first()->id;
                    $deal->ticket_buy_id = $ticket_id;
                }
                if ($deal->save()) {
                    Ticket::setStatus($deal->ticket_sell_id, 3);
                    Ticket::setStatus($deal->ticket_buy_id, 3);
                    Ticket::setStage($deal->ticket_sell_id, 3);
                    Ticket::setStage($deal->ticket_buy_id, 3);
                }
            }
        }
        return response()->json(['params' => $params]);
    }
    public function editObject(request $request)
    {
        if ($request->ajax()) {
            $param = $request->input('object');
            $ticket_id = $request->input('ticket_id');
            $object_id = $request->input('object_id');
            $tab_id = $request->input('tab_id');
            $params = json_decode($param, true);
            $object = null;
            $object = Objects::find($object_id);
            foreach ($params['objects'] as $value) {
                $key = $value['field'];
                $value = $value['value'];
                if ($value != '') $object->$key = $value;
            }

            $address_id = "";
            $address_id = Address::where('value', $params['address'])->pluck('id')->first();
            if ($address_id == null) {
                $result = DadataSuggest::suggest("address", ["query" => $params['address'], "count" => 1]);
                $address_id = Address::get_id_or_create($result);
            }
            $object->address_id = $address_id;
            $object->save();
            $ticket = Ticket::find($ticket_id);
            foreach ($params['tickets'] as $value) {
                $key = $value['field'];
                $value = $value['value'];
                $ticket->$key = $value;
            }
            $ticket->save();
            foreach ($params['catalog_items'] as $value) {
                $catalog_name = $value['catalog_name'];
                $old_value = $value['old_value'];
                $new_value = $value['new_value'];
                if ($new_value != "" && $old_value != " ") {
                    $new_value_id = CatalogItems::getIdByValue($catalog_name, $new_value);
                    $item = EntityCatalogItems::where('entity_id', $object_id)
                        ->where('entity_type', 4)
                        ->where('item_id', $old_value)
                        ->update(['item_id' => $new_value_id]);
                }
                if ($new_value != '' && $old_value == " ") {
                    $new_value_id = CatalogItems::getIdByValue($catalog_name, $new_value);
                    $item = new EntityCatalogItems();
                    $item->entity_id = $object_id;
                    $item->entity_type = 4;
                    $item->item_id = $new_value_id;
                    $item->save();
                }
                if ($new_value == '' && $old_value != " ") {
                    $item = EntityCatalogItems::where('entity_id', $object_id)
                        ->where('entity_type', 4)
                        ->where('item_id', $old_value)
                        ->delete();
                }
            }
        }
        return response()->json(['params' => $new_value, 'h' => $ticket]);
    }
    public function sendComment(request $request, $ticketId)
    {
        $text = $request->input('text');
        $comment = new Comment;
        $comment->text = $text;
        $comment->user_id = Auth::user()->id;
        $comment->ticket_id = $ticketId;
        $comment->created_at = \Carbon\Carbon::now();
        $comment->save();
        $fullnameUser = Auth::user()->first_name . " " . Auth::user()->second_name . " " . Auth::user()->last_name;
        $created_at = Date::parse($comment->created_at)->format('j F Y в H:i');
        return response()->json(['data' => 'success', 'created_at' => $created_at, 'user' => $fullnameUser]);
    }
    public function changeStatus(request $request)
    {

        $ticketId = $request->input('$ticketId');
        $action = $request->input('action');
        if ($action == "cancel") $response = Ticket::setStatus($ticketId, 2);
        if ($action == "renew") $response = Ticket::setStatus($ticketId, 1);
        if ($action == "delete") $response = Ticket::setStatus($ticketId, 4);
        return response()->json(['data' => $response]);
    }
    public function addToFavorite(request $request)
    {
        $entity = $request->input('entity');
        $entityId = $request->input('entityId');
        $value = $request->input('value');
        $userId = Auth::user()->id;
        if ($value == "true") $response = Favorites::DelFav($entityId, $userId, $entity);
        else   $response = Favorites::AddFav($entityId, $userId, $entity);

        return response()->json(['data' => $response]);
    }
    public function findClient(request $request)
    {
        $phone = $request->input('phone');
        $phone = preg_replace('/[^0-9]/', '', $phone);
        $clients = Client::FindByPhone($phone);
        if (count($clients) > 0) $response = true; else  $response = false;
        return response()->json(['data' => $clients, 'response' => $response]);
    }
    public function createTicket(request $request)
    {
        if ($request->ajax()) {

            $param = $request->input('ticket');
            $param = json_decode($param, true);

            $ticket = new Ticket;
            if ($param['client'] == 'new') {
                $client = new Client;
                $client->first_name = $param['first_name'];
                $param['second_name'] = '' ?: $client->second_name = $param['second_name'];
                $param['last_name'] = '' ?: $client->last_name = $param['last_name'];
                $client->phone = $param['phone'];
                if ($param['gender'] == 'male')
                    $client->gender = 1;
                else
                    $client->gender = 2;
                $client->save();
                $client_id = $client->id;
            } else   $client_id = $param['client'];
            if ($param['type_ticket'] == 'sell') {
                $address_id = "";
                $address_id = Address::where('value', $param['address'])->pluck('id')->first();
                if ($address_id == null) {
                    $result = DadataSuggest::suggest("address", ["query" => $param['address'], "count" => 1]);
                    $address_id = Address::get_id_or_create($result);
               }
                $object = new Objects;
                $object->type = $param['type_object'];
                $object->address_id = $address_id;
                $param['floor'] == '' ?: $object->floor = $param['floor'];
                $param['area'] == '' ?: $object->area = $param['area'];
                $param['num_rooms'] == '' ?: $object->num_rooms = $param['num_rooms'];
               $object->save();
                $object_id = $object->id;
                $ticket->type = 1;
                $ticket->object_id = $object_id;
                $param['description'] == '' ?: $ticket->description = $param['description'];
                $param['price'] == '' ?: $ticket->price = str_replace(",", ".", str_replace(" ", '', $param['price']));
            } else   $ticket->type = 2;
            $param['title']  == '' ? $ticket->title = "Заявка" : $ticket->title = $param['title'];
            $ticket->client_id = $client_id;
            $ticket->status = 1;
            $ticket->stage = 1;
            $ticket->user_id = Auth::user()->id;
            $ticket->save();
            $ticket_id = $ticket->id;
        }
        return response()->json(['ticket_id' => $ticket_id]);
    }
    public function createCollection(Request $request)
    {
        $ticket_id = $request->input('ticket_id');
        $ckecked_objects = $request->input('ckecked_objects');
        $collection = new Collection();
        $collection ->ticket_id = $ticket_id;
        $collection -> collection= $ckecked_objects;
        $collection->save();
        return response()->json(['results' => $ticket_id, 'f'=>$ckecked_objects]);
    }
    public function createShows(Request $request)
    {
        $ticket_id = $request->input('ticket_id');
        $ckecked_objects = $request->input('ckecked_objects');
        foreach($ckecked_objects as $object)
        {
            $show = new Show();
            $show->ticket_buy_id = $ticket_id;
            $show->ticket_sell_id = $object;
            $show->save();
        }
        return response()->json(['results' => $ticket_id, 'f'=>$ckecked_objects]);
    }
    public function findDeveloper(Request $request)
    {

        $developer_name = $request->input('developer_name');
        $results = Developer::select("name_of_org", "id")
            ->where('name_of_org', 'ilike', '%' . $developer_name . '%')
            ->limit(5)->get()->toArray();
        return response()->json(['results' => $results]);
    }
    public function makeMainImage(request $request)
    {
        $id = $request->input('id');
        $obj_id = Media::whereId($id)->pluck('object_id')->first();
        Media::where('object_id', $obj_id)->update(['is_main' => false]);
        Media::whereId($id)->update(['is_main' => true]);
        return response()->json(['response' => 'success']);
    }
    public function deleteScan(request $request)
    {
        $id = $request->input('id');
        ScanDeal::find($id)->delete();;
        return response()->json(['response' => 'success']);

    }
    public function deleteImage(request $request)
    {
        $id = $request->input('id');
        Media::find($id)->delete();;
        return response()->json(['response' => 'success']);
    }
    public function scan_deal_upload(Request $request, $entity_id)
    {
        $localpath = '/images/uploads/scan_deals/';
        $scan_deal = null;
        if ($request->hasFile('file')) {
            $destinationPath = public_path($localpath);
            $image = $request->file('file');
            $fileName = time() . '_scan_deal_' . $entity_id . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $fileName);

            $scan_deal = new ScanDeal;
            $scan_deal->name = $fileName;
            $scan_deal->path = $localpath;
            $scan_deal->deal_id = $entity_id;
            $scan_deal->save();

        }
        return response()->json([
            'success' => 'Record has been updated successfully!',
            'media' => $scan_deal
        ]);
    }
    public function upload_media(Request $request, $entity_id)
    {
        $localpath = '/images/uploads/media_objects/';
        $media = null;
        if ($request->hasFile('file')) {
            $destinationPath = public_path($localpath);
            $image = $request->file('file');
            $fileName = time() . '_object_' . $entity_id . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $fileName);
            $media = new Media;
            $media->name = $fileName;
            $media->path = $localpath;
            $media->is_main = false;
            $media->object_id = $entity_id;
            $media->save();
        }
        return response()->json([
            'success' => 'Record has been updated successfully!',
            'media' => $media
        ]);
    }
    public function сontractGet ($id)
    {
        $deal =Deal::find($id)->toArray();


        $templateProcessor = new TemplateProcessor(resource_path('temp.docx'));
        $seller = Client::leftJoin('tickets','tickets.client_id', 'clients.id')
           ->leftJoin('deals','deals.ticket_sell_id', 'tickets.id')
            ->where('deals.id', $id)->first();

        $templateProcessor->setValue
        (
            array(
                'name_seller',
                'date_of_birth_seller',
                'pass_serial_seller',
                'pass_num_seller',
                'pass_gave_by_seller',
                'pass_gave_code_seller',
                'pass_gave_when_seller',
                'address_registration_seller'
            ),

            array(
                $seller->second_name." ".$seller->first_name." ".$seller->last_name,
                $seller->date_of_birth,
                $seller->pass_serial,
                $seller->pass_num,
                $seller->pass_gave_by,
                $seller->pass_gave_code,
                $seller->pass_gave_when,
                trim($seller->address_registration),

                )
        );


        $buyer = Client::leftJoin('tickets','tickets.client_id', 'clients.id')
            ->leftJoin('deals','deals.ticket_buy_id', 'tickets.id')
            ->where('deals.id', $id)->first();


        $templateProcessor->setValue
        (
            array(
                'name_buyer',
                'date_of_birth_buyer',
                'pass_serial_buyer',
                'pass_num_buyer',
                'pass_gave_by_buyer',
                'pass_gave_code_buyer',
                'pass_gave_when_buyer',
                'address_registration_buyer'
            ),

            array(
                $buyer->second_name." ".$buyer->first_name." ".$buyer->last_name,
                $buyer->date_of_birth,
                $buyer->pass_serial,
                $buyer->pass_num,
                $buyer->pass_gave_by,
                $buyer->pass_gave_code,
                $buyer->pass_gave_when,
                trim($buyer->address_registration),

            )
        );

        $object = Objects::leftJoin('tickets','tickets.object_id', 'objects.id')
            ->leftJoin('deals','deals.ticket_sell_id', 'tickets.id')
            ->where('deals.id', $id)->first();

        $templateProcessor->setValue
        (
            array(
                'date',
                'area_object',
                'floor_object',
                'flat_object',
                'address_object',
                 'price',
                'deposit',
                'price_without_deposit',
                'day_payment',
                'price_written',
                'deposit_written',
                'price_without_deposit_written',
            ),
            array(
                Date::parse($object->date)->format('j F Y г.') ,
                $object->area,
                $object->floor,
                $object->flat,
                Address::find($object->address_id)->value,
                number_format($object->price, 0, '', ' '),
                number_format( $object->deposit, 0, '', ' '),
                number_format( $object->price-$object->deposit, 0, '', ' '),
                $object->day_payment,
                User::num2str($object->price),
                User::num2str($object->deposit),
                User::num2str($object->price-$object->deposit),
            )
        );
        $export_file = public_path('filename.docx');

        $templateProcessor->saveAs($export_file);
        return response()->download($export_file)->deleteFileAfterSend(true);
    }

    public function reportGet(Request $request)
    {
        $date_min = $request->input('date_min');
        $date_max = $request->input('date_max');
        $entity= $request->input('entity');
        $entity_id= $request->input('entity_id');
        $array = array();
        $events = DB::table('deals')->orderBy('new_date','desc');

        if($entity=="users")
        {
            $events ->select(DB::raw('count(deals.id) as count'),DB::raw("to_char( date, 'MONTH YYYY' ) as new_date"),
                DB::raw('sum(deals.price) as sum'),DB::raw('name as "num_rooms"'));
        }
        else {
            $events ->select(DB::raw('count(deals.id) as count'),DB::raw("to_char( date, 'MONTH YYYY' ) as new_date"),
                DB::raw('sum(deals.price) as sum'),DB::raw('num_rooms'));
        }

        $events->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
            ->join('objects', 'objects.id', '=', 'tickets.object_id');
        if($entity=="users") {
            $events -> leftJoin('users', 'users.id', 'user_id');
        }
     if($date_max!="no")
         $events  ->where('date', '>', $date_min)->where('date', '<', $date_max);
        if($entity=="developer")
        {
            $events = $events->join('developers', 'developers.id','=', 'objects.developer_id')
            ->where('objects.developer_id',$entity_id);
        }

        if($entity== "user")
        {
            $events = $events->where('tickets.user_id',$entity_id);
        }
        if($entity=="users") $events->groupby('new_date', 'name');
        else $events->groupby('new_date', 'num_rooms');

        $events=$events->get()->toArray();
        $last_m=0;
        $first_m = 0;
        $months = array();
        foreach ($events as $item) {
            $months[$item->new_date] = Date::parse($item->new_date)->format(' F Y г.');
            if ($first_m === 0) $first_m = $months[$item->new_date];
            $last_m = $months[$item->new_date];
            $array[$item->num_rooms]['count'][$item->new_date] = $item->count;
            $array[$item->num_rooms]['sum'][$item->new_date] = number_format($item->sum, 0, ' ', ' ') . ' руб.';
        }
        foreach ($events as $item) {
            foreach ($months as $key => $month) {
                isset($array[$item->num_rooms]['count'][$key]) ?:
                    $array[$item->num_rooms]['count'][$key] = 'Нет продаж';

                isset($array[$item->num_rooms]['sum'][$key]) ?:
                    $array[$item->num_rooms]['sum'][$key] = 'Нет продаж';
            }
        }
        return response()->json([
            'first_m' => $first_m,
            'months' => $months,
            'array' => $array,
            'last_m' => $last_m,
            'count' => count($months),
            'entity'=>$entity
        ]);
    }

    public function downloadReport($date_min,$date_max,$entity=null,$entity_id=null)
    {
        Date::setlocale(config('app.locale'));
        $first_m = 0;
        $last_m = 0;
        $entity_name = "";
        $array = array();
        $events = DB::table('deals')
            ->orderBy('new_date','desc');
        if($entity=="users")
        {
            $events ->select(DB::raw('count(deals.id) as count'),DB::raw("to_char( date, 'MONTH YYYY' ) as new_date"),
                DB::raw('sum(deals.price) as sum'),DB::raw('name as "num_rooms"'));
        }
        else {
            $events ->select(DB::raw('count(deals.id) as count'),DB::raw("to_char( date, 'MONTH YYYY' ) as new_date"),
                DB::raw('sum(deals.price) as sum'),DB::raw('num_rooms'));
        }
           $events ->join('tickets', 'tickets.id', '=', 'deals.ticket_sell_id')
            ->join('objects', 'objects.id', '=', 'tickets.object_id');
         if($entity=="users") {
             $events -> leftJoin('users', 'users.id', 'user_id');
         }
        if($date_max!="no")
            $events  ->where('date', '>', $date_min)->where('date', '<', $date_max);
        if($entity=="developer")
        {
            $events = $events->join('developers', 'developers.id','=', 'objects.developer_id')
                ->where('objects.developer_id',$entity_id);
            $entity_name=Developer::find($entity_id)->name_of_org;
        }

        if($entity== "user")
        {
            $events = $events->where('tickets.user_id',$entity_id);
            $entity_name=User::find($entity_id)->name;
        }

        if($entity=="users") $events->groupby('new_date', 'name');
        else $events->groupby('new_date', 'num_rooms');

        $events=$events->get()->toArray();

        $months = array();
        foreach ($events as $item) {
            $months[$item->new_date] = Date::parse($item->new_date)->format(' F Y г.');
            if ($first_m === 0) $first_m = $months[$item->new_date];
            $last_m  = $months[$item->new_date];
            $array[$item->num_rooms]['count'][$item->new_date] = $item->count;
            $array[$item->num_rooms]['sum'][$item->new_date] = number_format($item->sum, 0, ' ', ' ') . ' руб.';
        }

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->setDefaultFontName('Times New Roman');
        $phpWord->setDefaultFontSize(12);
        $sectionStyle = array(
            'marginTop' => '800',
            'marginLeft' => '800',
            'marginRight' => '800',
            'marginBottom' => '800',
        );

        $section = $phpWord->addSection($sectionStyle);
        $section->addText('Отчет', [], array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
        $section->addText('о динамике продаж объектов недвижимости', [], array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
        $section->addText('с' . $first_m . ' по ' . $last_m . '', [], array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
        $header = array('bold' => true);
        $section->addText(htmlspecialchars('Общество с ограниченной ответсвенностью "Вклад"'),
            array('align' => 'center', 'bold' => true), array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));

        if($entity=="developer")
        {
            $section->addText(htmlspecialchars('I.Количество проданных объектов за период' . $first_m . '-' . $last_m . ' от застройщика '.$entity_name), $header);
        }

        else if($entity== "user")
        {
            $section->addText(htmlspecialchars('I.Количество проданных объектов за период' . $first_m . '-' . $last_m . ' от риэлтора '.$entity_name), $header);
        }

        else $section->addText(htmlspecialchars('I.Количество проданных объектов за период' . $first_m . '-' . $last_m . ''), $header);

        $styleTable = array('borderColor' => '006699', 'borderSize' => 0, 'cellMargin' => 50, 'valign' => 'center');
        $styleFirstRow = array('valign' => 'center');

$count = count($months);
        $phpWord->addTableStyle('countTable', $styleTable, $styleFirstRow);
        $phpWord->addTableStyle('sumTable', $styleTable, $styleFirstRow);

        //
        $table = $section->addTable('countTable');
        $table->addRow();
        $table->addCell(1000)->addText(htmlspecialchars('Количество комнат'));
        foreach ($months as $key => $column) {
            $table->addCell(10000/$count)->addText(htmlspecialchars($column));
        }
        foreach ($array as $room => $row) {
            $table->addRow();
            $table->addCell(10000/$count)->addText(htmlspecialchars($room));
            foreach ($months as $key => $column)
                $table->addCell(10000/$count)->addText(htmlspecialchars($row["count"][$key] ?? 'нет продаж'));
        }

        //
        if($entity=="developer")
        {
            $section->addText(htmlspecialchars('II.Итог реализации объектов за период' . $first_m . '-' . $last_m . ' от застройщиком '.$entity_name), $header);
        }

        else if($entity== "user")
        {
            $section->addText(htmlspecialchars('II.Итог реализации объектов за период' . $first_m . '-' . $last_m . ' от риэлтора '.$entity_name), $header);
        }

      else $section->addText(htmlspecialchars('II.Итог реализации объектов за период' . $first_m . '-' . $last_m . ''), $header);
        $table = $section->addTable('countTable');
        $table->addRow();
        $table->addCell(1000)->addText(htmlspecialchars('Количество комнат'));
        foreach ($months as $key => $column) {
            $table->addCell(10000/$count)->addText(htmlspecialchars($column));
        }
        foreach ($array as $room => $row) {
            $table->addRow();
            $table->addCell(10000/$count)->addText(htmlspecialchars($room));
            foreach ($months as $key => $column)
                $table->addCell(10000/$count)->addText(htmlspecialchars($row["sum"][$key] ?? 'нет продаж'));
        }


        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="report_common_'.$first_m.'_'.$last_m.'.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save("php://output");

    }
    public function downloadCollection($id)
    {
        $tickets = Collection::GetCollection($id)[$id];

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->setDefaultFontName('Times New Roman');
        $phpWord->setDefaultFontSize(12);
        $sectionStyle = array(
            'marginTop' => '0',
            'marginLeft' => '200',
            'marginRight' => '200',
            'marginBottom' => '800',
        );
        $section = $phpWord->addSection($sectionStyle);
        $tableStyle = array(
        'cellMargin' => 90
    );
        $table = $section->addTable($tableStyle);
        $table->addRow(null);
        $name = Auth::user()->name;
        $phone = Auth::user()->phone;
        $table->addCell(12000,array('bgColor'=>"bf2908"))->addText(htmlspecialchars('ООО "ВКЛАД", Риэлтор '.$name.', тел.'.$phone),
            array('color'=>'ffffff'), array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT));
        $section->addText('Подборка от '.$tickets['created_at'],
            array('bold'=>true, 'size'=>16),
            array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
        $imageStyle = array('width'=>200,'align'=>'center');
        $styleFont = array('bold'=>true, 'size'=>14,'underline'=>'single');
        $count =0;
        foreach ($tickets['tickets'] as $ticket)
        {
            $count++;
            if($count==3){
                $section->addPageBreak();
                $table = $section->addTable($tableStyle);
                $table->addRow(null);
                $table->addCell(12000,array('bgColor'=>"bf2908"))->addText(htmlspecialchars('ООО "ВКЛАД", Риэлтор '.$name.', тел.'.$phone),
                    array('color'=>'ffffff'), array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT));
                $count=0;}

            $section->addText($ticket['object']['type'].', '.'№ объекта - '.$ticket['object_id'],$styleFont);
            $table = $section->addTable($tableStyle);
            $table->addRow(null);
            $table->addCell()->addText(htmlspecialchars(''));
            $table->addCell(4000,array('bgColor'=>"bf2908"))->addText(htmlspecialchars('Основные характеристики'),
                array('color'=>'ffffff'));
            $table->addCell(null,array('bgColor'=>"bf2908"))->addText(htmlspecialchars('Характеристика дома'),
                array('color'=>'ffffff'));
            $table->addRow();
           if($ticket['object']['main_photo'])
               $table->addCell()->addImage(public_path($ticket['object']['main_photo']['path'].$ticket['object']['main_photo']['name']), $imageStyle);
           else
               $table->addCell()->addImage(public_path('/images/not_found.jpg'), $imageStyle);

            $obfileds = Objects::GetObject($ticket['object_id']);
            $text =[];
            foreach ($obfileds[2] as $obfiled)
            {
                $text[] =$obfiled['title'].": ".$obfiled['value']??'';
            }
            $table->addCell(4000)->addText( implode('<w:br/>',$text));
            $text = [];
            foreach ($obfileds[1] as $obfiled)
            {

                $text[] =$obfiled['title'].": ".$obfiled['value']??'-';

            }
            $table->addCell()->addText( implode('<w:br/>',$text));

            foreach ($obfileds[3] as $obfiled)
            {
                $section->addText($obfiled['title'].": ".$obfiled['value'],
                    array('italic'=>'true')
                );
            }
           $section->addLine(['weight' => 1, 'width' => 800, 'height' => 0]);
        }

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="Подборка#'.$id.'.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save("php://output");


    }
}
