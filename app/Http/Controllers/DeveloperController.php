<?php

namespace App\Http\Controllers;

use App\Models\Address\Address;
use App\Models\Developer;
use App\Models\Object\Objects;
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class DeveloperController extends Controller
{
    public function index()
    {
    }
    public function developerCard($developerId,$tab=1, $edit=false){
        $developer = Developer::GetDeveloper($developerId);
        $view = View::make('developer.developerCard', [
            'tab' => $tab,
            'edit'=>$edit,
            'developer' => $developer,
            'developerId'=>$developerId
        ]);
        if($edit==true && $tab==1)
            $view->nest('child', 'developer.childDeveloperCard.edit.developer', ['developer' => $developer, 'developerId'=>$developerId]);
        else {
         if($tab==1)
            $view->nest('child', 'developer.childDeveloperCard.developer', ['developer' => $developer, 'developerId'=>$developerId]);
          if($tab==2) {
              $objects = Objects::GetObjectsOfDeveloper($developerId);
              $view->nest('child', 'developer.childDeveloperCard.objects', ['objects' => $objects]);
            }
        }
        return $view;
    }

    public function developerEdit(request $request)
    {
        if($request->ajax()){
            $params = $request->input('developer');
            $developer_id = $request->input('developerId');
            $developer = Developer::whereId($developer_id)->first();
            foreach ($params as $key => $param) {
                if($key!='address' && $key!='contacts' && $key !='undefined'){
                    if ($param != '')
                    $developer->$key = $param;
                    else   $developer->$key = null;
                }
                if( $key=='address' && $param != '' ){
                    $address_id = Address::where('value',$param)->pluck('id')->first();
                    if($address_id == null) {
                        $result = DadataSuggest::suggest("address", ["query" => $param, "count" => 1]);
                        $address_id = Address::get_id_or_create($result);
                    }
                    $developer->address_id = $address_id;
                }
            }
            $developer->save();
        }
        return response()->json(['params'=>$params]);
    }

    public function developersList(){
        $search = Cache::get('developers_search',"");
        $offset =  Cache::get('developers_page');
        $developers =  Developer::GetDevelopersList($offset,$search);
        $count = count($developers);
        $allcount =Developer::GetDevelopersListCount($search);
        return view('developer.developersList', ['developers' => $developers,'offset'=>$offset,'count'=>$count,'allcount'=>$allcount]);
    }
    public function developersSearch(request $request)
    {
        $search= $request->input('text');
        $offset =0;
        Cache::forever('developers_page', 0);
        Cache::forever('developers_search', $search);
        $developers  =  Developer::GetDevelopersList($offset,$search);
        $count = Developer::GetDevelopersListCount($search);
        return response()->json(['data'=> $developers ,'new_offset'=> $offset,'count'=>$count]);
    }

    public function developersGet(request $request){
        $search = Cache::get('developers_search',"");
        $offset =Cache::get('developers_page',0);
        $page = $request->input('page');
        if($page == 'next') $new_offset = $offset+15;
        if($page =="back") $new_offset = $offset-15;
        if($new_offset <0)$new_offset=0;
        $developers  =  Developer::GetDevelopersList($new_offset,$search);
        Cache::forever('developers_page',$new_offset);
        return Response()->json(['data'=> $developers ,'new_offset'=> $new_offset]);
    }
}
