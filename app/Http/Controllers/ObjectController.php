<?php

namespace App\Http\Controllers;
use App\Models\Object\{Objects,Media};
use App\Models\Catalog\{CatalogItems,Catalog};
use App\Models\Ticket\{Ticket,Comment,ScanDeal,Deal,NeedFields};
use App\Models\{Favorites,Client};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
class ObjectController extends Controller
{
    public function objectsList($tab=1,$drawer=null)
    {  //Cache::forever('objects_filter', '');
        $filter =Cache::get('objects_filter','');
        $filter_fields =null;
        $filter_fields =  CatalogItems::getFilter('filterObjectsFields');
        $text = Cache::get('objects_search','');
        if(Cache::get('object_tab') == $tab)
            $offset =  Cache::get('objects_page',0);
        else {
            $offset =0;
            Cache::forever('objects_page',0);
        };
        Cache::forever('object_tab', $tab);
        $user_id = Auth::user()->id;
        $objects=Objects::GetObjectsList($offset,$tab,$user_id,$text,$filter);
        $allcount = Objects::GetObjectsListCount($tab,$user_id,$text,$filter);
        if($drawer == "filter"){
            $drawer ="filter";
        }
        $legenda=  CatalogItems::getItemByCatalogName('ticketStages');

        return view('object.objectsList', ['legenda'=>$legenda,'objects' => $objects,'filter_values' => $filter,'filter_fields' => $filter_fields, 'drawer' =>$drawer, 'tab'=>$tab,'allcount'=>$allcount]);
    }
    public function getCoords(request $request){
        $tab=Cache::get('object_tab',1);
        $text = Cache::get('objects_search','');
        $filter =Cache::get('objects_filter','');
        $user_id = Auth::user()->id;
        $offset =Cache::get('objects_page',0);
        $objects=Objects::GetCoords($offset,$tab,$user_id,$text,$filter);
        return response()->json(['data' => $objects]);
    }
    public function filterSearch(request $request)
    {
        if ($request->ajax()) {
            $param = $request->input('ticket');
            $param = json_decode($param, true);
            Cache::forever('objects_page', 0);
            Cache::forever('objects_filter', $param);
            Cache::forever('objects_search', '');
            $offset =0;
            $tab=Cache::get('object_tab');
            $user_id = Auth::user()->id;

            //$objects=Objects::GetObjectsList($offset,$tab,$user_id,'',$param);
        }
        return response()->json(['data' => 'success']);

    }
    public function filterReset(request $request)
    {
            Cache::forever('objects_page', 0);
            Cache::forever('objects_filter', '');
            Cache::forever('objects_search', '');
            return response()->json(['data' => 'success']);
    }
    public function objectsSearch(request $request)
    {
        $text= $request->input('text');
        $offset =0;
        $user_id = Auth::user()->id;
        Cache::forever('objects_page', 0);
        Cache::forever('objects_search', $text);
        Cache::forever('objects_filter','');
        $tab=Cache::get('object_tab');
        $objects=Objects::GetObjectsList($offset,$tab,$user_id,$text);
        $objects_count=Objects::GetObjectsListCount($tab,$user_id,$text);
        return response()->json(['data'=>$objects,'new_offset'=> $offset,'count'=>$objects_count,'$text'=>$text]);
    }
    public function objectsGet(request $request){

        $text =Cache::get('objects_search', '');
        $filter =Cache::get('objects_filter','');
        $offset =Cache::get('objects_page',0);
        $tab=Cache::get('object_tab');
        $page = $request->input('page');
        $user_id = Auth::user()->id;
        if($page == 'next') $new_offset = $offset+15;
        if($page =="back") $new_offset = $offset-15;
        if($new_offset <0)$new_offset=0;
        $data = Objects::GetObjectsList($new_offset,$tab,$user_id,$text,$filter);
        $objects_count=Objects::GetObjectsListCount($tab,$user_id,$text,$filter);
        Cache::forever('objects_page', $new_offset);
        return response()->json(['data'=>$data,'new_offset'=> $new_offset,'count'=>$objects_count]);
    }
    public function objectCard($objectId,$ticketId,$tab=1,$edit =false)
    {
        $catalogs =  CatalogItems::getItemByCatalogName('fieldsObject');
        $comments = Comment::GetComments($ticketId);
        $view = View::make('object.objectCard', [
            'edit'=>$edit,
            'tab' => $tab,
            'ticketId' => $ticketId,
            'objectId' => $objectId,
            'comments'=>$comments['comments']
        ]);
        if(!$edit) {
            if ($tab == 1) {
                $ticket = Ticket::GetTicket($ticketId);
                $deal = Deal::where('ticket_sell_id',$ticketId)->get()->toArray();
                $view->nest('child', 'object.childObjectCard.ticket', ['ticket' => $ticket,'deal'=>$deal]);
            }
            if ($tab == 2) {
                $object = Objects::GetObject($objectId);
                $view->nest('child', 'object.childObjectCard.object', ['object' => $object]);
            }
            if ($tab == 3) {
                $media = Media::GetMediaObject($objectId);
                $view->nest('child', 'object.childObjectCard.media', ['media' => $media, 'tab' => $tab]);
            }
            if ($tab == 4) {
                $objects = Objects::GetSimilarObjects($objectId, 0, 10);
                $view->nest('child', 'object.childObjectCard.similar', ['objects' => $objects]);
            }
        }
        if($edit)
        {
            if ($tab == 2) {

                $object = Objects::GetObject_edit($objectId);
                $view->nest('child', 'object.childObjectCard.edit.object', [
                    'object' => $object,
                    'tab' => $tab,
                    'ticketId' => $ticketId,
                    'objectId' => $objectId,
                ]);
            }
        }
        return $view;
    }
    public function getTab(request $request, $objectId,$ticketId)
    {
        $message =array();
        $tab =  $request->input('tab');
        switch ($tab) {
            case "tabTicket":
                $message= Ticket::GetTicket($ticketId);
                break;
            case "tabObject":
                $message=Objects::getObject($objectId);
                break;
            case "tabMedia":
                $message='1';
                break;
            case "tabSimilar":
                $message='1';
                break;
            default:
                $status = 404;
                break;
        }
        return response()->json(['data'=>$message] );
    }
}
