<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Models\Object\{Objects,Media};
use App\Models\Catalog\{CatalogItems,Catalog};
use App\Models\Ticket\{Collection, Ticket, Comment, ScanDeal, Deal, NeedFields};
use App\Models\{Favorites,Client};
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class TicketController extends Controller
{
    public function index()
    {
    }

    public function findMyTicket(request $request)
    {
        $text= $request->input('text');
        $userId = Auth::user()->id;
        $tickets=Ticket::findMyTicket($text,$userId);
        return response()->json(['results'=>$tickets]);
    }
    public function ticketsSearch(request $request)
    {
        $text= $request->input('text');
        $offset =0;
        Cache::forever('tickets_page', 0);
        Cache::forever('tickets_search', $text);
        $tab=Cache::get('ticket_tab');
        $tickets=Ticket::searchTicketsInTab( $text,$tab,$offset);
        $tickets_count=Ticket::searchTicketsCount($text);
        return response()->json(['data'=>$tickets,'new_offset'=> $offset,'count'=>$tickets_count]);
    }
    public function ticketsGet(request $request){
        $offset =Cache::get('tickets_page',0);
        $tab=Cache::get('ticket_tab');
        $page = $request->input('page');
        $text = Cache::get('tickets_search', '');
        if($page == 'next') $new_offset = $offset+15;
        if($page =="back") $new_offset = $offset-15;
        if($new_offset <0)$new_offset=0;
        $tickets = Ticket::searchTicketsInTab($text,$tab,$new_offset);
        Cache::forever('tickets_page', $new_offset);
        return response()->json(['data'=>$tickets,'new_offset'=> $new_offset]);
    }
    public function TicketList($tab=1,$drawer=null)
    {
        $search = Cache::get('tickets_search',"");
        if(Cache::get('ticket_tab') == $tab)
            $offset =  Cache::get('tickets_page',0);
        else {
            Cache::forever('tickets_page',0);
            $offset=0;
        }
        Cache::forever('ticket_tab', $tab);
        $tickets = Ticket::searchTicketsInTab($search,$tab,$offset);
        $countTickets = Ticket::searchTicketsCount($search);
        $allcount =$countTickets[$tab];
        $objectTypes =null;
        if($drawer == 'createticket'){
            $drawer ="createticket";
            $objectTypes= CatalogItems::getItemByCatalogName('objectTypes');
        }
        else $drawer =false;
        $legenda=  CatalogItems::getItemByCatalogName('ticketStages');
        return view('ticket.ticketsList', ['legenda'=>$legenda,'allcount'=>$allcount,'tickets' => $tickets,'tab'=>$tab,'countTickets'=>$countTickets,'drawer' =>$drawer,'objectTypes'=>$objectTypes]);

    }
    public function ticketCard($ticketId,$tab=1, $edit=false)
    {

        $userId = Auth::user()->id;
        $ticket = Ticket::GetTicket($ticketId);
        $ticketSS = Ticket::GetTicketStatusStage($ticketId);
        $ticketType = Ticket::GetTicketType($ticketId);
        $favoriteStatus =  Favorites::GetStatus( $ticketId,$userId,'ticket');
        $comments = Comment::GetComments($ticketId);

        $dealId=null;
        if($ticketType==1) {
            if(Deal::where('ticket_sell_id',$ticketId)->get()->count()!=0)
                $dealId =Deal::where('ticket_sell_id',$ticketId)->first()->toArray()['id'];

        }
        else {
            if(Deal::where('ticket_buy_id',$ticketId)->get()->count()!=0)
                $dealId =Deal::where('ticket_buy_id',$ticketId)->first()->toArray()['id'];

        }

        $view = View::make('ticket.ticketCard', [
            'tab' => $tab,
            'ticketId' => $ticketId,
            'favoriteStatus'=>$favoriteStatus,
            'ticketSS'=>$ticketSS,
            'comments'=>$comments['comments'],
            'edit'=>$edit,
            'ticketType'=>$ticketType,
            'dealId'=>$dealId
        ]);
        if($edit==true){
            if($tab==1) {
                $ticket = Ticket::GetTicket($ticketId);
                $needFields = NeedFields::get_need_fields(true,$ticketId );
                if($ticket['params']['type']['value']==1)
                    $view->nest('child', 'ticket.childTicketCard.sell.edit.about_ticket', ['ticket' => $ticket,
                        'edit'=>$edit,
                        'dealId'=>$dealId
                    ]);
                else  $view->nest('child', 'ticket.childTicketCard.buy.edit.about_ticket', ['ticket' => $ticket,
                    'needFields' => $needFields,
                    'dealId'=>$dealId]);
            }
            if($tab==4) {
                $scan_deal = ScanDeal::where('deal_id',$dealId)->get();
                if($ticketType==1)
                    $view->nest('child', 'ticket.childTicketCard.sell.edit.deal',
                        ['ticket' => $ticket,
                            'edit'=>$edit,
                            'tab'=>$tab,
                            'scan_deal'=>$scan_deal
                        ]);
                else  $view->nest('child', 'ticket.childTicketCard.buy.edit.deal', [
                    'edit'=>$edit,
                    'tab'=>$tab,
                    'scan_deal'=>$scan_deal]);
            }
        }
        else {
            if($tab==2)
            {
                $tickets = Ticket::GetCollection($ticketId);
                    $view->nest('child', 'ticket.childTicketCard.buy.collection', [
                        'tickets' => $tickets,
                    ]);
                }
            if($tab==1)
            {
                $ticket = Ticket::GetTicket($ticketId);
                if($ticket['params']['type']['value']==1)
                    $view->nest('child', 'ticket.childTicketCard.sell.about_ticket', ['ticket' => $ticket,'dealId'=>$dealId]);
                else {
                    $needFields = NeedFields::get_need_fields(false,$ticketId );
                    $view->nest('child', 'ticket.childTicketCard.buy.about_ticket', [
                        'ticket' => $ticket,
                        'needFields' => $needFields,
                        'dealId'=>$dealId]);
                }
            }
            if($tab==4)
            {
                $scan_deal = ScanDeal::where('deal_id',$dealId)->get();
                if($ticketType==1)
                {
                    $deal =Deal::getDeal('ticket_sell_id',$ticketId);
                    $view->nest('child', 'ticket.childTicketCard.sell.deal', [
                        'deal'=>$deal,
                        'edit'=>$edit,
                        'tab'=>$tab,
                        'scan_deal'=>$scan_deal]);
                }
                else {
                    $deal =Deal::getDeal('ticket_buy_id',$ticketId);
                    $view->nest('child', 'ticket.childTicketCard.buy.deal', [
                        'edit'=>$edit,
                        'deal'=>$deal,
                        'scan_deal'=>$scan_deal,
                        'tab'=>$tab,
                    ]);
                }

            }
        }
        return $view;
    }
}
