<?php

namespace App\Http\Controllers;
use App\Charts\SampleChart;
use App\Models\Developer;
use App\Models\Ticket\Deal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use Jenssegers\Date\Date;
class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function auth()
    {
        return view('auth.register');
    }
}
