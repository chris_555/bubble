<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FlatsKeepMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'objectKeep',  'title' => 'Состояние')
        );


        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "BUILD", "title": "Ремонт от строителей", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "COSMETIC", "title": "Косметический ремонт", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "GOOD", "title": "Современный ремонт", "value": 3}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
               [
                'catalog_id' => $id,
                'item'=>'{"name": "EURO", "title": "Евроремонт", "value": 4}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "BLACK", "title": "Черновая отделка", "value": 5}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]


            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'objectKeep')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
    }
}
