<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalodobjectsStudioType extends Migration
{
 protected $id;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'objectStudioType', 'title' => 'Планировка')
        );


        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "NO", "title": "Обычная планировка", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "EURO", "title": "Кухня-гостинная", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"name": "STUDIO", "title": "Студия", "value": 3}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'objectStudioType')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
    }
}
