<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createсatalogs extends Migration
{
    /**
     * Run the migrations
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',255);
            $table->string('title',255);
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('catalogItems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('catalog_id');
            $table->string('item',1024);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('catalog_id')->references('id')->on('catalogs')   ->onDelete('cascade');;
        });

        Schema::create('entityCatalogItems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('entity_id');
            $table->integer('entity_type');
            $table->integer('item_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('item_id')->references('id')->on('catalogItems')->onDelete('cascade');;

        });

        $id = DB::table('catalogs')->insertGetId(
            array(
                'name' => 'objectTypes' ,
                'title' => 'Тип объекта',
                )
        );


        DB::table('catalogItems')->insert([

            [
            'catalog_id' => $id,
            'item'=>'{"title": "Номер", "value": 4}',
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
            'catalog_id' => $id,
            'item'=>'{"title": "Малосемейка", "value": 3}',
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
            'catalog_id' => $id,
            'item'=>'{"title": "Пансионат", "value": 2}',
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'catalog_id' => $id,
                'item'=>'{"title": "Квартира", "value": 1}',
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'catalog_id' => $id,
                'item'=>'{"title": "Комната", "value": 5}',
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
            'catalog_id' => $id,
            'item'=>'{"title": "Новостройка", "value": 6}',
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),
            ]
            ]
        );

        $id = DB::table('catalogs')->insertGetId(
            array(
                'name' => 'objectClass' ,
                'title' => 'Класс объекта',
            )
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Новостройка", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Вторичная", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]
            ]
        );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('entityCatalogItems', function (Blueprint $table) {
          $table->dropForeign('entitycatalogitems_item_id_foreign');
      });
    Schema::table('catalogItems', function (Blueprint $table) {
           $table->dropForeign('catalogitems_catalog_id_foreign');
      });

    Schema::dropIfExists('catalogItems');
    Schema::dropIfExists('catalogs');
    Schema::dropIfExists('entityCatalogItems');

    }
}
