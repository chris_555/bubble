<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type');
            $table->bigInteger('address_id')->nullable();//на время нул
            $table->float('area')->nullable();;
            $table->integer('num_rooms')->nullable();
            $table->integer('num_bedrooms')->nullable();
            $table->integer('num_bathrooms')->nullable();
            $table->integer('floor')->nullable();
            $table->float('area_kitchen')->nullable();
            $table->float('celling_height')->nullable();
            $table->string('flat',10)->nullable();
            $table->integer('year_construct')->nullable();
            $table->integer('developer_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('address_id')->references('id')->on('addresses');
        });
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_main');
            $table->integer('object_id');
            $table->string('name');
            $table->string('path');
            $table->foreign('object_id')->references('id')->on('objects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
        Schema::dropIfExists('objects');
    }
}
