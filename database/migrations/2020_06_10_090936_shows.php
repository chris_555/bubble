<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Shows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ticket_buy_id');
            $table->integer('ticket_sell_id');
            $table->timestamp('date_time')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ticket_buy_id')->references('id')->on('tickets')   ->onDelete('cascade');;
            $table->foreign('ticket_sell_id')->references('id')->on('tickets')   ->onDelete('cascade');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
