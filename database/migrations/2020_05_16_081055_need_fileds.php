<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NeedFileds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('needFields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('source')->nullable();
            $table->string('source_name')->nullable();
            $table->softDeletes();
        });
        Schema::table('ticketNeeds', function (Blueprint $table) {
        $table->foreign('field_id')->references('id')->on('needFields') ->onDelete('cascade');
    });
            DB::table('needFields')->insert([
                    [
                        'title'=>'price-min',
                        "description" => 'Цена',
                        "source" => 'table',
                        "source_name" => 'tickets',
                    ],
                    [
                        'title'=>'price-max',
                        "description" => 'Цена',
                        "source" => 'table',
                        "source_name" => 'tickets',
                    ],
                    [
                        'title'=>'address',
                        "description" => 'Местоположение',
                        "source" => 'table',
                        "source_name" => 'tickets',
                    ],
                    [
                        'title'=>'objectKeep',
                        "description" => 'Состояние',
                        "source" => 'catalog',
                        "source_name" => 'objectKeep',
                    ],
                    [
                        'title'=>'objectClass',
                        "description" => 'Класс объекта',
                        "source" => 'table',
                        "source_name" => 'objectClass',
                    ],
                    [
                        'title'=>'area-min',
                        "description" => 'Общая площадь',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'area-max',
                        "description" => 'Общая площадь',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'floor-max',
                        "description" => 'Этаж',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'floor-min',
                        "description" => 'Этаж',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'num_rooms',
                        "description" => 'Количество комнат',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'year_construct-min',
                        "description" => 'Год постройки',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                    [
                        'title'=>'year_construct-max',
                        "description" => 'Год постройки',
                        "source" => 'table',
                        "source_name" => 'objects',
                    ],
                ]
            );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticketNeeds', function (Blueprint $table) {
        $table->dropForeign('ticketneeds_field_id_foreign');
    });
        Schema::dropIfExists('needFields');
    }
}
