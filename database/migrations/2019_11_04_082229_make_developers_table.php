<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name')->nullable();
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('name_of_org');
            $table->string('full_name_of_org');
            $table->bigInteger('inn');//12
            $table->bigInteger('ogrn');//13
            $table->bigInteger('kpp');//9
            $table->string('date_registration');
            $table->integer('address_id')->nullable();
            $table->string('phones',1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('objects', function (Blueprint $table) {
            $table->foreign('developer_id')->references('id')->on('developers')   ->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('objects', function (Blueprint $table) {
        $table->dropForeign('objects_developer_id_foreign');
    });
        Schema::dropIfExists('developers');
    }
}
