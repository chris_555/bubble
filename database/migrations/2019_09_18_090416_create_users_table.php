<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('last_name',100);
            $table->string('first_name',100);
            $table->string('second_name',100);
            $table->integer('position_id');
            $table->string('login',50)->unique();
            $table->string('email',50)->unique()->nullable();
            $table->string('phone',12)->unique();
            $table->date('date_of_birth');
            $table->integer('gender');
            $table->string('password',255);
            $table->foreign('position_id')->references('id')->on('positions');
            $table->timestamps();
            $table->softDeletes();

        });

        DB::table('positions')->insert([
            'name' =>'Риэлтор',
        ]);
        DB::table('positions')->insert([
            'name' =>'Администратор',
        ]);
        DB::table('users')->insert([
            'login' => 'test',
            'name' =>'Иванов Иван Иванович',
            'email' =>'test@mail.ru',
            'first_name' =>'Иван',
            'date_of_birth' => '01.01.1979',
            'position_id' =>1,
            'gender' =>1,
            'phone' =>'79221111111',
            'second_name' =>'Иванов',
            'last_name' =>'Иванович',
            'password' => bcrypt('12345678'),
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),
        ]);
        DB::table('users')->insert([
            'login' => 'kriska_555',
            'name' =>'Кырова Кристина Андреевна',
            'email' =>'kriska_555@mail.ru',
            'first_name' =>'Кристина',
            'date_of_birth' => '01.01.1979',
            'position_id' =>1,
            'gender' =>2,
            'phone' =>'79227960195',
            'second_name' =>'Кырова',
            'last_name' =>'Андреевна',
            'password' => bcrypt('12345678'),
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('positions');
    }
}
