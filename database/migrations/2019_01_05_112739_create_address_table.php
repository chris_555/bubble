<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('city_name');
            $table->string('city_kladr')->nullable();
            $table->string('city_with_type');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('settlements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('settlement_name');
            $table->string('settlement_kladr')->nullable();
            $table->string('settlement_with_type');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('streets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('street_name');
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('settlement_id')->nullable();
            $table->string('street_kladr')->nullable();
            $table->string( 'street_with_type');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('settlement_id')->references('id')->on('settlements')   ->onDelete('cascade');;


        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value',500);
            //$table->integer('cityId');
            //$table->integer('settlementId')->nullable();
            $table->integer('street_id')->nullable();;
            $table->string('house_kladr')->nullable();
            $table->string('house')->nullable();;
            $table->string('block',10)->nullable();
            $table->string('geo_lon',255);
            $table->string('geo_lat',255);
            $table->timestamps()  ;
            $table->softDeletes()  ;

           //$table->foreign('city_id')->references('id')->on('cities');
          // $table->foreign('settlement_id')->references('id')->on('settlement');
            $table->foreign(['street_id'])->references('id')->on('streets')   ->onDelete('cascade');;

        });







    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign(['street_id']);
        });

        Schema::table('streets', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });

        Schema::table('streets', function (Blueprint $table) {
            $table->dropForeign(['settlement_id']);
        });

        Schema::dropIfExists('streets');
        Schema::dropIfExists('settlements');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('addresses');

    }
}
