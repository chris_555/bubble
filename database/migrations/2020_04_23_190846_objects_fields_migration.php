<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObjectsFieldsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'objectWalls', 'title' => 'Материал стен')
        );


        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Бетонные", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Газоблок", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Деревянные", "value": 3}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Монолитные", "value": 4}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Кирпич красный", "value": 5}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'objectWalls')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
    }
}
