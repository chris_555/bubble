<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name',100)->nullable();
            $table->string('first_name',100);
            $table->string('second_name',100)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth',500)->nullable();
            $table->string('pass_num')->nullable();
            $table->string('pass_serial')->nullable();
            $table->string('pass_gave_by')->nullable();
            $table->date('pass_gave_when')->nullable();
            $table->string('pass_gave_code')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('snils')->nullable();
            $table->string('address_registration',500)->nullable();
            $table->integer('gender')->nullable();
            $table->string('phone',12)->nullable();
            $table->string('email',50)->nullable();;
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
