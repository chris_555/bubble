<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FieldsObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'fieldsObject' ,'title' => 'Поля для объекта')
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "objectKeep"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "objectWalls"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "objectElevator"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "objectStudioType"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'fieldsObject')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
    }
}
