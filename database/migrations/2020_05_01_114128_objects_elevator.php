<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObjectsElevator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'objectElevator' ,'title' => 'Лифт')
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Нет", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{ "title": "Грузовой", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{ "title": "Пассажирский", "value": 3}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{ "title": "Пассажирский и грузовой", "value": 4}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $id = DB::table('catalogs')->where('name', 'objectElevator')->value('id');
    //    $catalogs_items_entity = EntityCatalogItems::where('catalog_id',$id)->pluck('id')->toArray();

        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
    }
}
