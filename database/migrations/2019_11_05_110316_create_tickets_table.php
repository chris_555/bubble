<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('type');//продажа или покупка
            $table->integer('status');
            $table->integer('stage');
            $table->integer('client_id');
            $table->integer('user_id');
            $table->integer('object_id')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references('id')->on('clients')   ->onDelete('cascade');;
            $table->foreign('user_id')->references('id')->on('users')   ->onDelete('cascade');;
            $table->foreign('object_id')->references('id')->on('objects')   ->onDelete('cascade');;

        });

        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ticket_buy_id');
            $table->integer('ticket_sell_id');
            $table->double('price');
            $table->double('deposit')->nullable();;
            $table->integer('day_payment')->nullable();;
            $table->date('date');
            $table->foreign('ticket_buy_id')->references('id')->on('tickets')   ->onDelete('cascade');;
            $table->foreign('ticket_sell_id')->references('id')->on('tickets')   ->onDelete('cascade');;
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticketNeeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticket_id');
            $table->bigInteger('field_id');
            $table->string('value',500);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ticket_id')->references('id')->on('tickets')   ->onDelete('cascade');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::table('ticketNeeds', function (Blueprint $table) {
            $table->dropForeign('ticketneeds_ticket_id_foreign');
        });
        Schema::table('deals', function (Blueprint $table) {
            $table->dropForeign('deals_ticket_sell_id_foreign');
        });
        Schema::table('deals', function (Blueprint $table) {
            $table->dropForeign('deals_ticket_buy_id_foreign');
        });
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('ticketNeeds');
        Schema::dropIfExists('deals');
        Schema::dropIfExists('ticketNeeds');
    }
}
