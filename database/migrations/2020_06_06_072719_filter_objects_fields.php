<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FilterObjectsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'filterObjectsFields' ,'title' => 'Поля фильтра объектов')
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table", "typeField":"between", "type":"number", "source":[{"title": "area-min"}, {"title": "area-max"}], "name": "area","title": "Площадь" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"main","maska":"price", "where_to_look":"table", "typeField":"between", "type":"text", "source":[{"title": "price-min"}, {"title": "price-max"}], "name": "price","title": "Стоимость" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"main", "source":"catalog","where_to_look":"table",  "typeField":"select", "name": "objectTypes" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"catalog", "source":"catalog","typeField":"select", "name": "objectStudioType" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"catalog", "source":"catalog","typeField":"select", "name": "objectKeep" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"catalog", "source":"catalog","typeField":"select", "name": "objectWalls" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"catalog", "source":"catalog","typeField":"select", "name": "objectElevator" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],

                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table", "typeField":"between", "type":"number", "source":[{"title": "area_kitchen-min"}, {"title": "area_kitchen-max"}], "name": "area_kitchen","title": "Площадь кухни" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"main","where_to_look":"table","typeField":"checkbox", "source":[{"value": 1, "title": "1к"}, {"value": 2, "title": "2к"},{"value": 3, "title": "3к"}, {"value": "4", "title": "4к+"}], "name": "num_rooms","title": "Комнатность" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table","typeField":"checkbox", "source":[{"value": 1, "title": "1к"}, {"value": 2, "title": "2к"},{"value": 3, "title": "3к"}, {"value": "4", "title": "4к+"}], "name": "num_bathrooms","title": "Количество ванных комнат" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table","typeField":"checkbox", "source":[{"value": 1, "title": "1к"}, {"value": 2, "title": "2к"},{"value": 3, "title": "3к"}, {"value": "4", "title": "4к+"}], "name": "num_bedrooms","title": "Количество спальных комнат" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table","typeField":"between", "type":"number", "source":[{"title": "floor-min"}, {"title": "floor-max"}], "name": "floor","title": "Этаж" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table","typeField":"between",  "type":"number", "source":[{"title": "celling_height-min"}, {"title": "celling_height-max"}], "name": "celling_height","title": "Высота потолков" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"table","typeField":"between", "type":"number", "source":[{"title": "year_construct-min"}, {"title": "year_construct-max"}], "name": "year_construct","title": "Год сдачи" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional", "where_to_look":"search","source":"search","typeField":"select", "name": "user_id", "title":"Риэлтор" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"block":"additional","where_to_look":"search", "source":"search","typeField":"select", "name": "developer_id", "title":"Застройщик" }',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'filterObjectsFields')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
    }
}
