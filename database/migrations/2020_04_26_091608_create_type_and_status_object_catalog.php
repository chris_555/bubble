<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeAndStatusObjectCatalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'ticketTypes', 'title' => 'Тип')
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Продажа", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Покупка", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );

        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'ticketStatus', 'title' => 'Статус')
        );
        DB::table('catalogItems')->insert([
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "В работе", "value": 1}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Отменена", "value": 2}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Завершена c успехом", "value": 3}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Удалена", "value": 4}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );


        $id = DB::table('catalogs')->insertGetId(
            array('name' => 'ticketStages', 'title' => 'Этап')
        );
        DB::table('catalogItems')->insert([

            //buy
                [
                    'catalog_id' => $id,
                    'item'=>'{  "title": "Выявление потребностей","value": 1, "type":1 , "color": "#7741e1"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Подборка", "value": 2, "type":1, "color": "#dbcc7d"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                        'item'=>'{"title": "Заключение сделки", "value": 3, "type":1, "color": "#cfebcf"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],

                //sell

                [
                    'catalog_id' => $id,
                    'item'=>'{  "title": "Заполнение объекта","value": 1, "type":2 , "color": "#4169E1"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Поиск покупателей", "value": 2, "type":2 , "color": "#FFD700"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ],
                [
                    'catalog_id' => $id,
                    'item'=>'{"title": "Заключение сделки", "value": 3, "type":2,  "color": "#32CD32"}',
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]

            ]
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $id = DB::table('catalogs')->where('name', 'ticketTypes')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
        $id = DB::table('catalogs')->where('name', 'ticketStages')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
        $id = DB::table('catalogs')->where('name', 'ticketStatuses')->value('id');
        DB::table('catalogItems')->where('catalog_id', '=', $id)->delete();
        DB::table('catalogs')->where('id', '=', $id)->delete();
    }
}
