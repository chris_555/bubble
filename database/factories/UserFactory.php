<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Catalog\{CatalogItems,Catalog};
use Illuminate\Support\Facades\Storage;
use App\Models\Ticket\{Ticket,Comment,ScanDeal,Deal,NeedFields};
use App\Models\{Developer, Favorites, Client, User};
use App\Models\Object\{Objects,Media};

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Developer::class, function (Faker $faker) {
    $fio= explode(" ",$faker->name);
    return [
        'name_of_org' => "Застройщик".$faker->numberBetween(1,200),
        'full_name_of_org' => "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ Застройщик".$faker->numberBetween(1,200),
        'last_name' => $fio[1],
        'first_name' => $fio[0],
        'second_name' => $fio[2],
        'date_registration' => $faker->date('Y-m-d'),
        'address_id'=>$faker->numberBetween(1,10),
        'ogrn' => $faker->numerify('#############'),
        'inn' => $faker->numerify('############'),
        'kpp' => $faker->numerify('#########'),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
      /*  "phones"=>[
            [
                '1'=>['phone'=> 73452608257,"position" =>'Генеральный директор']
            ]
        ]
*/
        //"phones"=>'{"1": {"phone": "73452608257","position":"Генеральный директор"},"2": {"phone": "79220002379","position":"Отдел продаж"}}'
    ];
});
$factory->define(User::class, function (Faker $faker) {
    $fio= explode(" ",$faker->name);
    return [
        'name' => $faker->name,
        'last_name' => $fio[1],
        'first_name' => $fio[0],
        'second_name' => $fio[2],
        'position_id' => 1,
        'date_of_birth' => $faker->date('Y-m-d'),
        'gender' => $faker->numberBetween(1,2),
        'login' => $faker->userName.'_'.$faker->numerify('###'),
        'password' => bcrypt('12345678'), // password
        'phone' => $faker->numerify('79#########'),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
    ];
});

$factory->define(Objects::class, function (Faker $faker) {
    $addreses_id = \App\Models\Address\Address::whereNotNull('house')->pluck('id')->toArray();
    $developers_id = \App\Models\Developer::pluck('id')->toArray();
    $area =$faker->randomFloat(3,10,100);
    return [
        'year_construct' => $faker->year(2020),
        'area' => $faker->randomFloat(3,30,100),
        'area_kitchen' => $faker->randomFloat(3,10,$area-20),
        'celling_height' => $faker->randomFloat(2,1,2),
        'flat' => $faker->numberBetween(1,100),
        'floor' => $faker->numberBetween(1,19),
        'num_rooms' => $faker->numberBetween(1,5),
        'num_bathrooms' => $faker->numberBetween(1,2),
        'developer_id'=>Arr::random($developers_id),
        'num_bedrooms' => $faker->numberBetween(1,2),
        'address_id' => Arr::random($addreses_id),
        'type' => $faker->numberBetween(1,6),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()

    ];
});

$factory->define(Client::class, function (Faker $faker) {
    $fio= explode(" ",$faker->name);
    $addreses_id = \App\Models\Address\Address::whereNotNull('house')->pluck('value')->toArray();
    $pass_gave_by = ['отделом УФМС России по Ханты-Манийскому автоном. окр. - Югре Cургутского района',
        'отделом МВД по г. Тюмени',
        'отделом МВД по г. Сургут',
        'отделом УФМС России по Республике Башкортостан в городе Бирск',
        ];
    return [
        'last_name' => $fio[1],
        'first_name' => $fio[0],
        'second_name' => $fio[2],
        'pass_num' => $faker->numerify('######'),
        'pass_serial' => $faker->numerify('#####'),
        'pass_gave_code'=> $faker->numerify('###').'-'.$faker->numerify('###'),
        'pass_gave_by'=> Arr::random($pass_gave_by),
        'address_registration'=>Arr::random($addreses_id),
        'pass_gave_when'=> $faker->date('Y-m-d','01.01.2000'),
        'date_of_birth' => $faker->date('Y-m-d'),
        'phone' => $faker->numerify('79#########'),
        'gender' => $faker->numberBetween(1,2),
        'email' => $faker->email,
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
    ];
});

$factory->define(Ticket::class, function (Faker $faker) use ($factory){
    $user_ids = User::pluck('id')->toArray();
    $description =[
        "Предлагаем вашему вниманию уютную квартиру формата 1+. В квартире сделан косметический ремонт, установлены пластиковые окна. Санузел обшит пластиковыми панелями, на полу выложена плитка. Очень просторный балкон на 2 комнаты обшит и застеклен. При продаже остается кухонный гарнитур. Дом расположен в районе с хорошей транспортной развязкой, можно удобно добраться в любую часть города как на машине так и на общественном транспорте. Рядом с домом в шаговой доступности расположены детские сады и школы, а также магазины у дома и ТЦ Солнечный. ",
        "Продам отличную просторную однокомнатную квартиру общей площадью 44,2 м.кв., в любимом многими районе Дом обороны. Квартира светлая, окна выходят на солнечную сторону. Комната - 19,8 м.кв., кухня - 10,1 м.кв., прихожая - 10,5 м.кв., ванная комната - 3,8 м.кв. Во всей квартире смонтированы новые натяжные потолки. На полу в кухне и в коридоре новая плитка. На стенах свежие обои. Санузел совмещен. Лоджия просторная, светлая застеклена пластиковыми окнами.В самом микрорайоне очень хорошо развита инфраструктура: ",
        "Отличная квартира формата 1 плюс, в развитом районе города!Квартира на среднем этаже – самый оптимальный вариант! И на лифте можно доехать, и пешком не утомительно подняться. Квартира очень просторная, светлая с высокими потолками, хорошими окнами. Квартира индивидуальной планировки, исполнение дома – панельное. В подъезде всегда чисто, моют этажи каждую неделю. Балкон довольно просторный Прекрасный вариант для молодых семей. Оптимальное жилье для каждого независимо от возраста, профессии и привычек.",
        "Предлагается вашему вниманию просторная 2ух комнатная квартира-распашонка! Сделан недавно косметический ремонт. Площадь квартиры 48 кв.м. Имеется большой застеклённый балкон.Отличная планировка, удобная для комфортного расположения мебели. При продаже остается кухонный гарнитур со встроенной бытовой техникой. В прошлом году сделали новую детскую площадку. Придомовая территория оборудована парковочными местами и детской площадкой. Все необходимое рядом: магазины, супермаркеты, аптеки, школы и дошкольные учреждения, остановки общественного транспорта. Двор тихий ",
        "Предлагается к продаже эксклюзивная просторная однокомнатная квартира, в которой высота потолков 3,7м!!! в ЖК \"Ново-Патрушево\". Данная квартира - это широкий простор для всех Ваших самых смелых идей и фантазий, великолепный вариант, чтобы сделать ремонт своей мечты! В Квартире выполнен хороший ремонт, вся мебель остается при продаже, что позволяет заехать и жить. В ванной комнате выложена плитка, установлена сан.техника. В подъезде два лифта - грузовой и пассажирский. Во дворе спортивные и детские площадки, достаточно парковочных мест. ",
        "Уважаемые покупатели! Продается замечательная квартира с косметическим ремонтом. Очень тихий и зеленый центр города, район \"кпд\", полностью кирпичный дом, не хрущевка, расположен на второй линии, вдали от шумных и пыльных дорог.Придомовая территория оборудована парковочными местами и детской площадкой. Все необходимое рядом: магазины, супермаркеты, аптеки, школы и дошкольные учреждения, остановки общественного транспорта. Расположена рядом с двумя школами 4 и 6 (школа с музыкальным уклоном)",
        "Продаем квартиру в проверенном кирпичном доме. Закрытая придомовая территория и парковка у дома создают удобство для автовладельцев. В квартире ведется ремонт Частично фото теряют актуальность, скоро сделаем новые. На данный момент полностью обновлен санузел (кафель, новая сантехника) убраны все межкомнатные двери. Постелен ламинат. Ведется ремонт кухни. В одной из комнат сделан ремонт. Квартира-распашонка, благодаря чему дневной свет освещает ее с двух сторон. Каждая комната изолирована, благодаря чему у всех членов семьи будет свой уголок",
        "Продается уютная двухкомнатная квартира в Восточном районе города. Новый кирпичный дом! По периметру дома и в лифте установлены видеокамеры. В квартире выполнен современный ремонт из качественных материалов. При продаже остается кухонный гарнитур, а так же частично бытовая техника. В спальне и сан. узле натяжные потолки, на полу ламинат. Сан узел в кафеле. Очень востребованный район города. Прекрасная транспортная развязка. Очень много скверов - Энергетиков, Рябиновый, Серебряные ключи, бульвар им.Косухина, крупные магазины - Мебельград, Окей, Магнит."
        ];
    $num =$faker->numberBetween(1,9000);
    if($num & 1)
    $ar = [
        'user_id' => Arr::random($user_ids),
        'client_id' => factory(Client::class)->create()->id,
        'status' => $faker->numberBetween(1,2),
        'title' => 'Заявка на покупку',
        'type' => 2,
        'stage' => $faker->numberBetween(1,2),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
    ];

   else $ar =  [
        'user_id' => Arr::random($user_ids),
        'client_id' => factory(Client::class)->create()->id,
        'description' => Arr::random($description),
        'object_id' => factory(Objects::class)->create()->id,
        'price' => $faker->numberBetween(1000000,5000000),
        'status' => $faker->numberBetween(1,2),
        'title' => 'Заявка на продажу',
        'type' => 1,
        'stage' => $faker->numberBetween(1,2),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
    ];
    return $ar;
});

$factory->define(Deal::class, function (Faker $faker) use ($factory){
    $ticket_id_sell1 = Deal::pluck('ticket_sell_id')->toArray();
    $ticket_id_sell  = Ticket::whereNotIn('id', $ticket_id_sell1)->where('type',1)->pluck('id')->toArray();

    $ticket_id_buy1 = Deal::pluck('ticket_buy_id')->toArray();
    $ticket_id_buy  = Ticket::whereNotIn('id', $ticket_id_buy1)->where('type',2)->pluck('id')->toArray();

    $ticket_id_sell = Arr::random($ticket_id_sell);
    $ticket_id_buy = Arr::random($ticket_id_buy);
    Ticket::whereIn('id', [$ticket_id_sell,$ticket_id_buy])->update(array('status' =>3,'stage'=>3));
    return [
        'ticket_buy_id' => $ticket_id_buy,
        'ticket_sell_id' => $ticket_id_sell,
        'price' =>  $faker->numberBetween( 1000000,5000000),
        'deposit' =>  $faker->numberBetween( 20,400),
        'day_payment' =>  $faker->numberBetween(5,30),
        "date" => $faker->dateTimeBetween('-1 year','now'),
        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
    ];
});


$factory->define(Media::class, function (Faker $faker) use ($factory){
    $objectsIds = Objects::pluck('id')->toArray();
    $path = 'images/uploads/media_objects/fake_media/';
    $names= array_slice(scandir(public_path($path)), 2);
    return [
        'is_main' => true,
        'object_id' => Arr::random($objectsIds),
        "path" => $path,
        "name" => Arr::random($names),
    ];
});


