<?php

use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Database\Seeder;
use App\Models\Catalog\{CatalogItems,Catalog};
use App\Models\Ticket\{Ticket,Comment,ScanDeal,Deal,NeedFields};
use App\Models\{Address\Address, Favorites, Client};
use App\Models\Object\{Objects,Media};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class , 30)->create();
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Мельникайте']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Азовская']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Тюменская']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Базинская']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Ленина']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Мельничная']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюмень Республики']);
        Address::put_addresses($result);
        $result = DadataSuggest::suggest("address", ["query"=>'Тюменская обл, г Тюмень']);
        Address::put_addresses($result);
        factory(App\Models\Developer::class , 100)->create();
        factory(App\Models\Ticket\Ticket::class , 1000)->create();
        factory(App\Models\Ticket\Deal::class , 600)->create();
        factory(App\Models\Object\Media::class , 900)->create();

        $catalogs_id = Catalog::whereIn('name',['objectStudioType','objectKeep','objectWalls','objectElevator'])->pluck('id')->toArray();
        $objectsIds = Objects::pluck('id')->toArray();
        foreach ($objectsIds as $objectId) {
            foreach ($catalogs_id as $catalog_id) {
                $itemsIds = CatalogItems::where('catalog_id', $catalog_id)->pluck('id')->toArray();
                DB::table('entityCatalogItems')->insert([
                    'entity_id' => $objectId,
                    'entity_type' => 4,
                    'item_id' => Arr::random($itemsIds)
                ]);
            }
        }

    }
}
