
<?php

use App\Enums\ObjectStatus;
use App\Enums\ObjectType;
return [

    ObjectStatus::class => [
        ObjectStatus::Active => 'Активен',
        ObjectStatus::Closed => 'Закрыт',
        ObjectStatus::InDeal => 'Оформляется',
    ],
    ObjectType::class => [
        ObjectType::New => 'Новостройка',
        ObjectType::Vtor=> 'Вторичка',

    ],
];