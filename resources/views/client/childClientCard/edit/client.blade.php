<div class ="block-content">
    <h3>
        Общая информация</br>
    </h3>
    @component('components.InputInline', [
    'value' => $client['first_name'],
    'title' => 'Имя',
    'id'=>'first_name',
    ])
    @endcomponent

    @component('components.InputInline', [
    'value' => $client['second_name'],
    'title' => 'Фамилия',
    'id'=>'second_name',
    ])
    @endcomponent

    @component('components.InputInline', [
    'value' => $client['last_name'],
    'title' => 'Отчество',
    'id'=>'last_name',
    ])
    @endcomponent

    <div class="input_inline_block ">
        <div class="input-title_inline" style='width: 150px'>Пол<span class="req">*</span></div>

        <input
            @if($client['gender'] ==2 ) checked @endif
        type=radio
            id=female
            name="gender"
            value='female' >
        <label class=bspace
               style ='margin-left: 12px'
               for = female>Женский</label>

        <input
            @if($client['gender'] ==1 ) checked @endif
        type=radio
            id=male
            name="gender"
            value='male'>
        <label class=bspace
               for = male>Мужской</label>

    </div>

    @component('components.InputInline', [
    'value' => $client['date_of_birth'],
    'type'=> 'date',
    'title' => 'Дата рождения',
    'id'=>'date_of_birth',
    ])
    @endcomponent

    @component('components.InputInline', [
'value' => $client['place_of_birth'],
'title' => 'Место рождения',
'type'=>'textarea',
'id'=>'place_of_birth',
])
    @endcomponent
</div>


    <div class ="block-content">
        <h3>
            Контактная информация </br>
        </h3>
    @component('components.InputInline', [
    'value' => $client['phone'],
    'title' => 'Телефон',
    'id'=>'phone',
    ])
    @endcomponent

    @component('components.InputInline', [
    'value' => $client['email'],
    'title' => 'email',
    'id'=>'email',
    ])
    @endcomponent
</div>
