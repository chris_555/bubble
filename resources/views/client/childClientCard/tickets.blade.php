<table id ="tickets">
    <thead>
    <tr>
        <th> Заголовок</th>
        <th> Тип заявки</th>
        <th> Этап</th>
        <th> Риэлтор</th>
        <th> Цена</th>
        <th> Код объекта</th>
        <th> Последний комментарий</th>
    </thead>
    @foreach ($tickets as $ticket )
        <tr style ="border-left: 2px solid  {{ $ticket["stage"]["color"]}}">
            <td><a href={{route("ticketCard", ["ticketId" => $ticket["id"]])}}>

                    <span style="color:gray">Создана {{ $ticket["created_at"] }} </span>
                    <br># {{$ticket["id"]}}
                    <br>{{ $ticket["params"]["title"] }} </a>
            </td>
            <td>{{ $ticket["params"]["type"]["title"]}}</td>
            <td>{{ $ticket["stage"]["title"]}}</td>
            <td>{{ $ticket["user"]["fullname"]}}
                <br>{{$ticket["user"]["phone"] !='' ?'тел.'.$ticket["user"]["phone"]: ''}} </td>
            <td>{{ $ticket["params"]["price"]}} руб.</td>

            <td><a href={{route("ticketCard", ["ticketId" => $ticket["id"]])}}> # {{ $ticket["id"] ?? ''}} </a> </td>
            <td>
                @if(!is_null($ticket['lastComment']))
                    <div class ="comments" style="width:100px;" >
                        <div class ="comment">
                            <div class="comment-header">
                                <div class="comment-user"> {{  $ticket['lastComment']['user']['fullname']  }} </div>  </div>
                        </div>
                        <div class="comment-content">
                            {{  $ticket['lastComment']['text']  }}
                        </div>
                    </div >

                    </div>
                @endif
            </td>

        </tr>
    @endforeach
</table>
