<div class ="block-content">
    <h3>
       Паспортные данные</br>
    </h3>
    @foreach($fields[1] as $field)
    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $field['value'],
    'title' => $field['title'],
    'id'=>$field['name'],
    ])
    @endcomponent
    @endforeach
    <h3>
        СНИЛС</br>
    </h3>
    @foreach($fields[2] as $field)
        @component('components.InputInline', [
        'readonly' => 'readonly',
        'value' => $field['value'],
        'title' => $field['title'],
        'id'=>$field['name'],
        ])
        @endcomponent
    @endforeach

</div>


