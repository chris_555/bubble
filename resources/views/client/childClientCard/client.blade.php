<div class ="block-content">
    <h3>
        Общая информация</br>
    </h3>
    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['first_name'],
    'title' => 'Имя',
    'id'=>'first_name',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['second_name'],
    'title' => 'Фамилия',
    'id'=>'second_name',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['last_name'],
    'title' => 'Отчество',
    'id'=>'last_name',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
         'value' => $client['gender'] ==1 ?'Мужской':"Женский",
         'title' => 'Пол',
         'id'=>'gender',
         ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['date_of_birth'],
    'title' => 'Дата рождения',
    'id'=>'date_of_birth',
    ])
    @endcomponent

    @component('components.InputInline', [
'readonly' => 'readonly',
'type'=>'textarea',
'value' => $client['place_of_birth'],
'title' => 'Место рождения',
'id'=>'place_of_birth',
])
    @endcomponent

</div>

    <div class ="block-content">
        <h3>
            Контактная информация </br>
        </h3>
    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['phone'],
    'title' => 'Телефон',
    'id'=>'phone',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $client['email'],
    'title' => 'email',
    'id'=>'email',
    ])
    @endcomponent
</div>
