
@extends('layouts.app')

@section('overlay-header')
<div class="returntoback">
    <a href="{{ route('clientsList') }}"> <i class="fas fa-angle-left dark-icon"></i>  К списку клиентов</a>
   <b>/  {{ $client->GetFio() }}  </b>
</div>
@endsection

@section('content')
    <div class="tabs">
        <span  id="1"> <a href={{route("clientCard", ["clientId" => $clientId, "tab" => 1])}}> О клиенте </a> </span>
        <span  id="3"> <a href={{route("clientCard", ["clientId" => $clientId, "tab" => 3])}}>Документы</a></span>
        <span  id="2"> <a href={{route("clientCard", ["clientId" => $clientId, "tab" => 2])}}>Заявки</a></span>
    </div>

    <div class="pageSeparator"></div>
    <div class="content_header">
    <div  class="separatorTitle nameTab">О клиенте</div>
        @if(!$edit &&( $tab==1 || $tab==3)) <button id="button_edit" onclick="window.location='{{route("clientCard", [
    "tab" => $tab,
    "clientId" => $clientId,
    "edit" =>'edit'
    ])}}'" class="seporator_item  light">
            Редактировать</button>
        @elseif($edit &&( $tab==1 || $tab==3))
            <button onclick="edit_client()"
                    class="seporator_item  light">
                Сохранить</button>
        @endif
    </div>
    <div class="pageSeparator"></div>
        <div class="objectContent" >
            {!! $child ?? '' !!}
        </div>
    <script>
        function edit_client(){
            var client = {};
            $('input[type=radio]').each(function(){
                if( $(this).prop("checked")) client[$(this).attr('name')] = $(this).val()=="male"?1:2;
            });

            $('input[type=text]').each(function() {
                $value = $(this).val();
                if($(this).attr('id')=='phone') $value = $value.replace(/\D+/g,"") ;
                client[$(this).attr('id')] = $value;

            });
            $('textarea').each(function() {
                $value = $(this).val();
                client[$(this).attr('id')] = $value;

            });
            $('input[type=date]').each(function() {
                client[$(this).attr('id')] = $(this).val();
            });
            $('input[type=number]').each(function() {
                client[$(this).attr('id')] = $(this).val();
            });
            console.log(client);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: '/client/edit',
                data: {clientId: {{$clientId ?? null}}, client: client},
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    location ="/client/"+{{ $clientId ?? 'null' }}+'/'+{{ $tab ?? '1' }};
                },
                error: function(response){
                    console.log(response);
                }
            });
        };

    </script>

@endsection

