@extends('layouts.app')
@section('overlay-header')
    <div class="search">
        <div class="search_box">
            <input id="search_input" type="text" value ="{{ Cache::get('clients_search') ??''}}" placeholder="ФИО или номер телефона">
            <button id="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>

    </div>

@endsection
@section('content')
    <div class="content_header">
        <div class="separatorTitle">
      КЛИЕНТЫ
        </div>

        <div class="pagination">
           <span>{{($offset)+1}} - {{($offset+$count)}} </span>  из <allcount>{{$allcount }} </allcount>
            <div class="item_pagination" id="back"> < </div>
            <div class="item_pagination" id="next"> > </div>
        </div>

    </div>
    <div class="pageSeparator"></div>


    <table id="clients">
        <thead>
        <tr>
            <th>ФИО</th>
            <th>Телефон</th>
            <th>Почта</th>
            <th>Дата создания</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
        <tr>
            <td> <a href={{route("clientCard", ["clientId" =>  $client['id']])}}> {{ $client['fio'] }}</a></td>
            <td>{{ $client['phone'] }}</td>
            <td>{{ $client['email'] }}</td>
            <td>{{ $client['created_at'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        function tableDraw(response){
            console.log(response.count);
            $('.pagination > span').text((response.new_offset +1)+" - "+(response.new_offset + response.data.length));
            if(response.new_offset ==0)$('#back').addClass('disabled'); else $('#back').removeClass('disabled');
            if((response.new_offset+15) >= response.count)$('#next').addClass('disabled'); else $('#next').removeClass('disabled');

            $("#clients > tbody").html("");
            response.data.forEach(element => {
                var tr =`
                              <tr>
                              <td> <a href ="\client\\` + element['id'] +`">`+element['fio']+`</a></td>
                              <td>`+element['phone']+`</td>
                              <td>`+element['email']+`</td=>
                              <td>`+element['created_at']+`</td>
                              </tr>
                            `;
                $('#clients > tbody').append(tr);

            });
        };

        $( document ).ready(function() {
            $('#search_button').on('click', function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/clients/search',
                    data: {text: $('#search_input').val()},
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        tableDraw(response);
                        $('allcount').text(response.count);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });


            if({{Cache::get('clients_page',0) }} == "0") $('#back').addClass('disabled');
            if({{Cache::get('clients_page',0) +15 }} >= {{$allcount}}) $('#next').addClass('disabled');

            $('.item_pagination').on('click', function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/clients/get',
                    data: {page: $(this).attr('id')},
                    type: 'POST',
                    success: function(response) {
                        tableDraw(response);
                        console.log(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });
        })
    </script>
@endsection

