
@extends('layouts.app')

@section('overlay-header')
    <div class="search">
        <div class="search_box">
            <input id="search_input" type="text" value ="{{ Cache::get('tickets_search') ??''}}" placeholder="Код заявки, клиент или заголовок">
            <button id="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
    </div>
@endsection

@section('content')

    <div class="content_header">
        <div class="separatorTitle">
            @if((Auth::user()->position_id == 2)) ВСЕ ЗАЯВКИ
                @else Мои заявки
            @endif
        </div>
        <button onclick="window.location='{{route("ticketsList", ["tab" => $tab, "drawer"=>"createticket"])}}'" class="seporator_item  dark">
           <b style="margin-right: 10px">+</b> Добавить заявку</button>
    </div>

    <div class="pageSeparator"></div>
    <div class="tabs">

        <span  id="1"> <a href={{route("ticketsList", ["tab" => 1,])}}>В работе(<count>{{$countTickets['1']}}</count>) </a></span>
        <span  id="2"> <a href={{route("ticketsList", ["tab" => 2])}}>Отмененные (<count>{{$countTickets['2']}}</count>)  </a></span>
        <span  id="3"> <a href={{route("ticketsList", ["tab" => 3])}}>Завершенные c успехом (<count>{{$countTickets['3']}}</count>)  </a></span>
        <span  id="4"> <a href={{route("ticketsList", ["tab" => 4])}}>Удаленные (<count>{{$countTickets['4']}}</count>)  </a></span>
        <span  id="5"> <a href={{route("ticketsList", ["tab" => 5])}}>Избранное (<count>{{$countTickets['5']}}</count>)  </a></span>
        <div class="pagination">
                <span>{{Cache::get('tickets_page',0)+1 }} - {{Cache::get('tickets_page',0)+count($tickets) }} </span>  из <allcount> {{$allcount }} </allcount>
                <div class="item_pagination" id="back"> < </div>
                <div class="item_pagination" id="next"> > </div>
            </div>
    </div>
    <table id ="tickets">
        <thead>
        <tr>
            <th> Заголовок</th>
            <th> Клиент</th>
            <th> Тип заявки</th>
            <th> Этап</th>
            <th> Риэлтор</th>
            <th> Цена</th>
            <th> Код объекта</th>
            <th> Последний комментарий</th>
        </thead>
            @foreach ($tickets as $ticket )
            <tr style ="border-left: 2px solid  {{ $ticket["stage"]["color"]}}">
            <td><a href={{route("ticketCard", ["ticketId" => $ticket["id"]])}}>
                    <span style="color:gray">Создана {{ $ticket["created_at"] }} </span>
                    <br># {{$ticket["id"]}}
                    <br>{{ $ticket["params"]["title"] }} </a>
            </td>
            <td><a href={{route("clientCard", ["clientId" => $ticket["client"]["id"]])}}>   {{ $ticket["client"]["fullname"]}}</a>
                <br> {{$ticket["client"]["phone"] !='' ?'тел.': ''}}
               {{$ticket["client"]["phone"]}} </td>
            <td>{{ $ticket["params"]["type"]["title"]}}</td>
            <td>{{ $ticket["stage"]["title"]}}</td>
            <td>{{ $ticket["user"]["fullname"]}}
                <br>{{$ticket["user"]["phone"] !='' ?'тел.'.$ticket["user"]["phone"]: ''}} </td>
            <td>{{ $ticket["params"]["price"]}}</td>
                <td>
                    @if($ticket["object_id"] ?? false)
                        <a href={{route("objectCard", ["ticketId" => $ticket["id"],"objectId"=>$ticket["object_id"]])}} >
                            # {{ $ticket["object_id"] ?? ''}}  </a>
                @endif
                </td>
                <td>
                @if(!is_null($ticket['lastComment']))
                    <div class ="comments" >
                        <div class ="comment" style="width: 50px">
                            <div class="comment-header">
                                <div class="comment-user"> {{  $ticket['lastComment']['user']['initials']  }} </div>  </div>
                        </div>
                        <div class="comment-content" style="width: none;">
                            <div class="crop"> {{  $ticket['lastComment']['text']  }}</div>
                        </div>
                    </div >

                    </div>
                @endif
            </td>

        </tr>
        @endforeach
    </table>
    Продажа <br>
    @foreach($legenda as $item)
        @if($item["item"]["type"] ==2)
            <div style ="display: inline-flex">
            <div style ="width: 220px">
                <div class="point" style='background:{{ $item["item"]["color"]}}' > </div>
               {{ $item["item"]["title"]}}
            </div>
            </div>
        @endif
    @endforeach

    <br> <br> Покупка <br>
    @foreach($legenda as $item)
        @if($item["item"]["type"] ==1)
            <div style ="display: inline-flex">
            <div  style ="width: 220px">
                <div class="point" style='background:{{ $item["item"]["color"]}}' > </div>
                {{ $item["item"]["title"]}}
            </div>
            </div>
        @endif
    @endforeach

    <script>
        function tableDraw(response){
            $('.pagination > span').text((response.new_offset +1)+" - "+(response.new_offset + response.data.length));
            if(response.new_offset ==0)$('#back').addClass('disabled'); else $('#back').removeClass('disabled');
            if((response.new_offset+15) >= {{$allcount}})$('#next').addClass('disabled'); else $('#next').removeClass('disabled');

            $("#tickets > tbody").html("");

            response.data.forEach(element => {
                if(element['lastComment'] != null) {
                    var comment =
                        `<div class ="comments" style="width:100px;"> <div class ="comment"><div class="comment-header">
                                   <div class="comment-user"> ` + element['lastComment']['user']['fullname'] + `</div></div></div>
                                   <div class="comment-content"> ` + element['lastComment']['text'] + ` </div> </div > </div>`;
                } else comment='';

                element["client"]["phone"] !=null? element["client"]["phone"] = 'тел. '+element["client"]["phone"]:element["client"]["phone"] = ''
                element["user"]["phone"] !=null? element["user"]["phone"] = 'тел. '+element["user"]["phone"]:element["user"]["phone"] = ''


                var tr =`
                         <tr style ="border-left: 2px solid`+ element["stage"]["color"]+`">
                          <td><a href="/ticket/`+element['id']+`">
                           <span style="color:gray">Создана`+ element["created_at"] +` </span>
                            <br>#`+element["id"]+`<br>`+ element["params"]["title"]+` </a> </td>
                            <td> `+element["client"]["fullname"] +`<br>`+  element["client"]["phone"] +` </td>
                            <td> `+element["params"]["type"]["title"]+`</td>
                             <td> `+element["stage"]["title"]+`</td>
                             <td> `+element["user"]["fullname"] +`<br>`+  element["user"]["phone"]  +`</td>
                             <td> `+element["params"]["price"] +`</td>
                             <td><a href="/object/`+element['object_id']+`/ticket/`+element["id"]+`"># `+element["object_id"]+`</a></td>
                             <td>`+comment+`</td></tr>`;
                $('#tickets > tbody').append(tr);
            });
        }
        $( document ).ready(function() {
            $('#search_button').on('click', function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/tickets/search',
                    data: {text: $('#search_input').val()},
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        tableDraw(response);
                        for (var k in response.count){
                            $('#'+k+' count').text( response.count[k]);
                        }
                        $('allcount').text(response.count[{{$tab ?? 1}}]);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });

            if({{Cache::get('tickets_page',0) }} == "0") $('#back').addClass('disabled');
            if({{Cache::get('tickets_page',0) +15 }} >= {{$allcount}}) $('#next').addClass('disabled');

            $('.item_pagination').on('click', function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/tickets/get',
                    data: {page: $(this).attr('id')},
                    type: 'POST',
                    success: function(response) {
                       console.log(response);
                       tableDraw(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });
        })
    </script>
@endsection

