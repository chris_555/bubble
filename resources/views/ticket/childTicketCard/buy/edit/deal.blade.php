
<div class ="block-content">

    <div>
        <h3>
            Информация о сделке </br>
        </h3>


        @component('components.InputInline', [
        'placeholder'=>"Код объекта",
        'type'=>'number',
        'req'=>"true",
        'style_title' =>"width:400px",
        'title'=>'Код объекта',
        'id'=>'object_id'])
        @endcomponent

        @component('components.InputInline', [
        'placeholder'=>"Сумма по договору",
        'req'=>"true",
        'style_title' =>"width:400px",
        'title'=>'Сумма по договору',
        'id'=>'price'])
        @endcomponent

        @component('components.InputInline', [
                'placeholder'=>"Сумма задатка",
                 'req'=>"true",
                'maska'=>'price',
                'style_title' =>"width:400px",
                'title'=>'Задаток',
                'id'=>'deposit'])
        @endcomponent

        @component('components.InputInline', [
          'placeholder'=>"Дата заключения",
          'type'=>'date',
          'req'=>"true",
          'style_title' =>"width:400px",
          'title'=>'Дата заключения',
          'id'=>'date'])
        @endcomponent

        @component('components.InputInline', [
           'placeholder'=>"Кол-во дней",
           'type'=>'number',
           'req'=>"true",
           'style_title' =>"width:400px",
           'title'=>'Срок расчета сторон',
           'id'=>'day_payment'])
        @endcomponent

    </div>
</div>

