
<div class ="block-content">
   <h3>
       {{ $ticket['params']['title']}} #{{ $ticket['id']}}</br>
   </h3>
    <span class="b2">
         {{ $ticket['created_at']}}
    </span>
    @component('components.fieldShow')
        @slot('title')
            Клиент
        @endslot
        @slot('value')
            {{ $ticket['client']['fullname']}}
            <br> тел.: {{ $ticket['client']['phone']}}
        @endslot
    @endcomponent

    <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Риэлтор
        @endslot
        @slot('value')
            {{ $ticket['user']['fullname']}}
            <br> тел.: {{ $ticket['user']['phone']}}
        @endslot
    @endcomponent
        <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Операция заявки
        @endslot
        @slot('value')
            {{ $ticket['params']['type']['title']}}
        @endslot
    @endcomponent
        <div class="pageSeparator"></div>
        @component('components.fieldShow')
            @slot('title')
                Валовая выручка
            @if(!$dealId)
                <span  class="info" style="margin-bottom: 0" >*</span>
            @endif
            @endslot
            @slot('value')
                    {{ $ticket['revenue']}}
            @endslot
        @endcomponent
    @if(!$dealId)
       <span  class="info" > <span  class="info" >*</span> Предварительное значение на основе потребности, итоговая выручка будет рассчитана после заключения сделки</span>
        </div>
@endif


<div class ="block-content">
    <h3>
       Редактировать потребность  </br>
    </h3>

    @component('components.input_between', [
       'value_min' => $needFields['price-min']['value'],
       'value_max' => $needFields['price-max']['value'],
       'title' => $needFields['price-min']['description'],
       'maska'=>'price',
       'id_min'=>$needFields['price-min']['title'],
       'id_max'=>$needFields['price-max']['title'],
       ])
    @endcomponent


    @component('components.dadataTextarea',[
     'value'=> $needFields['address']['value'] ])
    @endcomponent

    @component('components.input_between', [
    'value_min' => $needFields['year_construct-min']['value'],
    'value_max' => $needFields['year_construct-max']['value'],
    'title' => $needFields['year_construct-min']['description'],
    'id_min'=>$needFields['year_construct-min']['title'],
    'id_max'=>$needFields['year_construct-max']['title'],
    ])
    @endcomponent


    @component('components.input_between', [
    'value_min' => $needFields['area-min']['value'],
    'value_max' => $needFields['area-max']['value'],
    'title' => $needFields['area-max']['description'],
    'id_min'=>$needFields['area-min']['title'],
    'id_max'=>$needFields['area-max']['title'],
    ])
    @endcomponent

    @component('components.input_between', [
    'value_min' => $needFields['floor-min']['value'],
    'value_max' => $needFields['floor-max']['value'],
    'title' => $needFields['floor-max']['description'],
    'id_min'=>$needFields['floor-min']['title'],
    'id_max'=>$needFields['floor-max']['title'],
    ])
    @endcomponent



    @component('components.select', [
    'text'=>"Любой",
    'title' => $needFields['objectKeep']['description'],
    'values'=>$needFields['objectKeep']['values'],
    'selected_value'=>$needFields['objectKeep']['value'],
    'id'=>$needFields['objectKeep']['title']])
    @endcomponent

    @component('components.select', [
     'title' => $needFields['objectClass']['description'],
     'values'=>$needFields['objectClass']['values'],
     'selected_value'=>$needFields['objectClass']['value'],
     'id'=>$needFields['objectClass']['title']])
    @endcomponent


    <div class="input_inline_block">
        <div class="input-title_inline">Количество комнат</div>
        <input @if(in_array("1", $needFields['num_rooms']['value'] ?? [] )) checked @endif  type=checkbox value= "1" id='1_room'  name=num_rooms> <label style="margin-right: 20px" for = '1_room' > 1к</label>
        <input  @if(in_array("2", $needFields['num_rooms']['value'] ?? [] )) checked @endif  type=checkbox value= "2" id='2_room' name=num_rooms > <label style="margin-right: 20px" for = '2_room' > 2к</label>
        <input @if(in_array("3", $needFields['num_rooms']['value'] ?? [] )) checked @endif   type=checkbox value= "3" id='3_room' name=num_rooms> <label style="margin-right: 20px" for = '3_room' > 3к</label>
        <input  @if(in_array(">4", $needFields['num_rooms']['value'] ?? [])) checked @endif  type=checkbox value= ">4" id='4_room' name=num_rooms> <label style="margin-right: 20px" for = '4_room' > 4к+</label>
    </div>




</div>
<script>
$(function() {

$('textarea').each(function () {
this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden; min-height:20px');
}).on('input', function () {
this.style.height = 'auto';
this.style.height = (this.scrollHeight-10) + 'px';
});

$('.select').each(function() {

    var $this = $(this),
        selectOption = $this.find('option'),
        selectOptionLength = selectOption.length,
        selectedOption = selectOption.filter(':selected'),
        dur = 100;

    $value = "";
    $this.hide();
    $this.wrap('<div class="select"></div>');
    $('<div>', {
        class: 'select__gap',
        text: 'Выберите из списка',
        id: $this.attr('id')
    }).insertAfter($this);

    var selectGap = $this.next('.select__gap'),
        caret = selectGap.find('.caret');
    $('<ul>', {
        class: 'select__list'
    }).insertAfter(selectGap);

    var selectList = selectGap.next('.select__list');

    for (var i = 0; i < selectOptionLength; i++) {
        $('<li>', {
            class: 'select__item',
            html: $('<span>', {
                text: selectOption.eq(i).text()
            })
        })
            .attr('data-value', selectOption.eq(i).val())
            .appendTo(selectList);
    }
    var selectItem = selectList.find('li');

    selectList.slideUp(0);
    selectGap.on('click', function () {
        $id = $this.attr('id');
        if (!$(this).hasClass('on')) {
            $(this).addClass('on');
            selectList.slideDown(dur);
            selectItem.on('click', function () {
                var chooseItem = $(this).data('value');
                $value = chooseItem;
                $('#' + $id).val(chooseItem).attr('selected', 'selected');
                selectGap.text($(this).find('span').text());
                selectList.slideUp(dur);
                selectGap.removeClass('on');
                action($id, $value);
            });

        } else {
            $(this).removeClass('on');
            selectList.slideUp(dur);

        }
    });

});
});
</script>

