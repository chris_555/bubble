<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class ="block_right">

    <div>
        <h3>
            Информация о сделке </br>
        </h3>

        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'value'=>$deal['id']??"",
    'style_title' =>"width:400px",
    'readonly'  =>'readonly',
    'title'=>'№ Договора'
     ])
        @endcomponent

        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'value'=>$deal['ticket_sell_id']??"",
    'style_title' =>"width:400px",
    'readonly'  =>'readonly',
    'title'=>'Код заявки на продажу'
    ])
        @endcomponent

        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'value'=>$deal['ticket_buy_id']??"",
    'style_title' =>"width:400px",
    'readonly'  =>'readonly',
    'title'=>'Код заявки на покупку'
     ])
        @endcomponent

        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'value'=>$deal['object_id']??"",
    'style_title' =>"width:400px",
    'readonly'  =>'readonly',
    'pattern'=>'pattern = " ^[ 0-9]+$ " ',
    'title'=>'Код объекта',
    'id'=>"object_id"
    ])
        @endcomponent

        @if($deal['id'] ?? false)
            <a href={{route("objectCard", ["ticketId"=> $deal['ticket_sell_id']??"",'objectId'=>$deal['object_id'] ??""])}}>Перейти к обекту</a>
        @endif

        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'readonly'  =>'readonly',
    'value'=> $deal['price'] ?? '',
    'style_title' =>"width:400px",
    'title'=>'Сумма по договору',
    'id'=>'price'])
        @endcomponent

        @component('components.InputInline', [
                        'value'=>$deal['day_payment']??"",
                        'style_title' =>"width:400px",
                        'readonly'  =>'readonly',
                        'title'=>'Срок расчета в днях'
                        ])
        @endcomponent

        @component('components.InputInline', [
                 'value'=>$deal['deposit']??"",
                 'maska'=>'price',
                 'style_title' =>"width:400px",
                 'readonly'  =>'readonly',
                 'title'=>'Задаток'
                 ])
        @endcomponent
        @component('components.InputInline', [
    'placeholder'=>"Не указано",
    'value'=>$deal['date']??"",
    'readonly'  =>'readonly',
    'style_title' =>"width:400px",
    'title'=>'Дата заключения сделки'
  ])
        @endcomponent



    </div>
    @if($deal['id'] ?? false)
        <div class="upload">
            <form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >
                <div class="form-group">
                    <input type="file" name="file" id="scan_deal_upload" class="input-file">
                    <label for="scan_deal_upload" class="btn btn-tertiary js-labelFile">
                        <i class="icon fa fa-check"></i>
                        <span class="js-fileName">Загрузить скан договора</span>
                    </label>
                </div>
            </form>
        </div>
    @else
        <span class="info">
            <span class="req">*</span>Загрузка сканов договора доступна после заполнения полей</span>

    @endif
    <span  class="info">
            <span class="req">*</span>Редактирование информации о сделке недоступно после ее заполнения
        </span>
</div>
@if(!($deal['id'] ?? false))
    <span  class="info" style="margin-top: 30px">
           Сделка еще не заключена, для оформления сделки перейдите в режим редактирования
    </span>
@endif
@component('components.media',['media'=>$scan_deal,'tab'=>$tab])
@endcomponent

<script>
    $(function($) {
        if({{$deal['id'] ?? 'false'}}) {
            $('#button_edit').attr('disabled',true);
        }
    });

</script>
