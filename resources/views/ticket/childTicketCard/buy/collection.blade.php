    @foreach($tickets as $key=> $collection)
        <div class="collection_block">
               <h2>
            Подборка #{{$key}} от {{$collection['created_at']}}
                   <button id="init" onclick=colldown({{$collection['id']}})
                           class="item button light" style="float:right;margin-right: 10px">
                       <i class="fa fa-arrow-down" aria-hidden="true"></i> Скачать
                   </button>
        </h2>
        @foreach($collection['tickets'] as $ticket)
            <div class="card">
                <a href={{route("objectCard", ["ticketId"=> $ticket['id'],'objectId'=>$ticket['object_id']])}}>
                    @component('components.main_photo', ['photo' => $ticket['object']['main_photo'],'size'=>'265px'])
                @endcomponent
                    <p class="b2"> {{$ticket['object']['type']}}  #{{$ticket['object_id']}}</p>
                     <span class="item_address">{{$ticket['object']['address']}} </span>
                    <p>{{$ticket['object']['short_par']}}</p>
                    <p class="item_price">{{$ticket['price']}}</p>
                </a>
            </div>
        @endforeach
            @if($key < count($tickets))<div class="pageSeparator"></div>@endif
        </div>
        @endforeach

<script>
function colldown($id){location ="/collection/download/"+$id;}
</script>
