
<div class ="block-content">
    <h3>
        {{ $ticket['params']['title']}} #{{ $ticket['id']}}</br>
    </h3>
    <span class="b2">
         {{ $ticket['created_at']}}
    </span>
    @component('components.fieldShow')
        @slot('title')
            Клиент
        @endslot
        @slot('value')
            {{ $ticket['client']['fullname']}}
            <br> тел.: {{ $ticket['client']['phone']}}
        @endslot
    @endcomponent

    <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Риэлтор
        @endslot
        @slot('value')
            {{ $ticket['user']['fullname']}}
            <br> тел.: {{ $ticket['user']['phone']}}
        @endslot
    @endcomponent
    <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Операция заявки
        @endslot
        @slot('value')
            {{ $ticket['params']['type']['title']}}
        @endslot
    @endcomponent
    <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Валовая выручка
            @if(!$dealId)
                <span  class="info" style="margin-bottom: 0" >*</span>
            @endif
        @endslot
        @slot('value')
            {{ $ticket['revenue']}}

        @endslot
    @endcomponent
    @if(!$dealId)
        <span  class="info" > <span  class="info" >*</span> Предварительное значение на основе потребности, итоговая выручка будет рассчитана после заключения сделки</span>
@endif
</div>
<div class ="block-content">
    <h3>
        Потребность </br>
    </h3>
    @component('components.input_between', [
    'readonly' => 'readonly',
    'maska'=>'price',
    'value_min' => $needFields['price-min']['value'],
    'value_max' => $needFields['price-max']['value'],
    'title' => $needFields['price-min']['description'],
    'id_min'=>$needFields['price-min']['title'],
    'id_max'=>$needFields['price-max']['title'],
    ])
    @endcomponent

    @component('components.dadataTextarea',[
    'readonly' => 'readonly',
    'value'=> $needFields['address']['value'] ])
    @endcomponent


    @component('components.input_between', [
    'readonly' => 'readonly',
    'value_min' => $needFields['year_construct-min']['value'],
    'value_max' => $needFields['year_construct-max']['value'],
    'title' => $needFields['year_construct-min']['description'],
    'id_min'=>$needFields['year_construct-min']['title'],
    'id_max'=>$needFields['year_construct-max']['title'],
    ])
    @endcomponent


    @component('components.input_between', [
    'value_min' => $needFields['area-min']['value'],
    'value_max' => $needFields['area-max']['value'],
    'readonly' => 'readonly',
    'title' => $needFields['area-max']['description'],
    'id_min'=>$needFields['area-min']['title'],
    'id_max'=>$needFields['area-max']['title'],
    ])
    @endcomponent

    @component('components.input_between', [
    'value_min' => $needFields['floor-min']['value'],
    'value_max' => $needFields['floor-max']['value'],
    'readonly' => 'readonly',
    'title' => $needFields['floor-max']['description'],
    'id_min'=>$needFields['floor-min']['title'],
    'id_max'=>$needFields['floor-max']['title'],
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'title' => $needFields['objectKeep']['description'],
    'placeholder'=>"Любое",
    'value' => $needFields['objectKeep']['value'],
    'id'=>$needFields['objectKeep']['title']
 ])
    @endcomponent

        @component('components.InputInline', [
 'readonly' => 'readonly',
 'title' => $needFields['objectClass']['description'],
 'placeholder'=>"Любой",
 'value' => $needFields['objectClass']['value'],
 'id'=>$needFields['objectClass']['title']
])
    @endcomponent
    <div class="input_inline_block">
        <div class="input-title_inline">Количество комнат</div>
        <input @if(in_array("1", $needFields['num_rooms']['value'] ?? [])) checked @endif  type=checkbox value= "1" id='1_room'  name=num_rooms> <label style="margin-right: 20px" for = '1_room' > 1к</label>
        <input  @if(in_array("2", $needFields['num_rooms']['value']?? [])) checked @endif  type=checkbox value= "2" id='2_room' name=num_rooms > <label style="margin-right: 20px" for = '2_room' > 2к</label>
        <input @if(in_array("3", $needFields['num_rooms']['value']?? [])) checked @endif   type=checkbox value= "3" id='3_room' name=num_rooms> <label style="margin-right: 20px" for = '3_room' > 3к</label>
        <input  @if(in_array(">4", $needFields['num_rooms']['value']?? [])) checked @endif  type=checkbox value= ">4" id='4_room' name=num_rooms> <label style="margin-right: 20px" for = '4_room' > 4к+</label>
    </div>

</div>
