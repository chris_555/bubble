<div class ="block-content">
   <h3>
       {{ $ticket['params']['title']}} #{{ $ticket['id']}}</br>
   </h3>
    <span class="b2">
         {{ $ticket['created_at']}}
    </span>
    @component('components.fieldShow')
        @slot('title')
            Клиент
        @endslot
        @slot('value')
            {{ $ticket['client']['fullname']}}
            <br> тел.: {{ $ticket['client']['phone']}}
        @endslot
    @endcomponent

    <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Риэлтор
        @endslot
        @slot('value')
            {{ $ticket['user']['fullname']}}
            <br> тел.: {{ $ticket['user']['phone']}}
        @endslot
    @endcomponent
        <div class="pageSeparator"></div>
    @component('components.fieldShow')
        @slot('title')
            Операция заявки
        @endslot
        @slot('value')
            {{ $ticket['params']['type']['title']}}
        @endslot
    @endcomponent

    @component('components.fieldShow')
        @slot('title')
            Cтоимость объекта
        @endslot
        @slot('value')
            {{ $ticket['params']['price']}}
        @endslot
    @endcomponent

        <div class="pageSeparator"></div>


    @component('components.fieldShow')
        @slot('title')
            Валовая выручка
            @if(!$dealId)
                <span  class="info" style="margin-bottom: 0" >*</span>
            @endif
        @endslot
        @slot('value')
            {{ $ticket['revenue']}}

        @endslot
    @endcomponent
    @if(!$dealId)
        <span  class="info" > <span  class="info" >*</span> Предварительное значение, итоговая выручка будет рассчитана после заключения сделки</span>
@endif
        </div>
<div class ="block-content">
    <h3>
       Информация об объекте </br>
    </h3>
<div class="item_bc_ob">

    @component('components.main_photo', ['photo' => $ticket['object']['main_photo'],'size'=>'150px'])
    @endcomponent

</div>
    <div class="item_bc_ob">
        <a href={{route("objectCard", ["ticketId"=> $ticket['id'],'objectId'=>$ticket['object_id']])}}
            <span class="item_address">{{$ticket['object']['address']}} </span></a>
        <p>{{$ticket['object']['short_par']}}</p>
        <p class="item_price">{{$ticket['params']['price']}} </p>
        <p class="b2">{{$ticket['object']['type']}}  #{{$ticket['object_id']}} </p>
    </div>
    <br>
    @component('components.fieldShow')
        @slot('title')
          Класс объекта
        @endslot
        @slot('value')
            {{ $ticket['object']['class']}}
        @endslot
    @endcomponent
    <div class="pageSeparator"></div>
</div>
