@extends('layouts.app')
@section('overlay-header')
    <link href="{{ asset('css/components/media.css') }}" rel="stylesheet" />
<div class="returntoback">
    <a href="{{ route('ticketsList') }}">     <i class="fas fa-angle-left dark-icon"></i>  К списку заявок</a>
   <b>/ Заявка #{{ $ticketId }}  </b> <div class="chips">{{$ticketSS['status']['title']}} </div>
    <div class="stage">
        <div class="point" style="background: {{$ticketSS['stage']['color']}}"> </div>
        {{$ticketSS['stage']['title']}}

    </div>
</div>
<div class="header_navbar">
    @if(!$favoriteStatus)
        <button id="addToFav" class="item button light " value="false"><i class="fas fa-heart false"></i> В избранное</button>
    @elseif($favoriteStatus)
        <button id="addToFav" class="item button light" value ="true"><i class="fas fa-heart true"></i>Удалить из избранного</button>
    @endif

        @if($ticketSS['status']['value'] == 2 or $ticketSS['status']['value']==4)
            <button id="changeStatus"  class="item button light" onclick="changeStatus(this);"  action="renew">Возобновить</button>
        @else
            <button  id="changeStatus" class="item button light"  onclick="changeStatus(this);"  action="cancel">Отменить</button>
            <button  id="changeStatus" class="item button light"  onclick="changeStatus(this);"   action="delete">Удалить</button>
        @endif


</div>
@endsection

@section('content')
    <div class="tabs">
        <span  id="1"> <a href={{route("ticketCard", ["tab" => 1,"ticketId"=>$ticketId])}}>О заявке </a></span>
       @if($ticketType == 2) <span  id="2"> <a href={{route("ticketCard", ["tab" => 2,"ticketId"=>$ticketId])}}>Подборка </a></span> @endif
        <span  id="4"> <a href={{route("ticketCard", ["tab" => 4,"ticketId"=>$ticketId])}}>Договор </a></span>
    </div>

    <div class="pageSeparator"></div>
    <div class="content_header">
        <div class="separatorTitle nameTab">О заявке</div>
       @if(!$edit && !($tab==4 && $dealId)) <button id="button_edit" onclick="window.location='{{route("ticketCard", ["tab" => $tab,"ticketId"=>$ticketId,"edit"])}}'"
                class="seporator_item  light">
            Редактировать</button>
        @elseif ($tab==4 && $dealId)
            <button  class="seporator_item light" onclick="window.location='{{route("сontractGet",['id'=>$dealId])}}'">Скачать</button>
        @else
            <button onclick="edit_ticket()"
                     class="seporator_item  light">
            Сохранить</button>
           @endif
    </div>
    <div class="pageSeparator"></div>
        <div class="objectContent" >

            {!! $child ?? '' !!}

        </div>
    <div class="pageSeparator"></div>
    <div class="content_header">
    <div class="separatorTitle"> Комментарии</div>
    </div>
    @component('components.comments', ['comments' => $comments])
        @slot('width')1200px @endslot
    @endcomponent

    @component('components.inputeComment')
        @slot('width')1200px @endslot
        @endcomponent

    </div>


@endsection

