<script>
    document.body.style.overflow = "hidden";
    setInterval(function(){
        $count = 0;
$name='';
        $('.drawer  input').each(function() {
            if(($(this).attr("required") && $(this).val() =='') )
            {
                $count++;
            }
        });

        if($('*').is('input[name=client]')){
            if(   $('input[name=client]:checked').val()== undefined || !$('input[name=client]:checked').val()) {
                $count++;
            }
        }

        $('.drawer  select').each(function(){
            $name=$(this).attr('id');
            if($(this).attr('selected')!='selected')
            {
                $count++;          $name=$(this).attr('id');
            }
        });

        if($count==0){
            $('#create_bt').prop( "disabled", false );
        }
        else   {
           console.log($count+' '+$name);
           $('#create_bt').prop( "disabled", true );
        }

    }, 1000);

    function create()
    {
        var ticket ={};
        $('input[type=text]').each(function(){
            $value = $(this).val();
            if ($(this).attr('id') =='price') $value =($value.replace(',','.')).replace(' ','');
            ticket[$(this).attr('id')] = $value;
        });
        $('textarea').each(function(){
            ticket[$(this).attr('id')] = $(this).val();
        });

        $('select').each(function(){

            ticket[$(this).attr('id')] = $(this).val();
        });

        $('input[type=radio]').each(function(){
            if( $(this).prop("checked")) ticket[$(this).attr('name')] = $(this).val();
        });

        if(!$('*').is('input[name=client]')) ticket['client']='new';
        ticket['phone']  =  ticket['phone'].replace(/\D+/g,"");
        console.log(ticket);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/tickets/createTicket',
            data: {ticket:JSON.stringify(ticket)},
            dataType: "json",
            type: 'POST',
            success: function(response) {
                console.log(response);
                if(response.ticket_id) location ="/ticket/"+response.ticket_id;
            },
            error: function(response){
                console.log(response);

            }
        });
    };


    $(document).mouseup(function (e){
        var div = $(".drawer");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            location='/tickets/{{$tab ?? '1'}}/1';
        }
    });

    $(document).mouseup(function (e){
        var div = $(".select");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.next('.select__gap').removeClass('on');
            div.next('.select__gap').next('.select__list').slideUp(100);
        }
    });

    $array='';

    $(function() {

        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden; min-height:20px');
        }).on('input', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight-10) + 'px';
        });

        $('body').on('keydown', '.select__gap', function(e) {
            $this = $(this);
            $text = $this.text();
            $id =$this.attr('id');
            if($id==="developer_id")   $url = '/developers/findDeveloper';
            else    $url = '/users/findUser';
            $('ul[id=' + $id+ ']>li:not(:first-child)').remove();

            if ($text != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: $url,
                    data: {
                        developer_name: $text
                    },
                    dataType: "json",
                    type: 'POST',
                    success: function(response) {
                        $('ul[id='+$id+']>li:not(:first-child)').remove();
                        console.log(response);
                        response.results.forEach((element) => {
                            $('#'+$id).append($('<option>', {
                                value: element['id'],
                                text: element['name_of_org']
                            }));
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item',
                                html: $('<span>', {
                                    text: element['name_of_org']
                                })
                            }).attr('data-value', element['id']));
                        });
                        if (response.results.length == 0) {
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item disabled',
                                html: $('<span>', {
                                    text: "Мы ничего не нашли :("
                                })
                            }));
                        }
                    },
                    error: function(response) {
                        console.log(response);
                        $('ul[id="1gf"]').append($('<li>', {
                            class: 'select__item disabled',
                            html: $('<span>', {
                                text: "Мы ничего не нашли :("
                            })
                        }));
                    }
                });
            }
        });
        $('.select').each(function() {
            var $this = $(this),
                selectOption = $this.find('option'),
                selectOptionLength = selectOption.length,
                selectedOption = selectOption.filter(':selected'),
                dur = 100;
            $id = $this.attr('id');
            $text = $("#" + $id + " option:selected").text();
            $text ='Не указано';
            if($this.hasClass('search') && $this.attr('old_value') ==" " ){ $text ='';}
            $value = "";
            $this.hide();
            $this.wrap('<div class="select"></div>');
            $('<div>', {
                class: 'select__gap',
                text: $text,
                id: $id,
            }).insertAfter($this);

            var selectGap = $this.next('.select__gap'),
                caret = selectGap.find('.caret');
            $('<ul>', {
                class: 'select__list',
                style: "width:352px;"
            }).insertAfter(selectGap);
            if ($this.hasClass('search')) {
                selectGap.attr("placeholder", 'Выберите из списка');
                selectGap.attr('contenteditable', true);
            }
            var selectList = selectGap.next('.select__list');
            if ($this.hasClass('search')) {
                selectList.attr('id', $id);
            }
            for (var i = 0; i < selectOptionLength; i++) {
                $('<li>', {
                    class: 'select__item',
                    html: $('<span>', {
                        text: selectOption.eq(i).text()
                    })
                })
                    .attr('data-value', selectOption.eq(i).val())
                    .appendTo(selectList);
            }
            if ($this.hasClass('search')) {
                $('<li>', {
                    class: 'select__item disabled',
                    html: $('<span>', {
                        text: "Введите текст"
                    })
                }).appendTo(selectList);
            }
            var selectItem = selectList.find('li');
            selectList.slideUp(0);
            selectGap.on('click', function() {
                $id = $this.attr('id');
                if (!$(this).hasClass('on')) {
                    $(this).addClass('on');
                    selectList.slideDown(dur);
                    $('body').on('click', '.select__list>li', function(e) {
                        var chooseItem = $(this).data('value');
                        $value = chooseItem;
                        $('#' + $id).val(chooseItem).attr('selected', 'selected');
                        if ($this.hasClass('search')) {
                            $('#' + $id).val(chooseItem).attr('value', chooseItem);
                        }
                        $('.select__gap[id='+$id+']').text($(this).find('span').text());
                        selectList.slideUp(dur);
                        selectGap.removeClass('on');
                        action( $id, $value);
                    });
                } else {
                    $(this).removeClass('on');
                    selectList.slideUp(dur);
                }
            });
        });


        function action(id,val)
        {
            if(id =='type_ticket') {
                if(val =='sell') {
                    $('#ticket_sell_fields').show();
                }
                if(val =='buy') {
                    $('#ticket_sell_fields').hide();
                }
            }
        };



        $('.content_drawer').on('click', 'input[name="client"]', function(e) {

            if($(this).val() =='new') {
                $( '.clients_list').remove();
                $( '#prompt').append("<button id='showall' class=light> Показать весь список</button>");
                $( '#prompt').append('  <p><input checked type=radio value= new id=new  name=client><label for = new>Новый клиент</label></p>');
                block_newClient();

            }
        });

        $('.content_drawer').on('click', '#showall', function(e) {
            $( '#prompt').empty();
            drawPropt($array);
            $('input[name="client"]').prop('checked', false);
        });

    });
    function block_newClient(){
        var content =`<div id="new_client">
        <div class="input_inline_block ">
            <div class="input-title_inline" style='width: 150px'>Пол<span class="req">*</span></div>
            <input checked  type=radio id=female name="gender" value='female' ><label class=bspace for = female>Женский</label>
            <input  type=radio id=male   name="gender" value='male'><label class=bspace for = male>Мужской</label>
            </div>
            <div class="input_inline_block ">
            <div  class="input-title_inline" >Фамилия <span class="req">*</span> </div>
            <input required id='second_name' type ="text"  style='width: 200px' class="input_inline" placeholder=Фамилия >
            </div>
            <div class="input_inline_block ">
            <div class="input-title_inline">Имя</div>
            <input id='first_name' type ="text"  style="width: 200px"  class="input_inline" placeholder=Имя >
            </div>
            <div class="input_inline_block ">
            <div class="input-title_inline">Отчество</div>
            <input id='last_name' type ="text"  style="width: 200px" class="input_inline"  placeholder=Отчество >
</div>
<br>
<div class="pageSeparator"></div>
</div>`;
        $( '#prompt').append(content);
    };
    function drawPropt(client) {

        $( '#prompt').append('<div class=clients_list></div>');
        $( '.clients_list').append('<h2>Клиенты с этим номером телефона');
        $( '.clients_list').append('<span class=b2> Если клиента нет в списке, вы можете создать нового</span></h2>');
        client.forEach(
            client =>
            {
                console.log(client);
                $( '.clients_list').append(' <p> <input type=radio value= '+client["id"]+' id=client_'+client["id"] +' name=client><label for =client_'+client["id"]+ '>'+client["fullname"]+'</label></p>');
            }
        );
        $( '.clients_list').append(' <div class="pageSeparator"></div>');
        $( '.clients_list').append('  <p><input type=radio value= new id=new  name=client><label for = new>Новый клиент</label></p>');
        $( '.clients_list').append(' <div class="pageSeparator"></div>');
    };

    function OnInput (event) {
        if(event.target.value.length ==18) {
            $("#prompt").remove()
            $phone= event.target.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: '/clients/findClient',
                data: {phone:$phone},
                type: 'POST',
                success: function(response) {
                    console.log(response);

                    $('<div id=prompt></div>').insertAfter('.tel_block');
                    if(response.response ==true)
                    {

                        drawPropt(response.data);
                        $array = response.data;
                    }
                    else {
                        block_newClient();
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }
    }

</script>
<div  class="drawer">
    <div   class="content_header">
        <div  class="separatorTitle">
            Новая заявка
        </div>
    </div>
    <div  class="content_drawer" >
        <div class="pageSeparator"></div>

        @component('components.InputInline', ['title' => 'Заголовок', 'placeholder'=>"Введите заголовок", 'id'=>'title'])
        @endcomponent

        <div class="input_inline_block tel_block">
            <div class="input-title_inline">Телефон<span class="req">*</span></div>
            <input required id='phone' oninput="OnInput (event)" type ="text" style='width: 150px' class="input_inline" placeholder='+7 (995) 453-77-79'>
        </div>

        <div class="input_inline_block">
            <div class="input-title_inline">КАТЕГОРИЯ ЗАЯВКИ<span class="req">*</span></div>
            <select name="select-box" id="type_ticket" class="select">
                <option value="sell" >Продажа</option>
                <option value="buy">Покупка</option>
            </select>
        </div>

        <div class="input_inline_block">
            <div class="input-title_inline">Тип недвижимости<span class="req">*</span></div>
            <select name="select-box" id="type_object" class="select">
                @foreach($objectTypes as $objectType)
                    <option value="{{$objectType['item']['value']}}" >  {{$objectType['item']['title']  }}</option>
                @endforeach

            </select>
        </div>

        @if((Auth::user()->position_id == 2))
            <div id='user' class="input_inline_block">
                <div style="width: 165px"  class="input-title_inline">Выберите риэлтора</div>
                <select name="select-box"
                        id="user_id"
                        class="select search">
                </select>
            </div>
        @endif

        <div id="ticket_sell_fields" hidden>
            <div class="input_inline_block">
                <div class="input-title_inline">Адрес<span class="req">*</span></div>
                <textarea rows="1" wrap="hard"  id="address" name="address" type ="text"  placeholder="Введите адрес объекта" class="input_inline"></textarea>
            </div>


            @component('components.InputInline', [
    'title' => 'Этаж',
    'placeholder'=>"2",
    'id'=>'floor',
    'style'=>"width:50px"])
            @endcomponent

            @component('components.InputInline', [
    'title' => 'Площадь',
    'placeholder'=>"56",
    'helptext'=>"м кв.",
    'id'=>'area',
    'style'=>'width:50px'])
            @endcomponent

            @component('components.InputInline', [
    'title' => 'Количество комнат',
    'placeholder'=>"2",
    'id'=>'num_rooms',
    'style'=>'width:50px'])
            @endcomponent


            @component('components.InputInline', [
    'title' => 'Стоимость',
    'placeholder'=>"2 000 000",
    'helptext'=>"р.",
    'id'=>'price',
    'style'=>'width:100px'])
            @endcomponent


            <div class="input_inline_block">
                <div class="input-title_inline">Описание</div>
                <textarea rows="1" wrap="hard"  type ="text"  id="description" placeholder="Введите описание" class="input_inline"></textarea>
            </div>
        </div>



    </div>
    <div  class="footer_drawer" >
        <button id='create_bt' type="submit" onclick='create()' class="seporator_item  dark" disabled>
            Создать</button>
    </div>

</div>
<div  class="disable_block"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js"></script>

<script type="text/javascript" src="{{ URL::asset('js/dadata.js') }}"></script>
<!-- load jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- provide the csrf token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
