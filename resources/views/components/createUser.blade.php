<script>
    document.body.style.overflow = "hidden";
    setInterval(function(){
        $count = 0;
        $name='';
        $('.drawer  input').each(function() {
            if(($(this).attr("required") && $(this).val() =='') )
            {
                $count++;  $name=$(this).attr('id');
            }
        });

        if($('*').is('input[name=client]')){
            if(   $('input[name=client]:checked').val()== undefined || !$('input[name=client]:checked').val()) {
                $count++;  $name=$(this).attr('id');
            }
        }

        if($count==0){
            $('#create_bt').prop( "disabled", false );
        }
        else   {
            console.log($count+' '+$name);
            $('#create_bt').prop( "disabled", true );
        }

    }, 1000);

    function create()
    {
        var ticket ={};
        $('input[type=text]').each(function(){
            $value = $(this).val();
            if($value !='' )
            {
            if ($(this).attr('id') =='price') $value =($value.replace(',','.')).replace(' ','');
            ticket[$(this).attr('id')] = $value;
            }
        });
        $('textarea').each(function(){
            ticket[$(this).attr('id')] = $(this).val();
        });
        $('input[type=password]').each(function(){
            $value = $(this).val();
            ticket[$(this).attr('id')] = $value;
        });
        $('input[type=date]').each(function(){
            $value = $(this).val();
            ticket[$(this).attr('id')] = $value;
        });
        $('input[type=radio]').each(function(){
            if( $(this).prop("checked")) ticket[$(this).attr('name')] = $(this).val();
        });

        ticket['phone']  =  ticket['phone'].replace(/\D+/g,"");
        console.log(ticket);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/users/createUser',
            data: {ticket:JSON.stringify(ticket)},
            dataType: "json",
            type: 'POST',
            success: function(response) {
                console.log(response);
                if(response.id) location ="/user/"+response.id;
            },
            error: function(response){
                console.log(response);

            }
        });
    }


</script>
<div  class="drawer">
    <div   class="content_header">
        <div  class="separatorTitle">
            Новый сотрудник
        </div>
    </div>
    <div  class="content_drawer" >
        <div class="pageSeparator"></div>

        @component('components.InputInline', ['title' => 'Фамилия', 'req'=>'required','placeholder'=>"Введите текст", 'id'=>'second_name'])
        @endcomponent

        @component('components.InputInline', ['title' => 'Имя', 'req'=>'required','placeholder'=>"Введите текст", 'id'=>'first_name'])
        @endcomponent
        @component('components.InputInline', ['title' => 'Отчество', 'req'=>'required','placeholder'=>"Введите текст", 'id'=>'last_name'])
        @endcomponent
        @component('components.InputInline', ['title' => 'Дата рождения','req'=>'required', 'type'=>'date', 'id'=>'date_of_birth'])
        @endcomponent
        <div class="input_inline_block ">
            <div class="input-title_inline" style='width: 150px'>Пол<span class="req">*</span></div>

            <input
            type=radio
                id=female
                name="gender"
                value='female' >
            <label class=bspace
                   style ='margin-left: 12px'
                   for = female>Женский</label>

            <input
            type=radio
                id=male
                name="gender"
                value='male'>
            <label class=bspace
                   for = male>Мужской</label>

        </div>

        <div class="input_inline_block tel_block">
            <div class="input-title_inline">Телефон<span class="req">*</span></div>
            <input required id='phone'  type ="text" style='width: 150px' class="input_inline" placeholder='+7 (995) 453-77-79'>
        </div>
        @component('components.InputInline', ['title' => 'Логин', 'req'=>'true','placeholder'=>"Введите текст", 'id'=>'login'])
        @endcomponent
        @component('components.InputInline', ['title' => 'E-mail', 'placeholder'=>"Введите текст", 'id'=>'email'])
        @endcomponent
        @component('components.InputInline', ['title' => 'Пароль(не менее 8)', 'req'=>'true', 'type'=>'password', 'id'=>'password'])
        @endcomponent

    </div>
    <div  class="footer_drawer" >
        <button id='create_bt' type="submit" onclick='create()' class="seporator_item  dark" disabled>
            Создать</button>
    </div>

</div>
<div  class="disable_block"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js"></script>

<script type="text/javascript" src="{{ URL::asset('js/dadata.js') }}"></script>
<!-- load jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- provide the csrf token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
