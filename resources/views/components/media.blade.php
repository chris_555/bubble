<script>

    function delete_image(id)
    {
        $tab = {{$tab ?? ""}};

        if($tab === 3)   $url='/media/deleteImage';
        if($tab === 4)   $url='/scan_deals/deleteScan';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: $url,
            data: {id:id},
            type: 'POST',
            success: function(response) {
                console.log(response);
                $('#media_'+id).remove();
            },
            error: function(response){
                console.log(response);
            }
        });
    }

    function make_main_image(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/media/makeMainImage',
            data: {id:id},
            type: 'POST',
            success: function(response) {
                console.log(response);
                $('#media_'+id ).find('#make_main').remove();
                $d_id = $('.media_content').find('.label_media').attr('label_id');
                $('.media_content').find('.label_media').remove();
                var div =`<div label_id =`+id+` class="label_media chips">Главная</div>`;
                $('#media_'+id).prepend(div);
                $('#media_' + $d_id).append("<button  id='make_main' onclick='make_main_image(" + $d_id + ")' class='button light'>Сделать главной </button >");

            },
            error: function(response){
                console.log(response);
            }
        });
    }
</script>

<link href="{{ asset('css/components/media.css') }}" rel="stylesheet" />
<div class ="media" >
    <div class ="media_content" >
        @if(count($media)==0)
            <div class="name_chart" style="margin: 150px auto;font-size: 20px "> Нет загруженных фотографий </div>
        @else
        @foreach($media  as $photo)
            @if($photo['path'] && $photo['name'] )
                <div id='media_{{$photo["id"]}}' class ="image">
                    @if($photo['is_main'])
                        <div label_id ='{{$photo["id"]}}' class="label_media chips">Главная</div> @endif
                    <img src="{{URL::asset($photo['path'].$photo['name'])}}" alt="profile Pic" >
                        <button onclick='delete_image({{$photo["id"]}})' class="button light req">   Удалить </button >
                        @if($make_main?? false)
                            @if(!$photo['is_main'])
                            <button  id='make_main' onclick='make_main_image({{$photo["id"]}})' class="button light ">
                            Сделать главной </button >
                        @endif
                        @endif
                </div >

            @endif
        @endforeach
        @endif
    </div>

</div>
