
<div class ="image" style ="width:{{$size }} !important; height:{{$size}} !important; margin: 0 20px 10px 0">
    @if($photo)
        <img src="{{URL::asset($photo['path'].$photo['name'])}}" alt="profile Pic" >
    @else
        <img src="{{URL::asset('/images/not_found.jpg')}}" alt="profile Pic" >
    @endif
</div >


