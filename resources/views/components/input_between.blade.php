<div class="input_inline_block">
    <div {{$styleTitle ?? ''}} class="input-title_inline">{{$title ?? ''}}</div>
    <input  {{$readonly ?? '' }}  id="{{ $id_min ?? '' }}"
            type ={{$type ?? 'text' }}
            style='width: {{$width ?? "100px"}};margin-right: {{$margin_right ?? "20px"}}'
            class="input_inline {{$maska ?? ""}}"
            value='{{$value_min ?? ""}}'
            placeholder='Мин'> -

    <input {{$readonly ?? '' }}  id="{{ $id_max ?? '' }}"
           type ={{$type ?? 'text' }}
           style='width: {{$width ?? "100px"}};margin-left: {{$margin_left ?? "20px"}}'
           value='{{$value_max ?? ""}}'
           class="input_inline {{$maska ?? ""}}"
           placeholder='Макс'>
</div>
