<div class="input_inline_block">
    <div class="input-title_inline"> {{$title  }} </div>
    <select name="select-box" id={{$id}} class="select">

        @foreach($values as $value)
                <option @if($value['item']['value'] == $selected_value) selected @endif value="{{$value['item']['value']}}" >  {{$value['item']['title']  }}</option>
        @endforeach

    </select>
</div>
