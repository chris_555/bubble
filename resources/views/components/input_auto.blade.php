 <div class="field-show">
    <div class="field-title">{{ $data['title'] }}</div>

    @if(($data['type_field'] ?? "text" )== "select")

             <select name="select-box"
                     table="{{ $data['table'] ?? 'null' }}"
                     id="{{ $data['catalog_name'] ?? $data['title_table'] }}"
                     old_value = "{{ $data['id'] ?? " " }}"
                     is_catalog ="{{$data['is_catalog'] ?? false}}"
                     class="select {{$data['search_class'] ?? ''}}">
               @if(!($data['search_class'] ?? false))
                 @foreach($data['values'] as $value)
                     <option @if($value['item']['value'] == $data['value_id']) selected @endif value="{{$value['item']['value']}}" >  {{$value['item']['title']  }}</option>
                 @endforeach
                   @else
                      <option selected  value="{{$data['id']}}" >  {{$data['value_title']  }}</option>
                 @endif
             </select>

     @elseif(($data['title_table'] ?? "null") == "address" )
         @component('components.dadataTextarea_clear',['value'=> $data['value']  ])
         @endcomponent

     @elseif(($data['type_field'] ?? "text" )== "textarea" )
         <textarea
             table="{{ $data['table'] ?? 'null' }}"
             id="{{ $data['title_table'] }}"
             class="field-value input {{$data['maska'] ?? '' }}">{{ $data['value'] ?? 'Не указано'}}</textarea>

     @else
         <input
             table="{{ $data['table'] ?? 'null' }}"
             id="{{ $data['title_table'] }}"
             type="{{ $data['type_field'] ?? 'text' }}"
             class="field-value input {{ $data['maska'] ?? '' }}"
             placeholder="Не указано"
             value="{{ $data['value'] ?? null}}">
         </input>
     @endif

</div>

