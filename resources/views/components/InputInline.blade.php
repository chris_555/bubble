<div  {{$style_block ??''}}  class="input_inline_block">
    <div class="input-title_inline"
        {{($style_title??'') !=''? 'style ='.$style_title :''}}
    >{{ $title }} @if($req ?? false ) <span class="req">*</span> @endif
    </div>
      @if(($type ?? 'text') !='textarea')  <input
    @if($req ?? false ) required @endif
        {{$readonly ?? '' }}
        {{$attr ?? ''}}
        value = '{{$value ?? ''}}'
      {{($style??'') !=''? 'style = '.$style.'' :''}}
      id = "{{ $id ?? 'field'}}"
        type ="{{ $type ?? 'text' }}"
        class="input_inline {{ $maska?? '' }}"
        placeholder='{{ $placeholder ?? "Не указано" }}'
        >{{$helptext ?? ''}}
    @elseif(($type ?? 'text') =='textarea')
        <textarea {{$readonly ?? '' }}
            id="{{ $id ?? 'field'}}"
            class="field-value input" placeholder='Не указано'>{{ $value ?? ''}}</textarea>
          @endif
</div>

