<div class="field-show">
    <div class="field-title">{{ $title }}</div>
    @if($value !='')
        <div class="field-value">{{ $value ?? 'Не указано'}}</div>
    @else <div class="field-value"> Не указано </div>
    @endif
</div>

