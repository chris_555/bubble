
<div class ="comments" style="width: {{$width ?? '250px' }};" >

        @foreach($comments as $comment)

        <div class ="comment">
            <div class="comment-header">
        <div class="comment-user"> {{ $comment['user']['fullname']  }} </div> <div class="comment-date"> {{ $comment['created_at']  }}  </div>
    </div>
        <div class="comment-content">
            {{ $comment['text']  }}
        </div>
            </div >

        @endforeach

</div>
