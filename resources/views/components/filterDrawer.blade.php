<script>
    document.body.style.overflow = "hidden";
    $('body').on('click', '#showall', function(e) {
       if($('#additional').is(":hidden"))
       {
           $( '#additional').show();
           $(this).text("Cкрыть")
       }
       else {
           $( '#additional').hide();
        $(this).text("Дополнительно")}

    });
    function reset()
    {
        $('input[type=text]').each(function(){
            $(this).val("");
        });
        $('input[type=number]').each(function(){
            $(this).val("");
        });

        $('textarea').each(function(){
           $(this).val("");
        });

        $(' select').each(function(){
            if ($(this).hasClass('search')) {
                $('[id='+$(this).attr('id')+'][class = "select__gap"]').text("")
            } else $('[id='+$(this).attr('id')+'][class = "select__gap"]').text("Выберите значения");
            $(this).removeAttr( "selected" )

        });
            $('input:checked').prop('checked', false);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/objects/filter_reset',
            dataType: "json",
            type: 'POST',
            success: function(response) {
                console.log(response);
                location ="/objects/"+ {{$tab}};
            },
            error: function(response){
                console.log(response);
            }
        });

    }

    function find()
    {
        var array ={};
        $('#search_input').val('');
        $(' input[type=text]').each(function(){
            $value = $(this).val();
            if($value !=null && $value !='') {
                if ($(this).hasClass('price')) $value = ($value.replace(',', '.')).replace(' ', '').replace(' ', '');
                array[$(this).attr('id')] = $value;
            }
        });
        $(' input[type=number]').each(function(){
            $value = $(this).val();
            if($value !=null && $value !='' ) {
                array[$(this).attr('id')] = $value;
            }
        });

        array['catalog'] = [];
        $(' select').each(function(){
            var attr = $(this).attr('selected');
            if(typeof attr !== typeof undefined && attr !== false) {
               if($(this).hasClass('catalog'))
                array['catalog'].push($(this).val());
                else if($(this).attr('id') == "objectTypes") array['objects.type'] = $(this).val();
                else   array[$(this).attr('id')] = $(this).val();

            }
        });

        $(' input[type=checkbox]').each(function(){
            $value = $(this).val();
            if($value !=null) {
                if( $(this).prop("checked"))
                    array[$(this).attr('name')] = [];
            }
        });

        $(' input[type=checkbox]').each(function(){
            $value = $(this).val();
            if($value !=null) {
                if( $(this).prop("checked"))
                    array[$(this).attr('name')].push($(this).val());
            }
        });

        console.log(array);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/objects/filter_search',
            data: {ticket:JSON.stringify(array)},
            dataType: "json",
            type: 'POST',
            success: function(response) {
                console.log(response);
                location ="/objects/"+ {{$tab}};
            },
            error: function(response){
                console.log(response);

            }
        });
    };


    $(document).mouseup(function (e){
        var div = $(".select");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.next('.select__gap').removeClass('on');
            div.next('.select__gap').next('.select__list').slideUp(100);
        }
    });

    $array='';

    $(function() {
        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden; min-height:20px');
        }).on('input', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight-10) + 'px';
        });

        $('body').on('keyup', '.select__gap', function(e) {
            $this = $(this);
            $text = $this.text();
            $id =$this.attr('id');
            if($id==="developer_id")   $url = '/developers/findDeveloper';
            else    $url = '/users/findUser';
            $('ul[id=' + $id+ ']>li:not(:first-child)').remove();

            if ($text != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: $url,
                    data: {
                        developer_name: $text
                    },
                    dataType: "json",
                    type: 'POST',
                    success: function(response) {
                        $('ul[id='+$id+']>li:not(:first-child)').remove();
                        console.log(response);
                        response.results.forEach((element) => {
                            $('#'+$id).append($('<option>', {
                                value: element['id'],
                                text: element['name_of_org']
                            }));
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item',
                                html: $('<span>', {
                                    text: element['name_of_org']
                                })
                            }).attr('data-value', element['id']));
                        });
                        if (response.results.length == 0) {
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item disabled',
                                html: $('<span>', {
                                    text: "Мы ничего не нашли :("
                                })
                            }));
                        }
                    },
                    error: function(response) {
                        console.log(response);
                        $('ul[id="1gf"]').append($('<li>', {
                            class: 'select__item disabled',
                            html: $('<span>', {
                                text: "Мы ничего не нашли :("
                            })
                        }));
                    }
                });
            }
        });
        $('.select').each(function() {
            var $this = $(this),
                selectOption = $this.find('option'),
                selectOptionLength = selectOption.length,
                selectedOption = selectOption.filter(':selected'),
                dur = 100;
            $id = $this.attr('id');
            $text = "Выберите значение";
            if($this.attr('old_value') ) $text = $("#" + $id + " option:selected").text();
            if(($this.hasClass('search') && $this.attr('old_value') ==" ") || $this.hasClass('search')  ){ $text ='';}
            $value = "";
            $this.hide();
            $this.wrap('<div class="select"></div>');
            $('<div>', {
                class: 'select__gap',
                text: $text,
                id: $id,
                style: "width:438px;"
            }).insertAfter($this);

            var selectGap = $this.next('.select__gap'),
                caret = selectGap.find('.caret');
            $('<ul>', {
                class: 'select__list',
                style: "width:452px;"
            }).insertAfter(selectGap);
            if ($this.hasClass('search')) {
                selectGap.attr("placeholder", 'Выберите из списка');
                selectGap.attr('contenteditable', true);
            }
            var selectList = selectGap.next('.select__list');
            if ($this.hasClass('search')) {
                selectList.attr('id', $id);
            }
            for (var i = 0; i < selectOptionLength; i++) {
                $('<li>', {
                    class: 'select__item',
                    html: $('<span>', {
                        text: selectOption.eq(i).text()
                    })
                })
                    .attr('data-value', selectOption.eq(i).val())
                    .appendTo(selectList);
            }
            if ($this.hasClass('search')) {
                $('<li>', {
                    class: 'select__item disabled',
                    html: $('<span>', {
                        text: "Введите текст"
                    })
                }).appendTo(selectList);
            }
            var selectItem = selectList.find('li');
            selectList.slideUp(0);
            selectGap.on('click', function() {
                $id = $this.attr('id');
                if (!$(this).hasClass('on')) {
                    $(this).addClass('on');
                    selectList.slideDown(dur);
                    $('body').on('click', '.select__list>li', function(e) {
                        var chooseItem = $(this).data('value');
                        $value = chooseItem;
                       $('#' + $id).val(chooseItem).attr('selected', 'selected');
                        $('.select__gap[id='+$id+']').text($(this).find('span').text());
                        selectList.slideUp(dur);
                        selectGap.removeClass('on');
                    });
                } else {
                    $(this).removeClass('on');
                    selectList.slideUp(dur);
                }
            });

        });

    });
    function OnInput (event) {
        if(event.target.value.length ==18) {
            $("#prompt").remove()
            $phone= event.target.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: '/clients/findClient',
                data: {phone:$phone},
                type: 'POST',
                success: function(response) {
                    console.log(response);

                    $('<div id=prompt></div>').insertAfter('.tel_block');
                    if(response.response ==true)
                    {
                        drawPropt(response.data);
                        $array = response.data;
                    }
                    else {
                        block_newClient();
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }
    }

</script>
<div  class="drawer">
    <div   class="content_header">
        <div  class="separatorTitle">Фильтр
        </div>
        <button style="min-width: 10px; padding: 5px" class="light" onclick =" window.location='{{route("objectsList", ["tab"=>$tab])}}'">  <div class="close"> <i class="fa fa-times" aria-hidden="true"></i> </div>
        </button>
    </div>
    <div class="pageSeparator"></div>
    <div  class="content_drawer" >

        @component('components.input_filters', ['filter_values'=>$filter_values,'filter_fields' =>$filter_fields["main"]])
        @endcomponent
        <button id='showall' class=light> Дополнительно </button>
        <div id="additional" hidden>
        @component('components.input_filters', ['filter_values'=>$filter_values,'filter_fields' =>$filter_fields["additional"]])
        @endcomponent
        </div>

    </div>
    <div  class="footer_drawer" >

        <button id='create_bt'   onclick='find()' class="seporator_item  dark">
            Найти</button>

        <button style="margin-right: 10px;" onclick='reset()'  class="seporator_item" >
            Очистить</button>

    </div>

</div>
<div  class="disable_block"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js"></script>

<script type="text/javascript" src="{{ URL::asset('js/dadata.js') }}"></script>
<!-- load jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- provide the csrf token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
