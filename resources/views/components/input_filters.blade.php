@foreach($filter_fields as $data )


    @if(($data['type_field'] ?? "text" )== "select")
        <div class="field-show">
            <div class="field-title">{{ $data['title'] }}</div>
            <select name="select-box"
                    table="{{ $data['table'] ?? 'null' }}"
                    @foreach($data['values'] as $value)

                    @if(in_array($value['id'], ($filter_values["catalog"]??[])) ||
(($filter_values['objects.type']??'') && $data['name'] =='objectTypes'))
                    old_value =true
                    selected="selected"
                    @endif @endforeach
                    id="{{$data['name'] }}"
                    class="select {{$data['where_to_look'] ?? ''}} ">
                @if($data['source'] !='search')
                    @foreach($data['values'] as $value)
                        <option
                            @if(in_array($value['id'], ($filter_values["catalog"]??[]))) selected @endif
                        @if($value['id'] == ($filter_values['objects.type']??'') && $data['name'] =='objectTypes') selected @endif
                            value="{{$value['id']}}"
                        >  {{$value['item']['title']  }}</option>
                    @endforeach
                @endif
            </select>
        </div>
            @endif

            @if(($data['type_field'] ?? "text" )== "between")
            @component('components.input_between', [
    'title' => $data['title'],
    'id_min'=>$data['values'][0]['title'],
    'id_max'=>$data['values'][1]['title'],
    'type'=>$data['type'],
    'value_min'=>$filter_values[$data['values'][0]['title']] ?? '',
    'value_max'=>$filter_values[$data['values'][1]['title']] ?? '',
    'maska'=>$data['maska'],
    ])
            @endcomponent
        @endif


    @if(($data['type_field'] ?? "text" )== "checkbox")
        <div class="input_inline_block">
            <div class="input-title_inline">{{ $data['title'] }}</div>
            @foreach($data['values'] as $item)
                <input
            @if(in_array($item['value'] , $filter_values[$data['name']] ?? [])) checked @endif
                    type=checkbox value= {{ $item['value'] }} id={{ $item['value']. $data['name'] }}  name={{ $data['name'] }} > <label style="margin-right: 20px" for = {{ $item['value']. $data['name'] }}  > {{ $item['title'] }}</label>
            @endforeach
        </div>
    @endif
        @endforeach
