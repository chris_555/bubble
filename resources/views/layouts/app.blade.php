<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>ВКЛАД</title>
    <link rel="icon" type="image/png" href=" {{ asset('images/icons/favicon.ico') }}" />
    <link rel="stylesheet" href="{{ asset('css/tab.css') }}" >
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/e7f07e1206.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/components/field.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/components/comments.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/drawer.css') }}" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
</head>
<script
    src="https://code.jquery.com/jquery-3.5.1.js"
    integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>
    function notif($text){
        $('<div>', {
            class: 'notif',
            text: $text
        }).prependTo('body').slideToggle(300);
        setTimeout(() => {$('.notif').slideToggle(300);   setTimeout(()=>$('.notif').remove(),400)}, 1500);
    }
    $(document).mouseup(function (e){
        var div = $(".select");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.next('.select__gap').removeClass('on');
            div.next('.select__gap').next('.select__list').slideUp(100);
        }
    });

    $array='';
    $(function($) {
        $('.crop').each(function() {
        var size = 120,

            text = $(this).text();

        if(text.length > size){
            $(this).text(text.slice(0, size) + '...');
        }});

        $('.bottom_bar').hide();
        $(".num").mask("+7 (999) 999-99-99");
        $(".phone").mask("+7 (999) 999-99-99");
        $("#phone").mask("+7 (999) 999-99-99");
        $('#price').mask('000 000 000 000 000', {reverse: true});
        $('.price').mask('000 000 000 000 000', {reverse: true});
        $('.float').mask('000 000 000 000 000,00', {reverse: true});
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();

        if(dd<10){dd='0'+dd}
        if(mm<10){mm='0'+mm}
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("date").setAttribute("max", today);
        document.getElementById("date").setAttribute("value", today);
        });

    function edit_ticket() {
        var ticket ={};
        $('input[type=text]').each(function () {
            $value = $(this).val();
            if ($(this).attr('id') =="price" ||$(this).hasClass("price") ) {
                $value =($value.replace(/\s+/g, '')).replace(',','.');
            }
            ticket[$(this).attr('id')] = $value;

        });
        $('input[type=date]').each(function () {
            if($(this).attr('id'))  ticket[$(this).attr('id')] = $(this).val();
        });
        $('input[type=number]').each(function () {
            if($(this).attr('id'))  ticket[$(this).attr('id')] = $(this).val();
        });
        $('textarea').each(function () {
            ticket[$(this).attr('id')] = $(this).val();
        });
        $('select').each(function () {
            ticket[$(this).attr('id')] = $(this).val();
        });
        if({{$tab ?? 0  }} == 1) {
         ticket['num_rooms'] = [];
         $('input[type=checkbox]').each(function () {
             if ($(this).prop("checked"))
                 ticket[$(this).attr('name')].push($(this).val());
         });
         ticket['num_rooms'] = (ticket['num_rooms']).toString();
     }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/tickets/editTicket',
            data: {
                ticket:JSON.stringify(ticket),
                ticket_id:{{$ticketId ?? "null"}},
                tab_id:{{$tab ?? "null"}}
            },
            dataType: "json",
            type: 'POST',
            success: function(response) {
                console.log(response);
                location ="/ticket/"+{{ $ticketId ?? 'null' }}+"/"+{{ $tab ?? "null" }};
            },
            error: function(response){
                console.log(response);
            }
        });
    };
    $( document ).ready(function() {
        if (({{ $tab ?? 0}}) >0)
        {
            $('#{{ $tab ?? 'null'}}').addClass('active');
            $('.nameTab').text(   $('.active').text());
        }
          $('#addToFav').click(function(){
            $value = $('#addToFav').attr('value');
            var url ='/tickets/addToFavorite';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: url,
                data: {entityId:{{$ticketId  ?? "null" }},entity:'ticket',value:$value},
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    if ($value=="false") {
                        $('#addToFav').html('<i class="fas fa-heart true"></i>Удалить из избранного');
                        $('#addToFav').attr('value','true');
                    }
                    else {
                        $('#addToFav').html('<i class="fas fa-heart false"></i> В избранное');
                        $('#addToFav').attr('value','false');
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        });
    });
   function changeStatus(e) {
       $action = $(e).attr('action');
       var url = '/tickets/changeStatus';
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': '<?= csrf_token() ?>'
           }
       });
       $.ajax({
           url: url,
           data: {$ticketId:{{$ticketId  ?? 'null' }}, action: $action},
           type: 'POST',
           success: function (response) {
               console.log(response);
               $(e).detach();
               $("#changeStatus").detach();
               if($action=="delete") {
                   $('.chips').html('Удалена');
                   $('<button id="changeStatus"  class="item button light" onclick="changeStatus(this);"  action="renew">Возобновить</button>').appendTo($('.header_navbar'));
               }
               if($action=="cancel")
               {
                   $('.chips').html('Отменена');
                   $('<button id="changeStatus"  class="item button light" onclick="changeStatus(this);"  action="renew">Возобновить</button>').appendTo($('.header_navbar'));
               }
               if($action=="renew")
               {
                   $(' <button  id="changeStatus" class="item button light"  onclick="changeStatus(this);"  action="cancel">Отменить</button> <button  id="changeStatus" class="item button light"  onclick="changeStatus(this);"   action="delete">Удалить</button>').appendTo($('.header_navbar'));
                   $('.chips').html('В работе');
               }
                   },
           error: function (response) {
               console.log(response);
           }
       });
   };
    function sendComment() {
        var text =  $(".input-block").html();
        var url ='/'+{{$ticketId??'null'}}+'/sendComment';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: url,
            data: {text:text},
            type: 'POST',
            success: function(response){
                console.log(response);
                $('.comments').append('<div class ="comment"> <div class="comment-header"><div class="comment-user">'+response.user+'</div> <div class="comment-date">'+response.created_at+'</div><div class="comment-content">'+text+' </div> </div>')
                $(".input-block").html('');
            },
            error: function(){
                console.log('Ошибка');
            }
        });

    };


</script>
<script>

    $( document ).ready(function(){
        $('.input-file').each(function() {
            var $input = $(this);
            $input.on('change', function(element) {
             upload(this,$(this).attr('id'));

            });
        });

        function upload(img, $id) {
            var url ='';
            if($id == "media_upload") {
                url ='/'+{{$objectId ?? "null"}}+'/upload_media';}
            if($id == "scan_deal_upload")  {
               url ='/'+{{$dealId ?? "null"}}+'/scan_deal_upload';
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: url,
                data:new FormData($("#upload_form")[0]),
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data.media);
                    $( '.name_chart').remove();
                    var div = `
                     <div class ='image'><img src='`+data.media['path']+data.media['name']+`'>
                                 @if( $make_main ?? true)
<button id="make_main" onclick='make_main_image(`+data.media["id"]+`)' class="button light ">   Сделать главной </button >
@endif <button onclick='delete_image(`+data.media["id"]+`)' class="button light req">   Удалить </button ></div >`;
                    $('.media_content').append(div);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

</script>
<body>
@if( $drawer ?? '' )
    @if( $drawer =="createticket" ) @component('components.newTicketdrawer',["tab"=>$tab, 'objectTypes' =>$objectTypes ?? ''])
    @endcomponent
    @elseif( $drawer =="filter" ) @component('components.filterDrawer',["tab"=>$tab, 'filter_values'=>$filter_values,'filter_fields' =>$filter_fields ?? ''])
    @endcomponent
    @elseif( $drawer =="createusers" ) @component('components.createUser')
    @endcomponent
    @endif
@endif
<div class="container">
    <div class="main-menu">
        <div class="container-header">
        <div class="container-header-items">
            <div class="container-items">
                <div class="logo">
                    <a href="/">
                    <div class="box-icon-logo">
                        <img class ='icon' src="{{ asset('images/icons/logo.png') }}"  />
                    </div>
                    </a>
                </div>
                <div class="btn-close">
                    <div class="icon-item"></div>
                    <div class="icon-item"></div>
                    <div class="icon-item"></div>
                    <div class="icon-item"></div>
                </div>
                <div class="item">
                    <a href="{{ route('clientsList') }}">
                        <div class="menu-item">
                            <img class ='icon' src="{{ asset('images/icons/clients.ico') }}"  />
                            <div class="menu-text">
                                Клиенты
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('developersList') }}"> <div class="menu-item">
                            <img class ='icon' src="{{ asset('images/icons/worker.svg') }}" />
                            <div class="menu-text">
                                Застройщики
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('objectsList') }}">
                        <div class="menu-item">
                            <img class ='icon' src="{{ asset('images/icons/favicon.ico') }}"/>
                            <div class="menu-text">
                                Объекты
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('ticketsList') }}"> <div class="menu-item">
                            <img class ='icon' src="{{ asset('images/icons/ticket.ico') }}" />
                            <div class="menu-text">
                                Заявки
                            </div>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('showsList') }}"> <div class="menu-item">
                            <img class ='icon' src="{{ asset('images/icons/view_show_icon.png') }}" />
                            <div class="menu-text">
                                Показы
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item">
                    <div class="menu-item logout">
                        <img class ='icon' src="{{ asset('images/icons/exit.ico') }}" href="{{ route('logout') }}"
                             onclick="event.preventDefault(); document.getElementById('logout-form').submit();"/>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="overlay">
    <main class="overlay-header">
        @yield('overlay-header')
    </main>
        <main class="content">
            @yield('content')
        </main>
        <div  class="bottom_bar">
            <button disabled id="to_coll" style="margin-right: 10px;" onclick='to_collection_shows("collection")' class="item button dark " >
                В подборку </button>
            <button disabled id='to_show' style="margin-right: 10px;" onclick='to_collection_shows("show")' class="item button dark " >
                В показ </button>
            <div style="padding: 10px 0">Заявка<span class="req">*</span> </div>
               <select name="select-box"
                        id="user_id"  class="select search posit"> </select>
            <button style="position: absolute; right: 0;margin-right: 30px; " onclick='reset_checked()' class="item button noback " >
                <i style="margin-right: 10px; " class="fa fa-times" aria-hidden="true"></i> Снять выделение (<count_checked> 0 </count_checked>) </button>
        </div>
    </div>
</div>
</body>
</html>
