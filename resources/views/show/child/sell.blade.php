<table id ="tickets">
    <thead>
    <tr>
        <th style="width: 15%"> Дата и время</th>
        <th> Объект</th>
        <th> Вторая сторона</th>
    </thead>

    @foreach ($tickets as $ticket )
        <tr>
            <td>
                <div id=set_date id_show={{$ticket['id']}}  onclick=set_date({{$ticket['id']}}) >
                    @if($ticket['date_time']==null)
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>      Запланировать
                    @else    <i class="fa fa-pencil" aria-hidden="true"></i>        {{ $ticket['date_time']  }}
                </div>
                @endif
            </td>
            <td>
                @component('components.main_photo', ['photo' => $ticket["ticket_sell"]['object']['main_photo'],'size'=>'150px'])
                @endcomponent
                <span class="item_address">  <a href={{route("objectCard", ["ticketId" => $ticket["ticket_sell"]['id'],"objectId"=>$ticket['ticket_sell']["object_id"]])}} >
                                #{{ $ticket['ticket_sell']["object_id"]}}, {{ $ticket['ticket_sell']['object']["address"]}}
                                <br>
                            </a>
                            </span>
                <p class="item_price">{{$ticket['ticket_sell']['price']}}</p>
            </td>
            <td> <span class="b2"> Заявка #  {{ $ticket["ticket_buy_id"]}} </span>
                <br> {{ $ticket["user_buy"]["fullname"]}}
                <br>{{$ticket["user_buy"]["phone"] !='' ?'тел.'.$ticket["user_buy"]["phone"]: ''}}
            </td>

        </tr>
    @endforeach
</table>
