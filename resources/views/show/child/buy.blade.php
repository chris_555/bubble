    @foreach ($tickets as $key=> $ticket1 )
      <div class="client">
          <div onclick=show(this,{{$ticket1['client']['id']}}) class="close">Показать</div>
          {{$ticket1['client']['fullname']}}
          <div class="b2"> Заявка #  {{$key}} </div>
          <div style="display: none" id = {{$ticket1['client']['id']}}>
        <table>
            <thead>
            <tr>
                <th style="width: 15%"> Дата и время</th>
                <th> Объект</th>
                <th> Вторая сторона</th>
            </thead>
        @foreach ($ticket1['shows'] as $ticket )
        <tr>
            <td>
                    <div id=set_date id_show={{$ticket['id']}}  onclick=set_date({{$ticket['id']}}) >

                        @if($ticket['date_time']==null)
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>      Запланировать
                        @else    <i class="fa fa-pencil" aria-hidden="true"></i>        {{ $ticket['date_time']  }}
                    </div>
                @endif
            </td>
            <td>
                @component('components.main_photo', ['photo' => $ticket["ticket_sell"]['object']['main_photo'],'size'=>'150px'])
                @endcomponent
                <span class="item_address">  <a href={{route("objectCard", ["ticketId" => $ticket["ticket_sell"]['id'],"objectId"=>$ticket['ticket_sell']["object_id"]])}} >
                                #{{ $ticket['ticket_sell']["object_id"]}}, {{ $ticket['ticket_sell']['object']["address"]}}
                                <br>
                            </a>
                            </span>
                <p class="item_price">{{$ticket['ticket_sell']['price']}}</p>
            </td>
            <td> <span class="b2"> Заявка #  {{ $ticket["ticket_sell_id"]}} </span>
                <br> {{ $ticket["user_sell"]["fullname"]}}
                <br>{{$ticket["user_sell"]["phone"] !='' ?'тел.'.$ticket["user_sell"]["phone"]: ''}}
            </td>
        </tr>
        @endforeach
        </table>
          </div>
          <div class="pageSeparator"></div>
      </div>
    @endforeach




