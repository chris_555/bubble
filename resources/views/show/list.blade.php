@extends('layouts.app')
@section('overlay-header')
    <link href="{{ asset('css/components/media.css') }}" rel="stylesheet" />

    <div style="   padding-top: 10px;" class="content_header">
        <div class="separatorTitle">
            показы
        </div>
    </div>
@endsection
@section('content')
    <div class="tabs">
        <span  id="1"> <a href={{route("showsList", ["tab" => 1])}}> Мои покупатели </a></span>
        <span  id="2"> <a href={{route("showsList", ["tab" => 2])}}>Мои объекты </a></span>
    </div>
    <div class="pageSeparator"></div>
    <div class="content_header">
        <div class="separatorTitle nameTab"> Vj</div>
    </div>
    <div class="pageSeparator"></div>
    <div class="objectContent" >
        @if($count==0)
            <div class="name_chart" style="margin: 150px auto;font-size: 20px "> Нет созданных показов </div>
            @else {!! $child ?? '' !!} @endif
        <div class="Modal">
            <div class="Modal_Body">
                <span class="b2"> Выберите время</span>
                <p> <input id="datetime" type="datetime-local"></p>
                <p>
                    <button onclick=save_datetime() class="seporator_item  dark">Cохранить</button>
                    <button onclick=close_modal() class="seporator_item  light">Отменить</button>
            </div>
        </div>
    </div>

    <script>
        var id;
        $( document ).ready(function() {
            $('input[type=datetime-local]').attr('value',new Date().toJSON().slice(0,19));
        });
        function set_date($id) {
            id = $id;
            close_modal();
        };
        function save_datetime() {
            close_modal();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: '/shows/set_datetime',
                data: {id:id, datetime: $('#datetime').val()},
                type: 'POST',
                success: function(response) {
                    $('div[id_show='+id+']').text('');
                    $('div[id_show='+id+']').append('<i class="fa fa-pencil" aria-hidden="true"></i> '  +response.datetime);
                    console.log(response);
                },
                error: function(response){
                    console.log(response);
                }
            });
        };
        function close_modal() {
            $('.Modal').toggle();
        };
        function show(elem, $id) {
            if(elem.textContent.trim() =="Показать")
                elem.innerHTML ="Cкрыть";
            else elem.innerHTML ="Показать";
            $('#' + $id).toggle();
        }
    </script>

@endsection

