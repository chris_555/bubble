@extends('layouts.app')

@section('overlay-header')
    <div class="returntoback">
        @if((Auth::user()->position_id == 2))
            @else
            <b> Моя статистика  </b>
            @endif
    </div>
@endsection

@section('content')
    <div class="content_header">
        <div  class="separatorTitle nameTab">{{ $user['fullname']  }}</div>
    </div>
    <div class="pageSeparator"></div>

    <div style="height: 500px">
        @if((Auth::user()->position_id == 2))
            <button id="button_edit" onclick="window.location='{{route("userCard",['id'=>$user['id']])}}'"
                    class="seporator_item  light">
                Общая информация </button>

            <button id="button_edit" onclick="window.location='{{route("ticketsList", ["tab" => 1])}}'"
                    class="seporator_item  light">
                Заявки сотрудника </button>

            <button id="button_edit" onclick="window.location='{{route("objectsList", ["tab" => 2])}}'"
                    class="seporator_item  light">
                Объекты сотрудника</button>

            <button id="button_edit" onclick="window.location='{{route("myStatistics")}}'"
                    class="seporator_item  light">
                Статистика сотрудника </button>

        @endif

        <div id="app">
            @if($date_min===0 )
                <div class="name_chart" style="margin: 150px auto;font-size: 20px "> Нет заключенных сделок </div>
                @else
   <div class="name_chart">
           Количество заключенных  сделок с {{ $date_min }}  по {{ $date_max }}</br>
        </div>
        {!! $chart->container() !!}

    </div>
        <div id="app">
            <div class="name_chart">
                Валовая выручка с продаж с {{ $date_min }}  по {{ $date_max }}</br>
            </div>
            {!! $chart2->container() !!}

        </div>
        @endif
    </div>


    <script src="https://unpkg.com/vue"></script>
    <script>
        var app = new Vue({
            el: '#app',
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.7/c3.min.js"></script>
    {!! $chart->script() !!}
    {!! $chart2->script() !!}
@endsection
