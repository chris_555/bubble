@extends('layouts.app')

@section('overlay-header')
    <div class="returntoback">
        <b>  Профиль пользователя  </b>
    </div>
@endsection

@section('content')
    <div class="content_header">
        <div  class="separatorTitle nameTab">{{ $user['fullname']  }}</div>
        <button id="button_edit" onclick="window.location='#'" class="seporator_item  light">
            Редактировать</button>
    </div>
    <div class="pageSeparator"></div>
    <div class="objectContent">
        <div class="tabs">
            <button id="button_edit" onclick="window.location='{{route("userCard",["id"=> $user['id']])}}'"
                    class="seporator_item  light">
                Общая информация </button>

            <button id="button_edit" onclick="window.location='{{route("ticketsList", ["tab" => 1])}}'"
                    class="seporator_item  light">
                 Заявки сотрудника </button>

            <button id="button_edit" onclick="window.location='{{route("objectsList", ["tab" => 2])}}'"
                    class="seporator_item  light">
                Объекты сотрудника</button>


            <button id="button_edit" onclick="window.location='{{route("userStatistics", ["id" => $user['id']])}}'"
                    class="seporator_item  light">
                 Статистика сотрудника </button>
        </div>



        <div class ="block-content" style="margin-left: 0">


        @component('components.InputInline', [
           'readonly' => 'readonly',
           'value' =>  $user['position'],
           'title' => 'Должность',
           'id'=>'position',
           ])
        @endcomponent

        @component('components.InputInline', [
      'readonly' => 'readonly',
      'value' => $user['date_of_birth'],
      'title' => 'Дата рождения',
      'id'=>'date_of_birth',
      ])
        @endcomponent
            @component('components.InputInline', [
    'readonly' => 'readonly',
                   'value' => $user['gender'] ==1 ?'Мужской':"Женский",
                   'title' => 'Пол',
                   'id'=>'gender',
                   ])
            @endcomponent

        @component('components.InputInline', [
      'readonly' => 'readonly',
      'value' => $user['phone'],
      'title' => 'Номер телефона',
      'id'=>'phone',
      ])
        @endcomponent

        @component('components.InputInline', [
           'readonly' => 'readonly',
           'value' =>  $user['email'],
           'title' => 'email',
           'id'=>'email',
           ])
        @endcomponent
    </div>


@endsection
