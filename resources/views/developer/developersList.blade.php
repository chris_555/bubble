@extends('layouts.app')
@section('overlay-header')
    <div class="search">
        <div class="search_box">
            <input id="search_input" type="text" value ="{{ Cache::get('developers_search') ??''}}" placeholder="ФИО или наименование организации">
            <button id="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>

    </div>

@endsection
@section('content')
    <div class="content_header">
        <div class="separatorTitle">
 ЗАСТРОЙЩИКИ
        </div>

        <div class="pagination">
           <span>{{($offset)+1}} - {{($offset+$count)}} </span>  из <allcount>{{$allcount }} </allcount>
            <div class="item_pagination" id="back"> < </div>
            <div class="item_pagination" id="next"> > </div>
        </div>

    </div>
    <div class="pageSeparator"></div>


    <table id="developers">
        <thead>
        <tr>
            <th>Организация</th>
            <th>ФИО застройщика</th>
            <th>Дата создания</th>
        </tr>
        </thead>
        <tbody>
        @foreach($developers as $developer)
        <tr>
            <td> <a href={{route("developerCard", ["developerId" =>  $developer['id']])}}>  {{ $developer['name_of_org'] }} </a></td>
            <td> {{ $developer['fio'] }}</td>
            <td>{{ $developer['created_at'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        function tableDraw(response){
            $('.pagination > span').text((response.new_offset +1)+" - "+(response.new_offset + response.data.length));
            if(response.new_offset ==0)$('#back').addClass('disabled'); else $('#back').removeClass('disabled');
            if((response.new_offset+15) >= response.count)$('#next').addClass('disabled'); else $('#next').removeClass('disabled');
            $("#developers > tbody").html("");
            response.data.forEach(element => {
                var tr =` <tr> <td> <a href ="\developer\\`+element['id']+`">`+element['name_of_org']+`</a></td>
                              <td>`+element['fio']+`</td>
                              <td>`+element['created_at']+`</td>
                              </tr>
                `;

                $('#developers > tbody').append(tr);

            });
        };

        $( document ).ready(function() {
            $('#search_button').on('click', function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/developers/search',
                    data: {text: $('#search_input').val()},
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        tableDraw(response);
                        $('allcount').text(response.count);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });


            if({{Cache::get('developers_page',0) }} == "0") $('#back').addClass('disabled');
            if({{Cache::get('developers_page',0) +15 }} >= {{$allcount}}) $('#next').addClass('disabled');

            $('.item_pagination').on('click', function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/developers/get',
                    data: {page: $(this).attr('id')},
                    type: 'POST',
                    success: function(response) {
                        tableDraw(response);
                        console.log(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });
        })
    </script>
@endsection

