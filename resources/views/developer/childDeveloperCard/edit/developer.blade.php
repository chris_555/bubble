
<div class ="block-content">
    <h3>
        Основная информация</br>
    </h3>

    @component('components.InputInline', [
  'value' => $developer['full_name_of_org'],
  'title' => 'Полное  наименование',
  'id'=>'full_name_of_org',
   'req'=>true,
  'type'=>'textarea'
  ])
    @endcomponent

    @component('components.InputInline', [
   'value' => $developer['name_of_org'],
   'title' => 'Сокращенное наименование',
   'req'=>true,
   'id'=>'name_of_org',
   ])
    @endcomponent

    <div class="input_inline_block">
        <div class="input-title_inline">Юридеческий адрес  <span class="req">*</span> </div>
        @component('components.dadataTextarea_clear',['value'=> $developer['address_id'] ])
        @endcomponent
    </div>

    @component('components.InputInline', [
    'value' => $developer['inn'],
    'title' => 'ИНН',
    'req'=>true,
    'id'=>'inn',
    ])
    @endcomponent

    @component('components.InputInline', [
    'value' => $developer['kpp'],
    'title' => 'КПП',
    'req'=>true,
    'id'=>'kpp',
    ])
    @endcomponent

    @component('components.InputInline', [
    'value' => $developer['ogrn'],
    'title' => 'ОГРН',
    'req'=>true,
    'id'=>'ogrn',
    ])
    @endcomponent
    @component('components.InputInline', [
       'value' => $developer['date_registration'],
       'title' => 'Дата регистрации',
       'req'=>true,
       'id'=>'date_registration',
       'type'=>'date'
       ])
    @endcomponent

</div>
<div class ="block-content">
        <h3>
            Генеральный директор</br>
        </h3>

    @component('components.InputInline', [
    'value' => $developer['second_name'],
    'title' => 'Фамилия',
     'req'=>true,
    'id'=>'second_name',
    ])
    @endcomponent

        @component('components.InputInline', [
        'value' => $developer['first_name'],
        'title' => 'Имя',
        'req'=>true,
        'id'=>'first_name',
        ])
        @endcomponent


        @component('components.InputInline', [
        'value' => $developer['last_name'],
        'title' => 'Отчество',
         'req'=>true,
        'id'=>'last_name',
        ])
        @endcomponent
        </div>

        <div class ="block-content">
            @if(($developer['phones']??'')!='')
        <h3>
          Контактная информация</br>
        </h3>
        @foreach($developer['phones'] as $key => $object_field)
            @component('components.InputInline', [
      'value' => $object_field['position'],
      'title' => 'Отдел или должность',
      'id'=>'сontact',
      'attr'=>'id_contact='.$key.' field_name=position',
      ])@endcomponent

            @component('components.InputInline', [
 'value' => $object_field['phone'],
 'maska' => 'phone',
 'title' => 'Телефон',
 'id'=>'сontact',
 'attr'=>'id_contact='.$key.' field_name=phone',
 ])@endcomponent
            <div class="pageSeparator"></div>
        @endforeach
            @endif
    </div>

    <script>
        function edit_developer(){
            var developer = {};
            var contacts = {};
            var contact = {};
            $('textarea').each(function(){
                developer[$(this).attr('id')] = $(this).val();
            });
            $('input[type=text]').each(function() {
                $value = $(this).val();
                if($(this).attr('id')=="сontact"){
                    if($(this).hasClass('phone')) $value = $value.replace(/\D+/g,"") ;
                    contact[$(this).attr('field_name')] = $value;
                    $value =  contact[$(this).attr('field_name')];
                    contacts[$(this).attr('id_contact')]={  ...contacts[$(this).attr('id_contact')], [$(this).attr('field_name')]: $value};
                }
                else developer[$(this).attr('id')] = $value;

            });
            $('input[type=date]').each(function() {
                developer[$(this).attr('id')] = $(this).val();
            });
            developer['contacts'] =contacts ;
            console.log(developer);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: '/developer/edit',
                data: {developerId: {{$developer['id'] ?? null}}, developer: developer},
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    location ="/developer/"+{{ $developer['id']  ?? 'null' }};
                },
                error: function(response){
                    console.log(response);
                }
            });
        };
    </script>
