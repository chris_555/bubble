<div class ="block-content">
    <h3>
        Основная информация</br>
    </h3>

    @component('components.InputInline', [
  'readonly' => 'readonly',
  'value' => $developer['full_name_of_org'],
  'title' => 'Полное  наименование',
  'id'=>'full_name_of_org',
  'type'=>'textarea'
  ])
    @endcomponent


    @component('components.InputInline', [
   'readonly' => 'readonly',
   'value' => $developer['name_of_org'],
   'title' => 'Сокращенное наименование',
   'id'=>'name_of_org',
   ])
    @endcomponent

    @component('components.InputInline', [
       'readonly' => 'readonly',
       'value' => $developer['address_id'],
       'title' => 'Юридический адрес',
       'id'=>'address_id',
       ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $developer['inn'],
    'title' => 'ИНН',
    'id'=>'inn',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $developer['kpp'],
    'title' => 'КПП',
    'id'=>'kpp',
    ])
    @endcomponent

    @component('components.InputInline', [
    'readonly' => 'readonly',
    'value' => $developer['ogrn'],
    'title' => 'ОГРН',
    'id'=>'ogrn',
    ])
    @endcomponent

    @component('components.InputInline', [
       'readonly' => 'readonly',
       'value' =>  date_format(date_create($developer['date_registration']),'d.m.Y' ),
       'title' => 'Дата регистрации',
       'id'=>'date_registration',
       ])
    @endcomponent

</div>
<div class ="block-content">
        <h3>
            Генеральный директор</br>
        </h3>
    @component('components.InputInline', [
        'readonly' => 'readonly',
        'value' => $developer['second_name'],
        'title' => 'Фамилия',
        'id'=>'second_name',
        ])
    @endcomponent

        @component('components.InputInline', [
        'readonly' => 'readonly',
        'value' => $developer['first_name'],
        'title' => 'Имя',
        'id'=>'first_name',
        ])
        @endcomponent
        @component('components.InputInline', [
        'readonly' => 'readonly',
        'value' => $developer['last_name'],
        'title' => 'Отчество',
        'id'=>'last_name',
        ])
        @endcomponent
    </div>


    <div class ="block-content">
    @if(($developer['phones']??'')!='')
    </br>
    <h3>
        Контактная информация</br>
    </h3>
    @foreach($developer['phones'] as $key => $object_field)

        @component('components.InputInline', [
'readonly' => 'readonly',
'value' => $object_field['phone'],
'maska' => 'phone',
'title' => $object_field['position'],
'id'=>'сontact',
])@endcomponent

        <div class="pageSeparator"></div>
    @endforeach
    @endif
    </div>
