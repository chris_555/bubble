
@extends('layouts.app')

@section('overlay-header')
<div class="returntoback">
    <a href="{{ route('developersList') }}"> <i class="fas fa-angle-left dark-icon"></i>  К списку застройщиков</a>
   <b>/  {{ $developer['name_of_org'] }}  </b>
</div>
@endsection

@section('content')
    <div class="tabs">
        <span  id="1"> <a href={{route("developerCard", ["developerId" => $developerId, "tab" => 1])}}> О застройщике </a> </span>
        <span  id="2"> <a href={{route("developerCard", ["developerId" => $developerId, "tab" => 2])}}>Объекты застройщика</a></span>
    </div>

    <div class="pageSeparator"></div>
    <div class="content_header">
    <div  class="separatorTitle nameTab">О клиенте</div>
        @if(!$edit && $tab==1) <button id="button_edit" onclick="window.location='{{route("developerCard", [
    "tab" => $tab,
    "developerId" => $developerId,
    "edit" =>'edit'
    ])}}'" class="seporator_item  light">
            Редактировать</button>
        @elseif($edit && $tab==1)
            <button onclick="edit_developer()"
                    class="seporator_item  light">
                Сохранить</button>
        @endif
    </div>
    <div class="pageSeparator"></div>
        <div class="objectContent" >
            {!! $child ?? '' !!}
        </div>

@endsection

