@extends('layouts.app')
@section('overlay-header')
<div class="returntoback">
    <a href="{{ route('objectsList') }}">     <i class="fas fa-angle-left dark-icon"></i>  К списку объектов</a>
   <b>/ Объект #{{ $objectId }}  </b>
</div>
@endsection

@section('content')
    <div class="tabs">
        <span  id="1"> <a href={{route("objectCard", ["objectId" => $objectId,"ticketId"=>$ticketId, "tab" => 1])}}> О заявке </a> </span>
        <span  id="2"> <a href={{route("objectCard", ["objectId" => $objectId,"ticketId"=>$ticketId, "tab" => 2])}}>Об объекте</a></span>
        <span  id="3"> <a href={{route("objectCard", ["objectId" => $objectId,"ticketId"=>$ticketId, "tab" => 3])}}>Медиа</a></span>
        <span  id="4"> <a href={{route("objectCard", ["objectId" => $objectId,"ticketId"=>$ticketId, "tab" => 4])}}>Похожие объекты</a></span>
    </div>

    <div class="pageSeparator"></div>
    <div class="content_header">
    <div  class="separatorTitle nameTab">О заявке</div>
        @if(!$edit && $tab==2) <button id="button_edit" onclick="window.location='{{route("objectCard", [
    "tab" => $tab,
    "objectId" => $objectId,
    "ticketId"=>$ticketId,
    "edit" =>'edit'
    ])}}'"
                            class="seporator_item  light">
            Редактировать</button>
        @elseif($edit && $tab==2)
            <button onclick="edit_object()"
                    class="seporator_item  light">
                Сохранить</button>
        @endif
    </div>

    <div class="pageSeparator"></div>
        <div class="objectContent" >
            {!! $child ?? '' !!}
        </div>
    <div class="pageSeparator"></div>
    <div class="content_header">
    <div class="separatorTitle"> Комментарии</div>
    </div>
    @component('components.comments', ['comments' => $comments])
        @slot('width')1200px @endslot
    @endcomponent

    @component('components.inputeComment')
        @slot('width')1200px @endslot
        @endcomponent

    </div>
@endsection

