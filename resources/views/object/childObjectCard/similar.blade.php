@if($objects['inStreet'])
    <span  class="info" style="margin-top: 10px;margin-bottom: 0">

        </span>
<table>
    <tr>
        <th>Объект</th>
        <th>Характеристики</th>
        <th>Цена</th>
        <th>Медиа</th>
        <th>Риэлтор</th>
        <th>Владелец</th>
        <th>Описание</th>
        <th>Последний комментарий</th>

    @foreach($objects['inStreet'] as $object)
        <tr style ="border-left: 2px solid  {{ $object["ticket"]["stage"]["color"]}}">
            <td><a href={{route("objectCard", ["objectId" => $object["id"],"ticketId"=>$object["ticket"]['id']])}}>

                    <span style="color:gray">{{ $object["object_params"]["type"] }} </span>
                    <br># {{$object["id"]}}
                    <br>{{ $object["object_params"]["address"] }} </a>
            </td>

            <td>
                Площадь: {{$object["object_params"]["area"]  }}
                <br>Этаж: {{ $object["object_params"]["floor"]}}
                <br>Год постройки: {{$object["object_params"]["year_construct"]}}
                <br>Кол-во комнат: {{$object["object_params"]["num_rooms"]}}
            </td>

            <td>{{$object["ticket"]["params"]["price"]}} тыс. руб.</td>
            <td><a href={{route("objectCard", ["objectId" => $object["id"],"ticketId"=>$object["ticket"]['id'], "tabId"=>3])}} > Фото({{ $object["object_params"]["media_count"] }}) </a></td>

            <td> <a href="#"> {{$object["ticket"]["user"]["fullname"]}} </a> </td>

            <td>
                <a href="#"> {{$object["ticket"]["client"]["fullname"]}} </a> <br>
            </td>

            <td><div class="crop">{{$object["ticket"]["params"]["description"]}} </div> </td>

            <td>
                @if(!is_null($object["ticket"]['lastComment']))
                    <div class ="comments" style="width:100px;" >
                        <div class ="comment">
                            <div class="comment-header">
                                <div class="comment-user"> {{  $object["ticket"]['lastComment']['user']['fullname']  }} </div>  </div>
                        </div>
                        <div class="comment-content">
                            {{  $object["ticket"]['lastComment']['text']  }}
                        </div>
                    </div >

                    </div>
                @endif
            </td>

        </tr>

    @endforeach
</table>
    @endif
