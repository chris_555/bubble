<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class ="block_right">
<div class="upload">
    <form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >
    <div class="form-group">
        <input type="file" name="file" id="media_upload" class="input-file">
        <label for="media_upload" class="btn btn-tertiary js-labelFile">
            <i class="icon fa fa-check"></i>
            <span class="js-fileName">Загрузить файл</span>
        </label>
    </div>
    </form>
    <span  class="info">
            <span class="req">*</span>Главная фотография будет использоваться как миниатура объекта
        </span>
</div>
</div>
@component('components.media', ['media' => $media,'tab'=>$tab,'make_main'=>true])
@endcomponent
