
    <div class ="block-content">

        @component('components.fieldShow')
   @slot('title')
       Клиент
   @endslot
   @slot('value')
       {{ $ticket['client']['fullname']}}
       <br> тел.: {{ $ticket['client']['phone']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
            @component('components.fieldShow')
   @slot('title')
       Стоимость
   @endslot
   @slot('value')
       {{ $ticket['params']['price']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
            @component('components.fieldShow')
   @slot('title')
       Тип предложения
   @endslot
   @slot('value')
           {{ $ticket['params']['type']['title']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
            @component('components.fieldShow')
   @slot('title')
       Риэлтор
   @endslot
   @slot('value')
       {{ $ticket['user']['fullname']}}
       <br> тел.: {{ $ticket['user']['phone']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
            @component('components.fieldShow')
   @slot('title')
       Заявка
   @endslot
   @slot('value')
       # <a href={{ route ("ticketCard", ["ticketId"=> $ticket['id']])}} >{{ $ticket["id"] }} </a>
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
            @component('components.fieldShow')
   @slot('title')
       Дата создания заявки
       @endslot
       @slot('value')
       {{ $ticket["created_at"] }} </a>
   @endslot
   @endcomponent

</div>
<div class ="block-content">

    @component('components.fieldShow')
   @slot('title')
       Этап
   @endslot
   @slot('value')
           {{  $ticket['stage']['title']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
   @slot('title')
       Статус
   @endslot
   @slot('value')
           {{  $ticket['status']['title']}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
   @slot('title')
       Тип заявки
   @endslot
   @slot('value')
       {{ $ticket['params']['type']['title']}}
   @endslot
   @endcomponent

</div>
<div class ="block-content">
    @component('components.fieldShow')
   @slot('title')
       Сделка
   @endslot
   @slot('value')
           {{ $deal!=null?'Заключена':'Не заключена'}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
   @slot('title')
       Номер договора
   @endslot
   @slot('value')
           {{ $deal !=null?'№'.$deal[0]['id']:''}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
   @slot('title')
       Дата заключения
   @endslot
   @slot('value')
           {{ $deal !=null ? date_format(date_create($deal[0]['date']), 'd.m.Y'): ''}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
   @slot('title')
       Сумма по договору
   @endslot
   @slot('value')
           {{ $deal!=null ?number_format($deal[0]['price'], 0, ' ', ' ').' руб.':''}}
   @endslot
   @endcomponent
   <div class="pageSeparator"></div>
        @component('components.fieldShow')
            @slot('title')
                Валовая выручка
                @if(!$deal)
                    <span  class="info" style="margin-bottom: 0" >*</span>
                @endif
            @endslot
            @slot('value')
                {{ $ticket['revenue']}}

            @endslot
        @endcomponent
        @if(!$deal)
            <span  class="info" > <span  class="info" >*</span> Предварительное значение, итоговая выручка будет рассчитана после заключения сделки</span>
        @endif
</div>

