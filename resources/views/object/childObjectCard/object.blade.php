

<div class ="block-content">
    <div id="map" style="width:555px; height:375px"></div>
    @foreach($object['1'] as $key =>  $object_field)
        @component('components.fieldShow_auto', ['data' => $object_field])@endcomponent
         @if($key <(count($object['1'])-1))      <div class="pageSeparator"></div>   @endif

    @endforeach
</div>
<div class ="block-content">
    @foreach($object['2'] as $key => $object_field)
        @component('components.fieldShow_auto', ['data' => $object_field])@endcomponent
            @if($key <(count($object['2'])-1))      <div class="pageSeparator"></div>   @endif

    @endforeach
</div>
<div class ="block-content">
    @foreach($object['3'] as $key => $object_field)
        @component('components.fieldShow_auto', ['data' => $object_field])@endcomponent
            @if($key <(count($object['3'])-1))      <div class="pageSeparator"></div>   @endif

    @endforeach
</div>


<script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
<script type="text/javascript">
    var map;

    DG.then(function () {
        map = DG.map('map', {
            center: [{{$object['1'][0]['geo_lat']}}, {{$object['1'][0]['geo_lon']}}],
            zoom: 16
        });

        DG.marker([{{$object['1'][0]['geo_lat']}}, {{$object['1'][0]['geo_lon']}}]).addTo(map).bindPopup('{{$object['1'][0]['value']}}');
    });
</script>
