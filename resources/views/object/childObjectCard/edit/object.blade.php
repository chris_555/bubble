<div class="block-content">
    @foreach($object['1'] as $key => $object_field) @component('components.input_auto', ['data' => $object_field])@endcomponent @if($key
  <(count($object[ '1'])-1)) @endif @endforeach </div>

<div class="block-content">
    @foreach($object['2'] as $key => $object_field) @component('components.input_auto', ['data' => $object_field])@endcomponent @if($key
      <(count($object[ '2'])-1)) @endif @endforeach </div>

<div class="block-content">
    @foreach($object['3'] as $key => $object_field) @component('components.input_auto', ['data' => $object_field])@endcomponent @if($key
          <(count($object[ '3'])-1)) @endif @endforeach </div>


<script>
    $(function() {
        $('textarea').each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight - 15) + 'px;overflow-y:hidden; min-height:20px');
        }).on('input', function() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $('body').on('keydown', '.select__gap', function(e) {
            $this = $(this);
            $text = $this.text();
            $id =$this.attr('id');
            $('ul[id=' + $id+ ']>li:not(:first-child)').remove();
            if ($text != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/developers/findDeveloper',
                    data: {
                        developer_name: $text
                    },
                    dataType: "json",
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        $('ul[id='+$id+']>li:not(:first-child)').remove();
                        console.log(response);
                        response.results.forEach((element) => {
                            $('#'+$id).append($('<option>', {
                                value: element['id'],
                                text: element['name_of_org']
                            }));
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item',
                                html: $('<span>', {
                                    text: element['name_of_org']
                                })
                            }).attr('data-value', element['id']));
                        });
                        if (response.results.length == 0) {
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item disabled',
                                html: $('<span>', {
                                    text: "Мы ничего не нашли :("
                                })
                            }));
                        }
                    },
                    error: function(response) {
                        console.log(response);
                        $('ul[id="1gf"]').append($('<li>', {
                            class: 'select__item disabled',
                            html: $('<span>', {
                                text: "Мы ничего не нашли :("
                            })
                        }));
                    }
                });
            }
        });

        $('.select').each(function() {
            var $this = $(this),
                selectOption = $this.find('option'),
                selectOptionLength = selectOption.length,
                selectedOption = selectOption.filter(':selected'),
                dur = 100;
            $id = $this.attr('id');
            $text = $("#" + $id + " option:selected").text();
            if($this.attr('old_value') ==" " ){ $text ='Не указано';}
            if($this.hasClass('search') && $this.attr('old_value') ==" " ){ $text ='';}
            $value = "";
            $this.hide();
            $this.wrap('<div class="select"></div>');
            $('<div>', {
                class: 'select__gap',
                text: $text,
                id: $id,
            }).insertAfter($this);

            var selectGap = $this.next('.select__gap'),
                caret = selectGap.find('.caret');
            $('<ul>', {
                class: 'select__list',
                style: "width:352px;"
            }).insertAfter(selectGap);
            if ($this.hasClass('search')) {
                selectGap.attr("placeholder", 'Выберите из списка');
                selectGap.attr('contenteditable', true);
            }
            var selectList = selectGap.next('.select__list');
            if ($this.hasClass('search')) {
                selectList.attr('id', $id);
            }
            for (var i = 0; i < selectOptionLength; i++) {
                $('<li>', {
                    class: 'select__item',
                    html: $('<span>', {
                        text: selectOption.eq(i).text()
                    })
                })
                    .attr('data-value', selectOption.eq(i).val())
                    .appendTo(selectList);
            }
            if ($this.hasClass('search')) {
                $('<li>', {
                    class: 'select__item disabled',
                    html: $('<span>', {
                        text: "Введите текст"
                    })
                }).appendTo(selectList);
            }
            var selectItem = selectList.find('li');
            selectList.slideUp(0);
            selectGap.on('click', function() {
                $id = $this.attr('id');
                if (!$(this).hasClass('on')) {
                    $(this).addClass('on');
                    selectList.slideDown(dur);
                    $('body').on('click', '.select__list>li', function(e) {
                        var chooseItem = $(this).data('value');
                        $value = chooseItem;
                        $('#' + $id).val(chooseItem).attr('selected', 'selected');
                        $('.select__gap[id='+$id+']').text($(this).find('span').text());
                        selectList.slideUp(dur);
                        selectGap.removeClass('on');
                    });
                } else {
                    $(this).removeClass('on');
                    selectList.slideUp(dur);
                }
            });
        });
    });

    function edit_object() {
        var object = {};
        object['catalog_items'] = [];
        object['objects'] = [];
        object['tickets'] = [];

        $('input[type=text]').each(function() {
            if ($(this).attr('id')) {
                $value = $(this).val();
                if ($(this).hasClass('float')) $value =$value.replace(',','.');
                object[$(this).attr('table')].push({
                    'field':$(this).attr('id'),
                    'value':$value,
                })

            }

        });
        $('input[type=date]').each(function() {
            if ($(this).attr('id')) object[$(this).attr('id')] = $(this).val();
        });
        $('input[type=number]').each(function() {
            if ($(this).attr('id')) {
                $value = $(this).val();
                object[$(this).attr('table')].push({
                    'field':$(this).attr('id'),
                    'value':$value,
                })
            }
        });
        $('textarea').each(function() {
            if($(this).attr('table'))
            object[$(this).attr('table')].push({
                'field':$(this).attr('id'),
                'value':$(this).val(),
            });
            else object[$(this).attr('id')] = $(this).val();

        });

        $('select').each(function() {
            if ($(this).attr('is_catalog') == "true")
                object['catalog_items'].push({
                    'catalog_name': $(this).attr('id'),
                    'old_value':$(this).attr('old_value'),
                    'new_value': $(this).val()
                });
            else if($(this).attr('table'))
                object[$(this).attr('table')].push({
                    'field':$(this).attr('id'),
                    'value':$(this).val(),
                });
            else object[$(this).attr('id')] = $(this).val();
        });
        console.log(JSON.stringify(object));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/objects/editObject',
            data: {
                object: JSON.stringify(object),
                ticket_id: {{$ticketId ?? "null" }},
                object_id: {{$objectId ?? "null"}},
                tab_id: {{ $tab ?? "null" }}
    },
        dataType: "json",
            type: 'POST',
            success: function(response) {
            console.log(response);
           location ="/object/"+{{ $objectId ?? 'null' }}+"/ticket/"+{{ $ticketId ?? 'null' }}+'/'+{{ $tab ?? "null" }};
        },
        error: function(response) {
            console.log(response);
        }
    });
    }
</script>
