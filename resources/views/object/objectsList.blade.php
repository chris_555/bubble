
@extends('layouts.app')
@section('overlay-header')
    <div class="search">
        <div class="search_box">
            <input id="search_input" type="text" value ="{{ Cache::get('objects_search') ??''}}" placeholder="Код объекта">
            <button id="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
    </div>
    <button onclick="window.location='{{route("objectsList", ["drawer"=>"filter","tab"=>$tab])}}'" class="seporator_item  dark">
        <b style="margin-right: 10px; z-index: 0;"><i class="fa fa-bars" aria-hidden="true">
                @if($filter_values)   <span class="filter_on"></span> @endif
            </i></b> Фильтр</button>
@endsection

@section('content')
    <div class="content_header">
        <div class="separatorTitle">ОБЪЕКТЫ</div> </div>
    <div class="pageSeparator"></div>
    <div class="tabs">

        <span  id="1"> <a href={{route("objectsList", ["tab" => 1])}}>Все объекты</a></span>
        <span  id="2"> <a href={{route("objectsList", ["tab" => 2])}}>Мои объекты</a></span>
        <div class="pagination">
            <span>{{Cache::get('objects_page',0)+1 }} - {{Cache::get('objects_page',0)+count($objects) }} </span>  из <allcount>{{$allcount }} </allcount>
            <div class="item_pagination" id="back"> < </div>
            <div class="item_pagination" id="next"> > </div>
        </div>
    </div>
   <div class ="map_table">
       <table id="objects">
        <thead>
        <tr>
            <th></th>
            <th>Объект</th>
            <th>Характеристики</th>
            <th>Цена</th>
            <th>Медиа</th>
            <th>Риэлтор</th>
            <th>Владелец</th>
            <th>Описание</th>
            <th>Последний комментарий</th>
        </thead>
        @foreach($objects as $object)
            <tr>
                <td>
                    <input value={{$object["ticket"]["id"]}} type=checkbox name="checked_objects" id='ticket_{{$object["ticket"]["id"]}}'>
                    <label for = 'ticket_{{$object["ticket"]["id"]}}' ></label>
                </td>
                <td><a href={{route("objectCard", ["objectId" => $object["id"],"ticketId"=>$object["ticket"]['id']])}}>
                        <span style="color:gray">{{ $object["object_params"]["type"] }} </span>
                        <br># {{$object["id"]}}
                        <br>{{ $object["object_params"]["address"] }} </a>
                </td>

                <td>
                    Площадь: {{$object["object_params"]["area"]  }}
                    <br>Кол-во комнат: {{$object["object_params"]["num_rooms"]}}
                    <br>Этаж: {{ $object["object_params"]["floor"]}}
                    <br>Год постройки: {{$object["object_params"]["year_construct"]}}
                    <br>Состояние: {{$object["object_params"]["Состояние"]['title']}}
                </td>

                <td>{{$object["ticket"]["params"]["price"]}}</td>
                <td>
                    <a href={{route("objectCard", ["objectId" => $object["id"],"ticketId"=>$object["ticket"]['id'], "tabId"=>3])}} > Фото({{ $object["object_params"]["media_count"] }}) </a></td>
                <td>  {{$object["ticket"]["user"]["fullname"]}}  </td>

                <td>
                    <a href={{route("clientCard", ["clientId" => $object["ticket"]["client"]["id"]])}}> {{$object["ticket"]["client"]["fullname"]}} </a> <br>
                </td>

                <td>
                  <div class="crop">  {{$object["ticket"]["params"]["description"]}}</div>
                </td>

                <td>
                    @if(!is_null($object["ticket"]['lastComment']))
                        <div class ="comments" style="width:100px;" >
                            <div class ="comment">
                                <div class="comment-header">
                                    <div class="comment-user"> {{  $object["ticket"]['lastComment']['user']['initials']  }} </div>  </div>
                            </div>
                            <div class="comment-content" style="max-width: 100px;word-wrap: break-word;width: min-content;">
                                <div class="crop">    {{  $object["ticket"]['lastComment']['text']  }}</div>
                            </div>
                        </div>
                    @endif
                </td>

            </tr>

        @endforeach
    </table>
    <div id="list_map"></div>
    </div>

   <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
   <script type="text/javascript">
       var map,markers;
       DG.then(function () {
           markers = DG.featureGroup(),
           map = DG.map('list_map', {
               center: [57.153033, 65.534328],
               zoom: 13
           });
           map_f();
       });

       function map_f(){
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': '<?= csrf_token() ?>'
               }
           });
           $.ajax({
               url: '/objects/getCoords',
               data: {tab: {{$tab ?? ''}} },
               type: 'POST',
               success: function (response) {
                   console.log(response);
                   response.data.forEach(element => {
                       $img='';
                       if(element['params']['main_photo']){
                           $path= element['params']['main_photo']['path']+''+element['params']['main_photo']['name'] ;
                           $img   = '<div class="image_map"> ' +
                               '<img src="/'+$path+'">' +
                               '</div>';
}
                        block = '<div class="map_block">'+ $img +'<div class="text_map">'+element['params']['type']+'<b>' +
                           '<a style="color:gold" href="/object/'+element["id"]+'/ticket/'+element["ticket_id"]+'">' +
                           '<p>#'+ element["id"]+'</a></b>'+'<br>'+ element["params"]["address_short"]+
                           '<p>'+element['params']['short_par'] +
                           '<p><b>'+element['price'] +'</b></div></div>';

                       DG.marker([element['coords']['geo_lat'],element['coords']['geo_lon']])
                           .addTo(markers)
                           .bindPopup(block);
                       markers.addTo(map);
                   });

               },
               error: function (response) {

                   console.log(response);
               }
           });
       }

   </script>
   <script>
       function to_collection_shows($type){
           $ticket_id = $('select').val();
           $ckecked_objects =[];

           $(' input[type=checkbox]').each(function(){
               $value = $(this).val();
               if($value !=null) {
                   if( $(this).prop("checked"))
                       $ckecked_objects.push($(this).val());
               }
           });

           if($type =='collection'){
               $url =  '/collections/create';
               $text = 'Подборка сохранена!'
           }
           if($type =='show') {
               $url =  '/shows/create';
               $text = 'Показ создан!'
           }

           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': '<?= csrf_token() ?>'
               }
           });
           $.ajax({
               url: $url,
               data: {ticket_id: $ticket_id, ckecked_objects: $ckecked_objects },
               type: 'POST',
               success: function (response) {
                   notif($text);
                   console.log(response);
                   reset_checked();
               },
               error: function (response) {
                   console.log(response);
               }
           });
       }
       function reset_checked(){
           $('.bottom_bar').hide();
           $('input:checked').prop('checked', false);
       }
       $(function() {
           $('body').on('click', 'input[name=checked_objects]', function(e) {
              if($("input[name=checked_objects]:checked").length ==0)
              $('.bottom_bar').hide();
              else
              {
                  $('count_checked').text($("input[name=checked_objects]:checked").length );
                  $('.bottom_bar').show();
              }
           });

           $('body').on('keyup', '.select__gap', function(e) {
           $this = $(this);
           $text = $this.text();
           $id =$this.attr('id');
           $url = '/tickets/findMyTicket';
           if ($text != '') {
               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                   }
               });
               $.ajax({
                   url: $url,
                   data: {
                       text: $text
                   },
                   dataType: "json",
                   type: 'POST',
                   success: function(response) {
                       $('ul[id='+$id+']>li:not(:first-child)').remove();
                       console.log(response);
                       response.results.forEach((element) => {
                           $('#'+$id).append($('<option>', {
                               value: element['id'],
                               text: element['name']
                           }));
                           $('ul[id='+$id+']').append($('<li>', {
                               class: 'select__item',
                               html: $('<span>', {
                                   text: element['name']
                               })
                           }).attr('data-value', element['id']));
                       });
                       if (response.results.length == 0) {
                           $('ul[id='+$id+']').append($('<li>', {
                               class: 'select__item disabled',
                               html: $('<span>', {
                                   text: "Мы ничего не нашли :("
                               })
                           }));
                       }
                   },
                   error: function(response) {
                       console.log(response);
                       $('ul[id="1gf"]').append($('<li>', {
                           class: 'select__item disabled',
                           html: $('<span>', {
                               text: "Мы ничего не нашли :("
                           })
                       }));
                   }
               });
           }
       });
       $('.select').each(function() {
           if( $(this).hasClass('posit'))
           {  var $this = $(this),
               selectOption = $this.find('option'),
               selectOptionLength = selectOption.length,
               selectedOption = selectOption.filter(':selected'),
               dur = 100;
           $id = $this.attr('id');
           $text = $("#" + $id + " option:selected").text();
           if($this.attr('old_value') ==" " ){ $text ='Не указано';}
           if($this.hasClass('search') && $this.attr('old_value') ==" " ){ $text ='';}
           $value = "";
           $this.hide();
           $this.wrap('<div class="select"></div>');
           $('<div>', {
               class: 'select__gap',
               text: $text,
               id: $id,
           }).insertAfter($this);

           var selectGap = $this.next('.select__gap'),
               caret = selectGap.find('.caret');
           $('<ul>', {
               class: 'select__list',
               style: "width:352px;"
           }).insertBefore(selectGap);

           if ($this.hasClass('search')) {
               selectGap.attr("placeholder", 'Выберите из списка');
               selectGap.attr('contenteditable', true);
           }
           var selectList = $this.next('.select__list');
           if ($this.hasClass('search')) {
               selectList.attr('id', $id);
           }
           for (var i = 0; i < selectOptionLength; i++) {
               $('<li>', {
                   class: 'select__item',
                   html: $('<span>', {
                       text: selectOption.eq(i).text()
                   })
               })
                   .attr('data-value', selectOption.eq(i).val())
                   .appendTo(selectList);
           }
           if ($this.hasClass('search')) {
               $('<li>', {
                   class: 'select__item disabled',
                   html: $('<span>', {
                       text: "Введите код"
                   })
               }).appendTo(selectList);
           }
           var selectItem = selectList.find('li');
           selectList.slideUp(0);
           selectGap.on('click', function() {
               $id = $this.attr('id');
               if (!$(this).hasClass('on')) {
                   $(this).addClass('on');
                   selectList.slideDown(dur);
                   $('body').on('click', '.select__list>li', function(e) {
                       var chooseItem = $(this).data('value');
                       $value = chooseItem;
                       $('#' + $id).val(chooseItem).attr('selected', 'selected');
                       $('.select__gap[id='+$id+']').text($(this).find('span').text());
                       selectList.slideUp(dur);
                       selectGap.removeClass('on');
                       $('#to_coll').prop('disabled', false);
                       $('#to_show').prop('disabled', false);
                   });
               } else {
                   $(this).removeClass('on');
                   selectList.slideUp(dur);
               }
           });
       }
       }
       );
       })
       $(window).on("scroll", function() {
           if ($(window).scrollTop() > 166) $('#list_map').addClass('fixed');
           else $('#list_map').removeClass('fixed');
       });
       if({{Cache::get('objects_page',0) }} == "0") $('#back').addClass('disabled');
       if({{Cache::get('objects_page',0) +15 }} >= {{$allcount}}) $('#next').addClass('disabled');
       function tableDraw(response){
           markers.eachLayer(n => this.markers.removeLayer(n));
           map_f();
           console.log( {{$allcount}});
           $('.pagination > span').text((response.new_offset +1)+" - "+(response.new_offset + response.data.length));

           if(response.new_offset ==0)$('#back').addClass('disabled'); else $('#back').removeClass('disabled');
           if((response.new_offset+15) >= {{$allcount}})$('#next').addClass('disabled'); else $('#next').removeClass('disabled');

           $("#objects > tbody").html("");
           response.data.forEach(element => {
               if(element["ticket"]['lastComment'] != null) {
                   var comment = `<div class ="comments" style="width:100px;"> <div class ="comment"><div class="comment-header">
                                  <div class="comment-user"> ` + element["ticket"]['lastComment']['user']['initials'] + `</div></div></div>
                                  <div class="comment-content" style="max-width: 100px;word-wrap: break-word;width: min-content;"> <div class=crop>` + element["ticket"]['lastComment']['text'] + `</div> </div> </div > </div>`;
               }
               else comment='';
               var tr=` <tr> <td>
<input value=`+element["ticket"]["id"]+` type=checkbox name="checked_objects" id=ticket_`+element["ticket"]["id"]+`>
       <label for = ticket_`+element["ticket"]["id"]+` ></label>
</td>
<td> <a href="/object/`+element["id"]+`/ticket/`+element["ticket"]['id']+`">
<span style="color:gray">`+element["object_params"]["type"] +` </span>
<br># `+element["id"]+` <br>`+element["object_params"]["address"] +` </a></td>
<td>Площадь: `+element["object_params"]["area"] +`
<br>Кол-во комнат: `+element["object_params"]["flat"]+`
<br>Этаж: `+element["object_params"]["floor"]+`
<br>Год постройки: `+element["object_params"]["year_construct"]+`
</td>
<td>`+element["ticket"]["params"]["price"]+`</td>
<td> <a href="/object/`+element["id"]+`/ticket/`+element["ticket"]['id']+`/3"> Фото(`+element["object_params"]["media_count"] +`) </a></td>
<td> `+element["ticket"]["user"]["fullname"]+`   </td>
<td> <a href="/client/`+element["ticket"]["client"]['id']+`">`+element["ticket"]["client"]["fullname"]+`  </a> <br> </td>
<td> <div class=crop>`+element["ticket"]["params"]["description"]+`</div></td><td>`+comment+`</td></tr>`;
               $('#objects > tbody').append(tr);
           });
       };
       $( document ).ready(function() {
           $('#search_button').on('click', function(){

               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                   }
               });
               $.ajax({
                   url: '/objects/search',
                   data: {tab: {{$tab}}, text: $('#search_input').val() },
                   type: 'POST',
                   success: function(response) {
                       console.log(response);
                       tableDraw(response);
                       $('allcount').text(response.count);
                   },
                   error: function(response){
                       console.log(response);
                   }
               });
           });

           $('.item_pagination').on('click', function(){
               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                   }
               });
               $.ajax({
                   url: '/objects/get',
                   data: {page: $(this).attr('id')},
                   type: 'POST',
                   success: function(response) {
                       tableDraw(response);
                       console.log(response);
                   },
                   error: function(response){
                       console.log(response);
                   }
               });
           });
       })
   </script>

@endsection

