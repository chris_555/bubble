@extends('layouts.app')

@section('overlay-header')
    <div class="returntoback">
        <a href="{{ route('myStatistics') }}"> <i class="fas fa-angle-left dark-icon"></i>   ПАНЕЛЬ АДМИНИСТРАТОРА </a>
        <b>/ Общество с ограниченной ответственностью "Вклад"  </b>
    </div>
@endsection

@section('content')
    <div class="content_header">
        <div  class="separatorTitle nameTab">Отчетность за период по месяцам</div>
    </div>
    <div class="pageSeparator"></div>

    @component('components.input_between', [
    'styleTitle' => 'style=width:200px',
     'title' =>'Выберете период',
     'type'=>'date',
     'maska'=>'date',
     'width'=>'150px',
     'id_min'=>'date_min',
     'id_max'=>'date_max',
     ])
    @endcomponent
    <input   type=checkbox value= "notime" id='notime' > <label style="margin-right: 20px" for = 'notime' > За все время </label>

    <div class="input_inline_block">
        <div style="width: 200px" class="input-title_inline">Выберите параметр </div>
        <input   type=radio value= "developer" id='1'  name=entity> <label style="margin-right: 20px" for = '1' > По застройщику</label>
        <input   type=radio value= "user" id='2' name=entity > <label style="margin-right: 20px" for = '2' > По риэлтору </label>
        <input   type=radio value= "users" id='4' name=entity > <label style="margin-right: 20px" for = '4' > По всем риэлторам </label>
        <input   type=radio value= "common" id='3' name=entity> <label style="margin-right: 20px" for = '3' > По комнатности  </label>
        </div>

    <div id='developer' class="input_inline_block">
        <div style="width: 200px" class="input-title_inline">Выберите застройщика</div>
    <select name="select-box"
            id="developer_id"
            class="select search">
    </select>
    </div>

    <div id='user' class="input_inline_block">
        <div style="width: 200px"  class="input-title_inline">Выберите риэлтора</div>
        <select name="select-box"
                id="user_id"
                class="select search">
        </select>
    </div>

   <div style="margin: 10px 0">
       <button id="init"
            class="item button dark" style="margin-right: 10px">Сформировать
      </button>
    <button id="download"
            class="item button dark disabled">Скачать отчет
    </button>
   </div>
    <span  style="margin-bottom: 20px" class="info" >
           Отчет будет доступен к загрузке после формирования таблицы
    </span>
    <div id="table"> </div>
    <script>
    $(function() {
        $('#notime').click(function(){
            if($(this).is(':checked')){
                $('#date_min').addClass('disabled');
                $('#date_max').addClass('disabled');
            }
            else {
                $('#date_min').removeClass('disabled');
                $('#date_max').removeClass('disabled');
            }
        })
        $('input[name=entity]').click(function(){
            if($(this).is(':checked')){
                if($(this).val()=="developer") {
                    $('#developer').show();
                    $('#user').hide()

                }
                else if($(this).val()=="user")  {
                    $('#user').show();
                    $('#developer').hide();
                }
                else {
                    $('#developer').hide();
                    $('#user').hide();}
            }

        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();

        if(dd<10){dd='0'+dd}
        if(mm<10){mm='0'+mm}
        today = yyyy+'-'+mm+'-'+dd;

        $('input[type=date]').attr("max", today)
        $('input[type=date]').attr("value", today)

        $('#developer').hide();
        $('#user').hide();

        $('#download').click(function(){
            $entity= $('input[name=entity]:checked').val();
            $entity_id = null;
            if($entity=="developer") {
                $entity_id = $('#developer_id').val();

            }
            else  if($entity=="user"){
                $entity_id = $('#user_id').val();
            }
            location ="/report/download/"+$('#date_min').val()+'/'+$('#date_max').val()+'/'+$entity+'/'+$entity_id;
        })


        $('#init').click(function(){
            $url = '/report/get';
            $entity= $('input[name=entity]:checked').val();
            $entity_id = null;
            if($entity=="developer") $entity_id = $('#developer_id').val();
             else  if($entity=="user") $entity_id = $('#user_id').val();

            if($('#notime').is(':checked'))
            {
                $date_max = 'no'
                $date_min = 'no';
            }
            else {
                $date_max = $('#date_max').val();
            $date_min =$('#date_min').val();
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                }
            });
            $.ajax({
                url: $url,
                data: {
                    entity: $entity,
                    entity_id:$entity_id,
                    date_min: $date_min,
                    date_max: $date_max
                },
                dataType: "json",
                type: 'POST',
                success: function(response) {
                    $('#download').removeClass('disabled');
                    $('#table').empty();
                    console.log(response);
                    $cltext="";
if(response.entity!='users') $cltext=" Количество комнат";


                    if(response.count!=0) {
                        $table = `
<b> I.Количество проданных объектов за период` + response.first_m + `-` + response.last_m + `</b>
<table><tr>
                    <td> `+$cltext+`</td>`;
                        Object.keys(response.months).forEach(function (key) {
                            $table += `<td>` + response.months[key] + `</td>`
                        });
                        Object.keys(response.array).forEach(function (key1) {
                            $table += `<tr><td>` + key1 + `</td>`;
                            Object.keys(response.months).forEach(function (key2) {
                                $val = response.array[key1]['count'][key2];
                                $table += `<td>` + $val + `</td>`
                            });
                            $table += `</tr>`;
                        });
                        $table += `</table>`;


                        $table += `
<b> II.Итог реализации объектов за период ` + response.first_m + `-` + response.last_m + `</b>
<table><tr>
                    <td> `+$cltext+`</td>`;
                        Object.keys(response.months).forEach(function (key) {
                            $table += `<td>` + response.months[key] + `</td>`
                        });
                        Object.keys(response.array).forEach(function (key1) {
                            $table += `<tr><td>` + key1 + `</td>`;
                            Object.keys(response.months).forEach(function (key2) {
                                $val = response.array[key1]['sum'][key2];
                                $table += `<td>` + $val + `</td>`
                            });
                            $table += `</tr>`;
                        });
                        $table += `</table>`;
                        $('#table').append($table);
                        var res = (100 / (response.count + 1));
                        $('#table tr td').width(res + '%');
                    }
                    else {
                       alert('Период отчета должен составлять минимум один месяц!')
                    }
                },
                error: function(response) {
                    console.log(response);

                }
            });

        });

        $('body').on('keydown', '.select__gap', function(e) {
            $this = $(this);
            $text = $this.text();
            $id =$this.attr('id');
            if($id==="developer_id")   $url = '/developers/findDeveloper';
            else    $url = '/users/findUser';
            $('ul[id=' + $id+ ']>li:not(:first-child)').remove();

            if ($text != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: $url,
                    data: {
                        developer_name: $text
                    },
                    dataType: "json",
                    type: 'POST',
                    success: function(response) {
                        $('ul[id='+$id+']>li:not(:first-child)').remove();
                        console.log(response);
                        response.results.forEach((element) => {
                            $('#'+$id).append($('<option>', {
                                value: element['id'],
                                text: element['name_of_org']
                            }));
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item',
                                html: $('<span>', {
                                    text: element['name_of_org']
                                })
                            }).attr('data-value', element['id']));
                        });
                        if (response.results.length == 0) {
                            $('ul[id='+$id+']').append($('<li>', {
                                class: 'select__item disabled',
                                html: $('<span>', {
                                    text: "Мы ничего не нашли :("
                                })
                            }));
                        }
                    },
                    error: function(response) {
                        console.log(response);
                        $('ul[id="1gf"]').append($('<li>', {
                            class: 'select__item disabled',
                            html: $('<span>', {
                                text: "Мы ничего не нашли :("
                            })
                        }));
                    }
                });
            }
        });
        $('.select').each(function() {
            var $this = $(this),
                selectOption = $this.find('option'),
                selectOptionLength = selectOption.length,
                selectedOption = selectOption.filter(':selected'),
                dur = 100;
            $id = $this.attr('id');
            $text = $("#" + $id + " option:selected").text();
            if($this.attr('old_value') ==" " ){ $text ='Не указано';}
            if($this.hasClass('search') && $this.attr('old_value') ==" " ){ $text ='';}
            $value = "";
            $this.hide();
            $this.wrap('<div class="select"></div>');
            $('<div>', {
                class: 'select__gap',
                text: $text,
                id: $id,
            }).insertAfter($this);

            var selectGap = $this.next('.select__gap'),
                caret = selectGap.find('.caret');
            $('<ul>', {
                class: 'select__list',
                style: "width:352px;"
            }).insertAfter(selectGap);
            if ($this.hasClass('search')) {
                selectGap.attr("placeholder", 'Выберите из списка');
                selectGap.attr('contenteditable', true);
            }
            var selectList = selectGap.next('.select__list');
            if ($this.hasClass('search')) {
                selectList.attr('id', $id);
            }
            for (var i = 0; i < selectOptionLength; i++) {
                $('<li>', {
                    class: 'select__item',
                    html: $('<span>', {
                        text: selectOption.eq(i).text()
                    })
                })
                    .attr('data-value', selectOption.eq(i).val())
                    .appendTo(selectList);
            }
            if ($this.hasClass('search')) {
                $('<li>', {
                    class: 'select__item disabled',
                    html: $('<span>', {
                        text: "Введите текст"
                    })
                }).appendTo(selectList);
            }
            var selectItem = selectList.find('li');
            selectList.slideUp(0);
            selectGap.on('click', function() {
                $id = $this.attr('id');
                if (!$(this).hasClass('on')) {
                    $(this).addClass('on');
                    selectList.slideDown(dur);
                    $('body').on('click', '.select__list>li', function(e) {
                        var chooseItem = $(this).data('value');
                        $value = chooseItem;
                        $('#' + $id).val(chooseItem).attr('selected', 'selected');
                        $('.select__gap[id='+$id+']').text($(this).find('span').text());
                        selectList.slideUp(dur);
                        selectGap.removeClass('on');
                    });
                } else {
                    $(this).removeClass('on');
                    selectList.slideUp(dur);
                }
            });
        });
    })
</script>
@endsection
