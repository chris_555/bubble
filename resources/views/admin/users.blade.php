
@extends('layouts.app')

@section('overlay-header')
    <div class="search">
        <div class="search_box">
            <input id="search_input" type="text" value ="{{ Cache::get('users_search') ??''}}" placeholder="ФИО или код сотрудника">
            <button id="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
    </div>
@endsection

@section('content')

    <div class="content_header">
        <div class="separatorTitle">
           Сотрудники
        </div>
        <button onclick="window.location='{{route("users_admin", [ "drawer"=>"createusers"])}}'" class="seporator_item  dark">
            <b style="margin-right: 10px">+</b> Добавить сотрудника</button>
    </div>
    <div class="pageSeparator"></div>
    <div style="padding: 10px;height: 20px">
    <div class="pagination">
        <span>{{Cache::get('users_page',0)+1 }} - {{Cache::get('users_page',0)+count($users) }} </span>  из <allcount> {{$allcount }} </allcount>
        <div class="item_pagination" id="back"> < </div>
        <div class="item_pagination" id="next"> > </div>
    </div>
    </div>

    <table id ="users">
        <thead>
        <tr>
            <th> Код риэлтора</th>
            <th> ФИО</th>
            <th> Телефон</th>
            <th> Почта</th>
        </thead>
        @foreach ($users as $user )
            <tr>
                <td>
                    <a href={{route("userCard", ["id" => $user["id"]])}}>
                        # {{$user["id"]}}</a>
                </td>
                <td>
                    {{$user["fullname"]}}
                </td>
                <td>
                    {{$user["phone"]}}
                </td>
                <td>
                    {{$user["email"]}}
                </td>
        @endforeach
    </table>


    <script>
        function tableDraw(response){
            $('.pagination > span').text((response.new_offset +1)+" - "+(response.new_offset + response.data.length));
            if(response.new_offset ==0)$('#back').addClass('disabled'); else $('#back').removeClass('disabled');
            if((response.new_offset+15) >= {{$allcount}})$('#next').addClass('disabled'); else $('#next').removeClass('disabled');

            $("#users > tbody").html("");
            response.data.forEach(element => {
if(!element["email"])element["email"]='';
                var tr =`
                         <tr >
                          <td><a href="/user/`+element['id']+`"> # `+element['id']+`</a> </td>
                            <td> `+element["fullname"]  +` </td>
                            <td> `+element["phone"]+`</td>
                             <td> `+element["email"]+`</td>
                    `;
                $('#users> tbody').append(tr);
            });
        }
        $( document ).ready(function() {
            $('#search_button').on('click', function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/users/search',
                    data: {text: $('#search_input').val()},
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        tableDraw(response);
                        $('allcount').text(response.allcount);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });

            if({{Cache::get('users_page',0) }} == "0") $('#back').addClass('disabled');
            if({{Cache::get('users_page',0) +15 }} >= {{$allcount}}) $('#next').addClass('disabled');
            $('.item_pagination').on('click', function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?= csrf_token() ?>'
                    }
                });
                $.ajax({
                    url: '/users/getList',
                    data: {page: $(this).attr('id')},
                    type: 'POST',
                    success: function(response) {
                        console.log(response);
                        tableDraw(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });
            });
        })
    </script>
@endsection

