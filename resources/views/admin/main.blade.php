@extends('layouts.app')

@section('overlay-header')
       <div class="content_header">
        <div  class="separatorTitle nameTab">Панель администратора</div>
    </div>
@endsection

@section('content')


    <div style="height: 500px;margin-top: 20px">
        <button id="button_edit" onclick="window.location='{{route("users_admin")}}'"
                class="seporator_item  light">
           Cотрудники </button>
        <button id="button_edit" onclick="window.location='{{route("reports")}}'"
                class="seporator_item  light">
            Отчеты </button>
        <button id="button_edit" onclick="window.location='{{route("tickets_admin")}}'"
                class="seporator_item  light">
           Работа с заявками </button>

        <div class="pageSeparator"></div>
        <div class="name_chart">Лидеры месяца</div>
        <div class='stat'>
<h3> <span style="color:gray" > июнь </span> | Топ 5 лидеров по количеству заключенных сделок  </h3>
            <div class="pageSeparator"></div>
            @foreach($static_users_count as $item)
                <div class="content_stat">
               <div >  {{$item->name}} </div>
                <div>  {{$item->count}}  </div>
                </div>
            @endforeach

        </div>
        <div class='stat'>
        <h3>  <span style="color:gray" > июнь </span> | Топ 5 лидеров по валовой выручке </h3>
            <div class="pageSeparator"></div>

        @foreach($static_users_sum as $item)
                <div class="content_stat">
                <div >  {{$item->name}} </div>
                <div >       {{  number_format(  $item->sum , 0, ' ', ' ').' руб.'}} </div>
                </div>
        @endforeach
        </div>
        <div class='stat'>
            <h3> <span style="color:gray" > июнь </span> | Топ 5 лидеров по продаже новостроек  </h3>
            <div class="pageSeparator"></div>
            @foreach($static_users_newb as $item)
                <div class="content_stat">
                    <div >  {{$item->name}} </div>
                    <div>  {{$item->count}}  </div>
                </div>
            @endforeach

        </div>
        <div class='stat'>
            <h3> <span style="color:gray" > июнь </span> | Топ 5 лидеров по продаже вторичного жилья  </h3>
            <div class="pageSeparator"></div>
            @foreach($static_users_oldb as $item)
                <div class="content_stat">
                    <div >  {{$item->name}} </div>
                    <div>  {{$item->count}}  </div>
                </div>
            @endforeach

        </div>
        <div class="pageSeparator"></div>
        <div class="name_chart" style=" margin-bottom: 0px">Статус заявок по сотрудникам
        </div>
        <div id="chart">

        </div>
        <div class="pageSeparator"></div>
        <div id="app" style="display: inline-block;width: 49%; margin-right: 30px">
            @if($date_min===0 )
                <div class="name_chart" style="margin: 150px auto;font-size: 20px "> У вас нет заключенных сделок </div>
            @else
                <div class="name_chart">
                    Количество заключенных сделок с {{ $date_min }}  по {{ $date_max }}</br>
                </div>
                {!! $chart->container() !!}

        </div>

        <div id="app" style="display: inline-block;width: 49%">
            <div class="name_chart">
                Валовая выручка с продаж с {{ $date_min }}  по {{ $date_max }}</br>
            </div>
            {!! $chart2->container() !!}

        </div>
        @endif
    </div>


    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script>
        var app = new Vue({
            el: '#app',
        });
        var users;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '<?= csrf_token() ?>'
            }
        });
        $.ajax({
            url: '/users/get',
            dataType: "json",
            type: 'POST',
            success: function(response) {
                users =response.data;
    bachart(users);
            },
            error: function(response) {

            }
        });
      function bachart(users) {
          var options = {
              series: [{
                  name: 'В работе',
                  data: {{$in_work}},
              }, {
                  name: 'Отменена',
                  data: {{$paused}}
              }, {
                  name: 'Завершена с успехом',
                  data: {{$done}}
              }
              ],
              colors: ['#f4c536', '#fd6e00', '#37c418'],
              chart: {
                  type: 'bar',
                  stacked: true,
              },
              plotOptions: {
                  bar: {
                      horizontal: true,
                  },
              },
              stroke: {
                  width: 1,
                  colors: ['#fff']
              },
              xaxis: {
                  categories: users,

              },
              tooltip: {
                  y: {
                      formatter: function (val) {
                          return val
                      }
                  }
              },
              fill: {
                  opacity: 1
              },
              legend: {
                  position: 'top',
                  horizontalAlign: 'left',
                  offsetX: 130,
                  offsetY: 20
              }
          };

          var chart = new ApexCharts(document.querySelector("#chart"), options);
          chart.render();
      }

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.7/c3.min.js"></script>
    {!! $chart->script() !!}
    {!! $chart2->script() !!}
    </div>


@endsection
